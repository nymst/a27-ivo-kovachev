<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [React Router](https://reactrouter.com/web/guides/quick-start)
- [ReactTraining / react-router](https://github.com/ReactTraining/react-router)

<br>

- [Learn React Router in 5 Minutes - A Beginner's Tutorial](https://www.freecodecamp.org/news/react-router-in-5-minutes/)
