import React from 'react';
import Layout from './components/Layout/Layout';
import NotFound from './components/NotFound/NotFound';
import Movies from './containers/Movies/Movies';
import IndividualMovie from './containers/Movies/IndividualMovie';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Home from './components/Home/Home';

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Redirect from="/" exact to="/home" />
          <Route path="/home" component={Home} />
          <Route path="/movies" exact component={Movies} />
          <Route path="/movies/:id" component={IndividualMovie} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
