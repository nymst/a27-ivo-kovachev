import React, { useEffect, useState, Fragment } from 'react';
import Loader from '../../components/Loader/Loader';
import { CONSTANTS } from '../../constants/constants';
import MovieDetails from '../../components/Movies/MovieDetails';
import AdditionalMovieInfo from '../../components/Movies/AdditionalMovieInfo';

const IndividualMovie = props => {
  const [movieInfo, setMovieInfo] = useState(null);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const imdbID = props.match.params.id;

  useEffect(() => {
    setLoading(true);

    fetch(`${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&i=${imdbID}`)
      .then(response => response.json())
      .then(data => {
        if (data.Response === 'True') {
          setMovieInfo(data);
        } else if (data.Response === 'False') {
          setError(data.Error);
        }
      })
      .finally(() => setLoading(false));
  }, [imdbID]);

  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <h1>{error}</h1>;
  }

  return (
    <div>
      {movieInfo && (
        <Fragment>
          <MovieDetails {...movieInfo} />
          <AdditionalMovieInfo {...movieInfo} />
        </Fragment>
      )}
    </div>
  );
};

export default IndividualMovie;
