<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 11. Custom Hook

### 1. Custom Hook

1. Look at the fetching logic in both of our containers - **Movies** and **IndividualMovie**. It is very similar actually - we can extract it in a **custom react hook**
1. Create a **useHttp** hook - one possible way to go would be to create **src/hooks/useHttp.js** file.
   1. The hook should take **url** and **optional initial data** parameters. Think about it - when loading the movies list we do not have initialData - we start with empty array. When rendering movie details we have the movie.
   
      ```js
        const useHttp = (url, initialData = null) => { .. }
        export default useHttp;
      ```

   2. The hook should manage **data**, **loading** and **error** states internally by using the **useEffect hook**. Remember that custom hoks are js functions that use react hooks and their nmes start with 'use' verb.
   3. Make the fetching to the passed url just like in the containers.
   4. Return an object holding the **data**, **setData**, **loading** and **error** states
  
      ```js
        return { data, setLocalData: setData, loading, error };
      ```

   5. If you've done your job correctly you should have a custom hook looking like this:

      ```js
        import { useEffect, useState } from 'react';

        const useHttp = (url, initialData = null) => {
          const [data, setData] = useState(initialData);
          const [loading, setLoading] = useState(false);
          const [error, setError] = useState(null);

          useEffect(() => {
            setLoading(true);

            fetch(url)
              .then(response => response.json())
              .then(data => {
                if (data.Response === 'True') {
                  setData(data);
                } else if (data.Response === 'False') {
                  setError(data.Error);
                }
              })
              .finally(() => setLoading(false));
          }, [url]);

          return { data, setLocalData: setData, loading, error };
        };

        export default useHttp;
      ```

1. Replace the fetching inside the **Movies** container with the custom hook
    1. Replace the movies, loading & error state variables, along with their functions and useEffect call with your new hook:
    
      ```js
          const { data, setLocalData, loading, error } = useHttp(
          `${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&s=grand`,
          {
            Search: []
          }
        );
        const movies = data.Search;
      ```
   2. Do not forget that **setMovies** will not exists anymore. You will need to use **setLocalData** instead. Alse keep in mind the data format:

      ```js
        setLocalData({ Search: filteredMovies });
      ```

   3. If you've done your job correct your Movies component will look something like:

      ```js
          const Movies = props => {
          const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(
            null
          );

          const { data, setLocalData, loading, error } = useHttp(
            `${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&s=grand`,
            {
              Search: []
            }
          );
          const movies = data.Search;

          if (loading) {
            return <Loader />;
          }
          if (error) {
            return <h1>{error}</h1>;
          }

          const recommendMovie = () => {
            const validMovieIndex = Math.floor(Math.random() * movies.length);
            setMovieRecommendationIndex(validMovieIndex);
          };

          const clearRecommendations = () => {
            setMovieRecommendationIndex(null);
          };

          const deleteMovie = movieId => {
            const filteredMovies = movies.filter(movie => movie.imdbID !== movieId);
            setLocalData({ Search: filteredMovies });
          };

          const moviesToShow =
            movieRecommendationIndex !== null ? (
              <div>
                <div>Our recommendation:</div>
                <MovieDetails
                  {...movies[movieRecommendationIndex]}
                  goToDetails={() =>
                    props.history.push(
                      `/movies/${movies[movieRecommendationIndex].imdbID}`
                    )
                  }
                />
              </div>
            ) : (
              <div className="movie-list">
                {movies.map(movie => {
                  return (
                    <MovieDetails
                      key={movie.imdbID}
                      {...movie}
                      goToDetails={() => props.history.push(`/movies/${movie.imdbID}`)}
                      deleteMovie={() => deleteMovie(movie.imdbID)}
                    />
                  );
                })}
              </div>
            );

          return (
            <div className="Movies">
              <div>
                <button onClick={recommendMovie}>Recommend a movie</button>
                <button onClick={clearRecommendations}>Clear recommendations</button>
              </div>

              <br />

              {moviesToShow}
            </div>
          );
        };
        export default withButtons(Movies);
      ```

2. Replace the fetching inside the **IndividualMovie** container with the custom hook
   1. Replace the movies, loading & error state variables, along with their functions and useEffect call with your new hook:

      ```js
        const { data, loading, error } = useHttp(
          `${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&i=${imdbID}`
        );
      ```

   2. Do not forget that **movieInfo** will not exists anymore. You will need to use **data** instead.
   3. If you've done your job correct your Movies component will look something like:

      ```js
          const IndividualMovie = props => {
          const imdbID = props.match.params.id;
          const { data, loading, error } = useHttp(
            `${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&i=${imdbID}`
          );
          
          if (loading) {
            return <Loader />;
          }
          if (error) {
            return <h1>{error}</h1>;
          }

          return (
            <div>
              {data && (
                <Fragment>
                  <MovieDetails {...data} />
                  <AdditionalMovieInfo {...data} />
                </Fragment>
              )}
            </div>
          );
        };

        export default IndividualMovie;
      ```


3. Pretty neat, hah :)
