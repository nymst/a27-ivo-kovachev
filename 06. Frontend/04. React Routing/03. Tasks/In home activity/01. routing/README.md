<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 10. Routing

### 1. Routing

1. Install **react-router-dom** package https://www.npmjs.com/package/react-router-dom

    ```bash
      npm i react-router-dom
    ```

2. Create a **Home component**. It should be something simple:

    ```js
      import React from 'react';

      const Home = props => {
        return <h1>Hello!</h1>;
      };

      export default Home;
    ```

3. In your root component **App.js** wrap the **Layout** with the **<BrowserRouter>** so the pages can receive the routing props.
4. Add a **<Switch>** and inside the routes for **Home** and **Movies** components as **children** to the **Layout**:

    ```html
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route path="/home" component={Home} />
            <Route path="/movies" component={Movies} />
          </Switch>
        </Layout>
      </BrowserRouter>
    ```

5. Test the Route urls inside the browser
6. Add a redirection from **/** to **/home**. Do not forget to add the **exact** keyword. You can test out and add the redirection first without it. Will the movies URl work?

    ```html
      <Redirect from="/" exact to="/home" />
    ```

7. Add a **IndividualMovie** container component. 
   1. Use the API endpoint to get one:

      ```
        https://www.omdbapi.com/?apiKey=1591af6&i=tt2319580
      ```

   2. Get the movie ID from route params:

      ```js
        const imdbID = props.match.params.id;
      ```
   3. Do not forget to add the loader and to handle possible errors

    If you have done your job right yor component should look something like this:

    ```js
      import React, { useEffect, useState, Fragment } from 'react';
      import Loader from '../../components/Loader/Loader';
      import { CONSTANTS } from '../../constants/constants';
      import MovieDetails from '../../components/Movies/MovieDetails/MovieDetails';

      const IndividualMovie = props => {
        const [movieInfo, setMovieInfo] = useState(null);

        const [loading, setLoading] = useState(false);
        const [error, setError] = useState(null);

        const imdbID = props.match.params.id;

        useEffect(() => {
          setLoading(true);

          fetch(`${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&i=${imdbID}`)
            .then(response => response.json())
            .then(data => {
              if (data.Response === 'True') {
                setMovieInfo(data);
              } else if (data.Response === 'False') {
                setError(data.Error);
              }
            })
            .finally(() => setLoading(false));
        }, [imdbID]);

        if (loading) {
          return <Loader />;
        }
        if (error) {
          return <h1>{error}</h1>;
        }

        return (
          <div>
            {movieInfo && (
              <Fragment>
                <MovieDetails {...movieInfo} />
              </Fragment>
            )}
          </div>
        );
      };

      export default IndividualMovie;
    ```
  
1. Attach it to the routing as a subresource to movies - remember that query parameters are added using **:**. Do you need to add **exact** in the **Movies** route?

    ```html
      <Route path="/movies/:id" component={IndividualMovie} />
    ```
   1. *Spoiler alert - yes :) Why?*
   2. Test it out - here's an example movieId - **tt2319580**. The url should be:
      ```
        http://localhost:3000/movies/tt2319580
      ```
   
9. Let's change a bit **MovieDetails component** 
   1.  Attach the delete movie callback to a button and show it only if the delete callback is passed:

        ```html
          {deleteMovie && (
              <div>
                <button onClick={deleteMovie}>Delete</button>
              </div>
            )}
        ```

   2.   Enhance **MovieDetails** component with a redirect to the **IndividualMovie** page on click by 
        1. In **Movies component** pass a redirection callback from the parent using the **props.history**. Make sure your component has **props** as function arguments:

        ```js
          const Movies = (props) => { ... }
        ```

        ```html
          <MovieDetails
              key={movie.imdbID}
              {...movie}
              goToDetails={() => props.history.push(`/movies/${movie.imdbID}`)}
              deleteMovie={() => deleteMovie(movie.imdbID)}
            />
        ```

        2. Add an **onClick** event on every movie's image in **MovieDetails** to go to individual movie, using **goToDetails** prop. Do not forget tp remove the onClick from the parent div!
        
          ```html
              return (
                <div id={props.imdbID} className='MovieDetails'>
                  <img onClick={props.goToDetails ? props.goToDetails : () => {}} src={props.Poster} alt="poster" />
                  <div>{props.Title}</div>
                  <div>
                    Year: {props.Year} | Type: {props.Type}
                  </div>
                  {deleteMovie && (
                    <div>
                      <button onClick={deleteMovie}>Delete</button>
                    </div>
                  )}
                </div>
              );
          ```
          3. Now you can **goToDetails** prop for the recommendation as well - use **${movies[movieRecommendationIndex].imdbID**

          ```html
            <div>
              <div>Our recommendation:</div>
              <MovieDetails {...movies[movieRecommendationIndex]}
                goToDetails={() =>
                  props.history.push(
                    `/movies/${movies[movieRecommendationIndex].imdbID}`
                  )
                } />
            </div>
          ```

10. Let's enrich our movie - add an **AdditionalMovieInfo** component

      ```js
        import React from 'react';

        const AdditionalMovieInfo = props => {
          const { Actors, Awards, Plot } = props;

          return (
            <div>
              <h3>Additional Info</h3>
              <div>{Plot}</div>

              <hr />

              <div>Actors: {Actors}</div>
              <div>Awards: {Awards}</div>
            </div>
          );
        };

        export default AdditionalMovieInfo;
      ```

11. Show the **MovieDetails** and the **AdditionalMovieInfo** inside the **IndividualMovie** container:

    ```html
      return (
        <div>
          {movieInfo && (
            <Fragment>
              <MovieDetails {...movieInfo} />
              <AdditionalMovieInfo {...movieInfo} />
            </Fragment>
          )}
        </div>
      );
    ```

12. Now we'll need some menu so we can browse the pages without manually entering their URLs. Let's create a **Navigation** component
    1.  Add **NavLink**-s for **Home** and **Movie** pages

    ```js
      import React from 'react';
      import { NavLink } from 'react-router-dom';
      import './Navigation.css';

      const Navigation = () => {
        return (
          <nav className="Navigation">
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/movies">Movies</NavLink>
          </nav>
        );
      };

      export default Navigation;
    ```

13. Add it as a child of the **Layout** component

    ```html
      const Layout = props => {
      return (
        <Fragment>
          <Header />
          <Navigation />

          <hr />

          <main>{props.children}</main>
        </Fragment>
      );
    };
    ```

14. Create a **NotFound** page, again make it simple:
    
    ```js
      import React from 'react';

      const NotFound = props => {
        return <h1>404</h1>;
      };

      export default NotFound;
    ```

    1. Show it whenever the current url in the browser is not registered as a route. Remember that we do that by using the regex *****
    
    ```html
      <Route path="*" component={NotFound} />
    ```

15. Play with css and add styling to the application
16. **Optional:** Add **Back** button in the **IndividualMovie** component. Remeber that:
    1.  You can use **props.history.goBack()** to return to the previous page
    2.  Only components directly rendred from thw Router will have the navigation props - location, history & match. To make sure your **Back** button works even if you paste the url use the higher order component **withBrowser**

      ```js
        import { withRouter } from 'react-router-dom';
        ...
        export default withRouter(IndividualMovie);
      ```
 