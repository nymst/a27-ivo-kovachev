import React from 'react';
import './styles/button.css';

const withButtons = (BaseComponent) => (props) => {
  return (
    <div className="myButton">
      <BaseComponent {...props} />
    </div>
  );
}

export default withButtons;