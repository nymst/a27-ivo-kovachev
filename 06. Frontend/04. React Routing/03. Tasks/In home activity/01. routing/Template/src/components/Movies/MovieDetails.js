import React from 'react';
import PropTypes from 'prop-types';
import './MovieDetails.css';

const MovieDetails = props => {
  const deleteMovie = props.deleteMovie || (() => {});

  return (
    <div id={props.imdbID} onClick={deleteMovie} className='MovieDetails'>
      <img src={props.Poster} alt="poster" />
      <div>{props.Title}</div>
      <div>
        Year: {props.Year} | Type: {props.Type}
      </div>
    </div>
  );
};

MovieDetails.propTypes = {
  imdbID: PropTypes.string,
  poster: PropTypes.string,
  title: PropTypes.string,
  year: PropTypes.string,
  type: PropTypes.string,
  deleteMovie: PropTypes.func
};

export default MovieDetails;
