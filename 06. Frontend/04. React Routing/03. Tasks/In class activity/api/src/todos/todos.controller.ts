import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { TodosDataService } from './todos-data.service';
import { ResponseMessageDTO } from './models/response-message.dto';
import { CreateTodoDTO, UpdateTodoDTO } from './models';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { User } from '../common/decorators/user.decorator';
import { ReturnTodoDTO } from './models/return-todo.dto';

@Controller('todos')
export class TodosController {
  public constructor(private readonly todosDataService: TodosDataService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  public async allTodos(@Query('name') name: string): Promise<ReturnTodoDTO[]> {
    const todos: ReturnTodoDTO[] = await this.todosDataService.allTodos();

    if (name) {
      return todos.filter(todo =>
        todo.name.toLowerCase().includes(name.toLowerCase()),
      );
    }

    return todos;
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  public async byId(@Param('id') id: string): Promise<ReturnTodoDTO> {
    return this.todosDataService.byId(+id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async addNewTodo(
    @Body() todo: CreateTodoDTO,
    @User() user: ShowUserDTO,
  ): Promise<ReturnTodoDTO> {
    return await this.todosDataService.createTodo(todo, user);
  }

  @Put('/:id')
  @HttpCode(HttpStatus.OK)
  public async updateTodo(
    @Param('id') todoId: number,
    @Body() todo: Partial<UpdateTodoDTO>,
  ): Promise<ReturnTodoDTO> {
    return await this.todosDataService.updateTodo(todoId, todo);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteTodo(
    @Param('id') todoId: number,
  ): Promise<ResponseMessageDTO> {
    await this.todosDataService.deleteTodo(todoId);
    return { msg: 'Todo Deleted!' };
  }
}
