import { UserRole } from '../../users/enums/user-role.enum';

export const attachUser = (req: Request, res: Response, next: Function) => {
  (req as any).user = { username: 'admin', roles: Object.keys(UserRole) };
  next();
};
