import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { TodosSystemErrorFilter } from './common/filters/todos-error.filter';
import { attachUser } from './common/middleware/attach-user.middleware';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'avatars'), { prefix: '/avatars' });

  app.enableCors();
  app.use(attachUser);

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  app.useGlobalFilters(new TodosSystemErrorFilter());

  await app.listen(app.get(ConfigService).get('PORT'));
}

bootstrap();
