<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React ToDo Client

### 1. Description

Seems like the whole front-end team you had been working with to deliver the Todo app got fired and now its your job to do it! Use the Todo API you have created and create a Todo web client with React to consume the API and satisfy the customers.

<br>

### 2. Project information

You will be using React with functional components to build the client app. The activity is continuous and ongoing, and each activity after the first one will build on the previous one refactoring existing code and adding new functionality. You will be provided with the ToDo Server at a later point

<br>

### 3. Goals

We can now make calls to the Todos API, fetch data, create new todos and update them. We finished the last exercise where we can simulate SPA with the `App` component returning different views based on its internal state. With only two-three views in hand this is still manageable, but what happens when there are ten or more views, each depending on different set of conditions? The code of the `App` component will quickly explode into a confusing mess with complex state management and complicated show conditions. This is where **routing** comes in play.

The **goal** is separate the logic for each individual view from the `App` component as much as possible and refactor existing application structure so that components are grouped by their functions and roles.

We will exercise the following:

- extending the application with new components
- refactoring existing code
- configuring application routes
- creating meaningful routed (top-level) components
- passing route props to non-routed (child) components

<br>

### 4. Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

<br>

### 5. Todo API server

<br>

#### 5.1 Configuring the server

**Before you start make sure you have `ts-node` installed as a global package.**

You have been provided with the REST API Todo server, which is located in the `api` folder. Make sure you have the following `.env` file in the root `api` folder (or if you don't - create it) with the following keys

```txt
PORT=5555
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=root
DB_DATABASE_NAME=tododb
JWT_SECRET=s3cr37
JWT_EXPIRE_TIME=1000000
```

Make sure to change the settings of the file to match you DB setup.

Next you will need to create the `ormconfig.json` file in the `api` folder (if it doesn't exist already) with the following content:

```json
{
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "tododb",
    "synchronize": true,
    "entities": [
        "src/database/entities/**/*.ts"
    ],
    "migrations": [
        "src/database/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/database/entities",
        "migrationsDir": "src/database/migration"
    }
}
```

Again, make sure to change the settings of the file to match you DB setup.

As you know using TypeORM will generate the necessary database objects. Still, do not forget to:
  1. Start your MariaDB / MySQL server
  2. Create the schema (if it doesn't already exist) - **tododb**

**Before you start the server run `npm run seed`. This will generate the database tables and the initial server user which will be needed later. Do not be concerned with the details below for now.**

Finally, you cans start the server with `npm run start` (or `npm run start:dev` if you plan to make changes to it).

<br>

#### 5.2 Server endpoints

You will utilize the following endpoints:

- `GET /todos`
- `POST /todos`
- `PUT /todos/:id`

Detailed typing for request and response objects:

<br>

##### `GET /todos`

Request: no body

Response:

```ts
// array of
{
  id: number;
  name: string;
  due: string;
  isDone: boolean;
}
```

<br>

##### `POST /todos`

Request body:

```ts
{
  // name length: [2-20]
  name: string;
  due: string;
  isDone: boolean;
}
```

Response:

```ts
{
  id: number;
  name: string;
  due: string;
  isDone: boolean;
}
```

<br>

##### `PUT /todos/:id`

Request body (question marks indicate optional properties):

```ts
{
  name?: string;
  due?: string;
  isDone?: boolean;
}
```

Response:

```ts
{
  id: number;
  name: string;
  due: string;
  isDone: boolean;
}
```

<br>

### 6. What stays the same

There are a few components that will remain the same, we will only moving the to more suitable place in the app structure. Those components are:

- `Header`
- `AppError`
- `Loading`
- `Container`

<br>

### 7. Project structure

Right now all the components are places flat in the `components` folder. Up to this moment this works for us nicely, but now that we will be adding more components to the app, having a ton of components in just one folder is not very manageable. In order to make the code more scalable and to ease finding components you need to create the following folders inside the `component` folder:

- `Base` - This will hold the base components: `Container`, `Header` and the new `Sidebar` component we will talk about below
- `Pages` - This will hold the non-feature related *page* components: `AppError`, `Loading` and two new components - `Home` and `NotFound`
- `Todo` - This will hold the `todo` **feature** components: `Todo`, `CreateTodo` and tje new `AllTodos` and `EditTodo` components

If we had any other features (i.e. boards, users, etc.) they would go in separate feature folders.

<br>

### 8. Application routing

In order to add routing to the app, you need to install the `react-router-dom` package:

```bash
npm i react-router-dom
```

Then you need to add the `BrowserRouter` provider component as the outermost component in the `App` component:

```jsx
return (
  <div>
    <BrowserRouter>
      ...
    </BrowserRouter>
  </div>
)
```

Navigated (top-level) components have the routing props `history`, `location` and `match` injected automatically in their `props` object. You can access them easily:

```jsx
const ComponentName = ({ history, location, match }) => {
  ...
};
```

In order to gain access to these properties from a non-navigated (child) component you need to wrap it in the `withRouter` higher-oder component:

```jsx
const ChildComponent = ({ history, location, match }) => {
  ...
};

export default withRouter(ChildComponent);
```

You need to implement the following router structure using the `Switch`, `Route` and `Redirect` components (make sure you import `Redirect` from `react-router-dom`):

- `/` should navigate to `/home`
- `/home` should display the `Home` component
- `/todos` should display the `AllTodos` component
- `/todos/:id` should display the `EditTodo` component
- `/todos/create` should display the `CreateTodo` component
- `*` should cover all other cases of invalid routes and display the `NotFound` component

Make sure you order the routes correctly, otherwise entering `/todos/create` in the address bar might navigate to `/todos/:id`. Now, what might be the reason for that?

Important note: Don't forget SPA routing has nothing to do with server routing - the two are separate.

<br>

### 9. `Home` and `NotFound` components

You can style those components as you like. They don't need to say anything more that `Home page` and `Page not found!` respectfully.

<br>

### 10. The `AllTodos` component

The `AllTodos` is a navigated component that will be responsible for retrieving and displaying all existing todos from the Todos API. It has one child component - the `Todo` component.

You will need to make `AllTodos` a routed component and move the logic for retrieving and displaying todos from the `App` component to this component. Then `AllTodos` needs to use the `Todos` component to display individual todos, and should also be able to update the `isDone` status of a todo when the checkbox is clicked in a `Todo` component.

When the todos are still loading it needs to display the `Loading` component with a message, and if there is an error with the request it needs to display the `AppError` component with the error message.

It should also display the `Create Todo` button and when the button is clicked it should navigate to `/todos/create`.

You have almost all of the logic implemented in the `App` component - you just need to move it to the `AllTodos` component.

<br>

### 11. The `Todo` component

The only change that needs to be done to the `Todo` component is adding a `Edit` button, which should navigate to the `EditTodo` component - `/todos/:id` where `:id` is the id of the displayed todo.

<br>

### 12. The `EditTodo` component

The `EditTodo` component is a navigated components. It depends on the route parameter `:id`, so it needs to read it from route params in the `match` prop.

This component should load the todo with the correct id from the Todos API, display the edit inputs for each todo property. When the data is loading or if there is an error it should display the appropriate views.

It should have `Update` and `Back` buttons. When they are clicked:

- `Update` should make a call to the API and update the todo (make sure you have validation on the todo before you pass it to the server) and should redirect to `/todos` if the operation is successful
- `Back` should navigate to the last view, i.e. `/todos`

You have most of the logic already in place in another component.

<br>

### 13. The `CreateTodo` component

The logic for creating todos already exist in another component, you just need to move it to this component.

When a new todo is created, it should navigate to the list of all todos. It should display the appropriate view when the request errors out.

<br>

### 14. The `Sidebar` component

Finally, we will put the links to the different "pages" in the `Sidebar` component which can be styled as a left side pane right under the application header, and left of the main content container. You can use whatever styles and layout you prefer.

It should contain the following links:

- `Home` (`/home`)
- `All todos` (`/todos`)
- `Create todo` (`/todos/create`)

<br>

### 15. The `App` component

When all refactoring is done and logic is moved to the navigated component the `App` component should only return the application skeleton view with the `Switch` component covering for the main view of the app. No logic should remain in the `App` component, keeping its code simple and clean:

```js
// imports here

const App = () => {

  return (
    <div>
      <BrowserRouter>
        <Header />
        <div className="page">
          <div className="sidebar">
            <Sidebar />
          </div>
          <div className="main-content">
            <Container>
              <Switch>
                ... routes here
              </Switch>
            </Container>
          </div>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
```
