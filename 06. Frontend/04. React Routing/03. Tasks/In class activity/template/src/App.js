import React, { useState, useEffect } from 'react';
import { BASE_URL } from './common/constants';
import Header from './components/Header/Header';
import AppError from './components/AppError/AppError';
import Todo from './components/Todo/Todo';
import CreateTodo from './components/CreateTodo/CreateTodo';
import Loading from './components/Loading/Loading';
import Container from './components/Container/Container';
import './App.css';

const App = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [appTodos, updateTodos] = useState([]);
  const [isCreateFormVisible, toggleCreateForm] = useState(false);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/todos`)
      .then(response => response.json())
      .then(result => {
        if (Array.isArray(result)) {
          updateTodos(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch(error => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const toggle = id => {
    const todoToUpdate = appTodos.find(todo => todo.id === id);
    if (!todoToUpdate) {
      setError(`No todo with id ${id} found!`);
      return;
    }
    fetch(`${BASE_URL}/todos/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isDone: !todoToUpdate.isDone,
      }),
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }

        const updatedTodos = appTodos.map(todo => {
          if (todo.id === result.id) {
            return result;
          }

          return todo;
        });

        updateTodos(updatedTodos);
      })
      .catch(error => setError(error.message))
      .finally(() => setLoading(false));
  };

  const createTodo = ({ name, due, isDone}) => {
    toggleCreateForm(false);
    setLoading(true);

    fetch(`${BASE_URL}/todos`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name,
        due,
        isDone,
      }),
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }

        updateTodos([...appTodos, result]);
      })
      .catch(error => setError(error.message))
      .finally(() => setLoading(false));
  };

  const hideCreateForm = () => toggleCreateForm(false);

  // show loading "page"
  if (loading) {
    return (
      <div>
        <Header />
        <Container>
          <Loading>
            <h1>Loading todos...</h1>
          </Loading>
        </Container>
      </div>
    )
  }

  // show error "page"
  if (error) {
    return (
      <div>
        <Header />
        <Container>
          <AppError message={error} />
        </Container>
      </div>
    )
  }

  // show create todo "page"
  if (isCreateFormVisible) {
    return (
      <div>
        <Header />
        <Container>
          <CreateTodo create={createTodo} close={hideCreateForm} />
        </Container>
      </div>
    )
  }

  // show all-todos "page"
  return (
    <div>
      <Header />
      <Container>
        <button id="create-todo-btn" onClick={() => toggleCreateForm(true)}>Create Todo</button>
        {appTodos && appTodos.map(todo => <Todo key={todo.id} todo={todo} toggle={toggle} />)}
      </Container>
    </div>
  );
}

export default App;
