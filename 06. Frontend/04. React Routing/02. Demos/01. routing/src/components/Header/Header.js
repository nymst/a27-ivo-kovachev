import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css';

const Header = () => {
  return (
    <div>
      <h2>My Awesome Application!</h2>

      <div className="navigation">
        <NavLink to="/home">Home</NavLink>
        <NavLink to="/posts">Posts</NavLink>
      </div>
    </div>
  );
};

export default Header;
