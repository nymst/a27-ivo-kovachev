import React, { useState, useEffect } from "react";
import Loader from '../Loader/Loader';
import PostDetails from './PostDetails';

const Posts = () => {

  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch("http://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((data) => setPosts(data))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const handleDeletePost = id => {
    setLoading(true);

    fetch(`http://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "DELETE",
      mode: "cors",
    })
      .then((response) => response.json())
      .then(() => {
        const index = posts.findIndex((post) => post.id === id);
        const updatedPosts = posts.slice();
        updatedPosts.splice(index, 1);

        setPosts(updatedPosts);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }

  if (error) {
    return <h4><i>An error has occured: </i>{error}</h4>
  }

  if (loading) {
    return <Loader />
  }

  const transformedPosts = posts.map((post) => {
    return (
      <PostDetails key={post.id} {...post} deletePost={handleDeletePost} isIndividual={false} />
    );
  });

  return (
    <main>
      <div>Check out all of our amazing posts!</div>
      {posts.length ? (
        <ul>{transformedPosts}</ul>
      ) : (
          <div>No posts to show ...</div>
        )}
    </main>
  );
};

export default Posts;
