import React from 'react';
import { withRouter } from 'react-router-dom';

const PostDetails = (props) => {
  const handleButtonClick = () => props.history.goBack();
  const renderDetailsButton = () => {
    if (!props.isIndividual) {
      return (
        <div>
          <button style={{ borderRadius: '10px' }} onClick={() => props.deletePost(props.id)}>-</button>
          <button style={{ borderRadius: '10px' }} onClick={() => props.history.push(`/posts/${props.id}`)}>+</button>
        </div>
      )
    }
  }

  return (
    <div style={{ marginBottom: 15 }}>
      {renderDetailsButton()}
      <div>Title: {props.title}</div>
      <div>Body: {props.body}</div>
      {props.isIndividual && <button onClick={handleButtonClick}>Back</button>}
    </div>
  );
};

export default withRouter(PostDetails);
