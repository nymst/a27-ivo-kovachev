import React from "react";
import Header from "./components/Header/Header";
import Posts from "./components/Posts/Posts";
import Post from "./components/Posts/Post";
import Home from './components/Home';
import NotFound from './components/NotFound';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

const App = () => {

  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Redirect path="/" exact to="/home" />
        <Route path="/home" component={Home} />
        <Route path="/posts" exact component={Posts} />
        <Route path="/posts/:id" component={Post} />
        <Route path="*" component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
