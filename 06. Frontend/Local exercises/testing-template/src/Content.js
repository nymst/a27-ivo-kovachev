import React from 'react';

const Content = props => {
  return (
    <>
      <div>Content: {props.number}</div>
      <div>{props.description}</div>
      <div>{props.story}</div>
    </>
  );
};

export default Content;
