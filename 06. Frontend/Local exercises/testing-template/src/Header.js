import React from 'react';

const Header = props => {
  // document.title = 'HUI';

  return (
    <div>
      <h1>Welcome!</h1>
      {props.children}
    </div>
  );
};

export default Header;
