import React, { useEffect, useState } from 'react';
import Header from './Header';
import Content from './Content';

const initialTitle = document.title;

const App = () => {
  const contents = [
    {
      number: 1,
      description: "Sucks",
      story: "Worst story ever"
    },
    {
      number: 2,
      description: "Sucks more",
      story: "Worster story ever"
    },
    {
      number: 3,
      description: "Sucks the most",
      story: "Worstest story ever"
    }
  ]

  const user = {
    name: 'BaiHui',
  }

  const Counter = (props) => {
    const [count, setCount] = useState(0);

    return (
      <div>
        <p>{props.name} clicked {count} times</p>
        <button onClick={() => setCount(count + 1)}>Click me</button>
      </div>
    )
  }

  const HandleDocumentTitle = () => {
    const [title, setTitle] = useState(initialTitle);
    console.log(title);

    return (
      <div>
        <button onClick={() => setTitle(title === initialTitle ? document.title = 'asd' : document.title = initialTitle)}> CLICK FOR HUI </button>
      </div>
    )
  }

  return (
    <div>
      <Header />
      {contents.map((c) => {
        return (
          <Content number={c.number} description={c.description} />
        )
      })}
      <Counter name={user.name} />
      <br />
      <HandleDocumentTitle />
    </div>
  )
};

export default App;
