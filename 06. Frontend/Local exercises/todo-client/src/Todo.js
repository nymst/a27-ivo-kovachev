
const Todo = ({ todo, changeTodo }) => {
  return (
    <div className="todo">
      <input type="checkbox" checked={todo.isDone} onChange={() => changeTodo(todo)} />
      <strong>{todo.name}</strong>
      <span className="date">{todo.dueDate}</span>
    </div>
  ) 
}

export default Todo;