import { useState } from "react";

const CreateTodo = ({ addTodo }) => {
  const [name, setName] = useState('');
  const [dueDate, setDueDate] = useState('')
  const [nameIsDirty, setNameIsDirty] = useState(false);
  const [dateIsDirty, setDateIsDirty] = useState(false);

  const handleClick = () => {
    if (!name || !dueDate) {
      console.log('not valid');
    } else {
      addTodo(name, dueDate)
      setName('');
      setDueDate('');
      setNameIsDirty(false);
      setDateIsDirty(false);
    }
  }

  const handleNameChange = (value) => {
    setName(value);
    setNameIsDirty(true);
  }

  const handleDateChange = (value) => {
    setDueDate(value);
    setDateIsDirty(true);
  }

  return (
    <>
      Name: <input
        value={name}
        className={(nameIsDirty && !name) ? "invalid" : ""}
        type="text"
        onChange={(e) => { handleNameChange(e.target.value); }} />
      Date: <input
        value={dueDate}
        className={(dateIsDirty && !dueDate)? "invalid" : ""}
        type="date"
        onChange={(e) => { handleDateChange(e.target.value); }} />
      <button onClick={handleClick}>Create Todo</button>
    </>
  )
}

export default CreateTodo