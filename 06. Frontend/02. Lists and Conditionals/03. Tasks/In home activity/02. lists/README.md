<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 06. Lists

### 1. Lists

Now it is time to stop copy-paste-ing MoviesDetails component and use list to render all the movies.

1. Replace the hard-coded **MovieDetails** with a mapping of the movies array
   
    ```js
      movies.map(movie => <MovieDetails key={movie.imdbID} {...movie} />)
    ```

2. Your `moviesToShow` variable should look like this:

    ```js
      const moviesToShow =
        movieRecommendationIndex !== null ? (
          <div>
            <div>Our recommendation:</div>
            <MovieDetails {...movies[movieRecommendationIndex]} />
          </div>
        ) : (
          movies.map(movie => <MovieDetails key={movie.imdbID} {...movie} />)
        );
    ```

3. Pay attention that we use movie's `ID` for list item's **key**. Remember that:
   1. Key is a unique property that helps React identify which items have changed, are added, or are removed. 
   2. The key should always be a unique value amongst its siblings in a given array. 
   3. The best keys are the ids from your data set. 