<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 05. Conditionals

### 1. Conditionals

In React it is often the case that we want to render different parts of the UI or even different components depending on our current state. In our movies project we want to have conditional views depending on correctness of the recommendation movie. 

If we get index for recommendation that is valid, we'll render details for this movie, otherwise the whole list will appear.

1. Change default recommendation and start the **movieRecommendationIndex** with initial value of **null**.

      ```js
      const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(null);
      ```

2. Create a **moviesToShow** variable - if the **movieRecommendationIndex** is valid then show a detailed view of the recommendation else, show all of the movies:

    ```js
      const moviesToShow =
      movieRecommendationIndex !== null ? (
        <div>
          <div>Our recommendation:</div>
          <MovieDetails {...movies[movieRecommendationIndex]} />
        </div>
      ) : (
        <Fragment>
          <MovieDetails {...movies[0]} />
          <MovieDetails {...movies[1]} />
          <MovieDetails {...movies[2]} />
        </Fragment>
      );
    ```

3. Here we see unfamiliar component - [**Fragment**](https://reactjs.org/docs/fragments.html). 
   It is a common pattern in React for a component to return multiple elements. But we cannot return multiple elements, remember? **Fragments** in React let you group a list of children without adding extra nodes to the DOM.

   ```html
    <Fragment>
      <MovieDetails {...movies[0]} />
      <MovieDetails {...movies[1]} />
      <MovieDetails {...movies[2]} />
    </Fragment>
   ```

4. Let's have another button - **clearRecommendations** button that will reset the recommendations. This way we'll have two buttons:

    ```html
    <button onClick={recommendMovie}>Recommend a movie</button>
    ```

    ```html
    <button onClick={clearRecommendations}>Clear recommendations</button>
    ```

5. Add `event handlers` for both of them:

    ```js
    const recommendMovie = () => {
      const validMovieIndex = Math.floor(Math.random() * movies.length);
      setMovieRecommendationIndex(validMovieIndex);
    };
    ```

    ```js
    const clearRecommendations = () => {
      setMovieRecommendationIndex(null);
    };
    ```

6. Now, using JSX interpolation and `moviesToShow` variable render conditionally the movies.

7. If you have done the job right, your Movies component should look like this:

    ```js
    import React, { useState, Fragment } from 'react';
    import MovieDetails from './MovieDetails';

    const Movies = () => {
      const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(
        null
      );

      const movies = [
        {
          imdbID: 'tt2278388',
          title: 'The Grand Budapest Hotel',
          year: '2014',
          type: 'movie',
          poster:
            'https://m.media-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg'
        },
        {
          imdbID: 'tt5712554',
          title: 'The Grand Tour',
          year: '2016–',
          type: 'series',
          poster:
            'https://m.media-amazon.com/images/M/MV5BYjkyOWIyZGYtYzU3ZS00NWM2LThjZGEtMDZjZjg2MTI2NzBhXkEyXkFqcGdeQXVyNjI4OTg2Njg@._V1_SX300.jpg'
        },
        {
          imdbID: 'tt2039345',
          title: 'Grand Piano',
          year: '2013',
          type: 'movie',
          poster:
            'https://m.media-amazon.com/images/M/MV5BMjEyNzkwMDMxM15BMl5BanBnXkFtZTgwOTM0MzE4MDE@._V1_SX300.jpg'
        }
      ];

      const recommendMovie = () => {
        const validMovieIndex = Math.floor(Math.random() * movies.length);
        setMovieRecommendationIndex(validMovieIndex);
      };

      const clearRecommendations = () => {
        setMovieRecommendationIndex(null);
      };

      const moviesToShow =
        movieRecommendationIndex !== null ? (
          <div>
            <div>Our recommendation:</div>
            <MovieDetails {...movies[movieRecommendationIndex]} />
          </div>
        ) : (
          <Fragment>
            <MovieDetails {...movies[0]} />
            <MovieDetails {...movies[1]} />
            <MovieDetails {...movies[2]} />
          </Fragment>
        );

      return (
        <div>
          <div>
            <button onClick={recommendMovie}>Recommend a movie</button>
            <button onClick={clearRecommendations}>Clear recommendations</button>
          </div>

          <br />

          {moviesToShow}
        </div>
      );
    };

    export default Movies;

    ```

