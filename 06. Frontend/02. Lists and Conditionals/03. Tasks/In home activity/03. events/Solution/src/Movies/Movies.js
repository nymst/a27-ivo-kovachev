import React, { useState, Fragment } from 'react';
import MovieDetails from './MovieDetails';

const Movies = () => {
  const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(
    null
  );
  const [movies, setMovies] = useState([
    {
      imdbID: 'tt2278388',
      title: 'The Grand Budapest Hotel',
      year: '2014',
      type: 'movie',
      poster:
        'https://m.media-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg'
    },
    {
      imdbID: 'tt5712554',
      title: 'The Grand Tour',
      year: '2016–',
      type: 'series',
      poster:
        'https://m.media-amazon.com/images/M/MV5BYjkyOWIyZGYtYzU3ZS00NWM2LThjZGEtMDZjZjg2MTI2NzBhXkEyXkFqcGdeQXVyNjI4OTg2Njg@._V1_SX300.jpg'
    },
    {
      imdbID: 'tt2039345',
      title: 'Grand Piano',
      year: '2013',
      type: 'movie',
      poster:
        'https://m.media-amazon.com/images/M/MV5BMjEyNzkwMDMxM15BMl5BanBnXkFtZTgwOTM0MzE4MDE@._V1_SX300.jpg'
    }
  ]);

  const recommendMovie = () => {
    const validMovieIndex = Math.floor(Math.random() * movies.length);
    setMovieRecommendationIndex(validMovieIndex);
  };

  const clearRecommendations = () => {
    setMovieRecommendationIndex(null);
  };

  const deleteMovie = movieId => {
    const filteredMovies = movies.filter(movie => movie.imdbID !== movieId);
    setMovies(filteredMovies);
  };

  const moviesToShow =
    movieRecommendationIndex !== null ? (
      <div>
        <div>Our recommendation:</div>
        <MovieDetails {...movies[movieRecommendationIndex]} />
      </div>
    ) : (
      movies.map(movie => {
        return (
          <MovieDetails
            key={movie.imdbID}
            {...movie}
            deleteMovie={() => deleteMovie(movie.imdbID)}
          />
        );
      })
    );

  return (
    <div>
      <div>
        <button onClick={recommendMovie}>Recommend a movie</button>
        <button onClick={clearRecommendations}>Clear recommendations</button>
      </div>

      <br />

      {moviesToShow}
    </div>
  );
};

export default Movies;
