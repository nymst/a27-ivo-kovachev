<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 07. Events

### 1. Events

In order to pass information from child to parent, pass a callback from the parent as props to the child. The child now has full control on when and how to call the callback 

1. Get a `deleteMovie` callback as props in the **MovieDetails** component. Do not forget to add a default implementation for the callback in the **MovieDetails** to cover the "recommendation" case:

    ```js
    const deleteMovie = props.deleteMovie || (() => {});
    ```
   
2. Add delete function for each movie - attach to its container a click event - the `deleteMovie` callback:

    ```js
    return (
      <div id={props.imdbID} onClick={deleteMovie}>
        <img src={props.poster} alt="poster" />
        <div>{props.title}</div>
        <div>
          Year: {props.year} | Type: {props.type}
        </div>
      </div>
    );
    ```
3. Add **prop-types** for the new callback prop:

    ```js
    deleteMovie: PropTypes.func
    ```

4. Now the **MovieDetails** component should look like this:

    ```js
    import React from 'react';
    import PropTypes from 'prop-types';

    const MovieDetails = props => {
      const deleteMovie = props.deleteMovie || (() => {});

      return (
        <div id={props.imdbID} onClick={deleteMovie}>
          <img src={props.poster} alt="poster" />
          <div>{props.title}</div>
          <div>
            Year: {props.year} | Type: {props.type}
          </div>
        </div>
      );
    };

    MovieDetails.propTypes = {
      imdbID: PropTypes.string,
      poster: PropTypes.string,
      title: PropTypes.string,
      year: PropTypes.string,
      type: PropTypes.string,
      deleteMovie: PropTypes.func
    };

    export default MovieDetails;
    ```

5. Refactor the movies array in **Movies** component to be stateful by adding the array to the state (use **useState**):

    ```js
      const [movies, setMovies] = useState([
      {
        imdbID: 'tt2278388',
        title: 'The Grand Budapest Hotel',
        year: '2014',
        type: 'movie',
        poster:
          'https://m.media-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg'
      },
      {
        imdbID: 'tt5712554',
        title: 'The Grand Tour',
        year: '2016–',
        type: 'series',
        poster:
          'https://m.media-amazon.com/images/M/MV5BYjkyOWIyZGYtYzU3ZS00NWM2LThjZGEtMDZjZjg2MTI2NzBhXkEyXkFqcGdeQXVyNjI4OTg2Njg@._V1_SX300.jpg'
      },
      {
        imdbID: 'tt2039345',
        title: 'Grand Piano',
        year: '2013',
        type: 'movie',
        poster:
          'https://m.media-amazon.com/images/M/MV5BMjEyNzkwMDMxM15BMl5BanBnXkFtZTgwOTM0MzE4MDE@._V1_SX300.jpg'
      }
    ]);
    ```

6. In the **Movies** component implement the logic for removing the movie with a specific id. Update movies in the state using `setMovies` function.

    ```js
    const deleteMovie = movieId => {
      const filteredMovies = movies.filter(movie => movie.imdbID !== movieId);
      setMovies(filteredMovies);
    };
    ```

7. Create and pass a callback from the parent during the mapping of **MovieDetails**

    ```html
      <MovieDetails
        key={movie.imdbID}
        {...movie}
        deleteMovie={() => deleteMovie(movie.imdbID)}
      />
    ```

8. Event handlers are simple **closures**. This way we are sure that the correct parameters are passed.

9. Now, if everything's correct your Movies component should look like this:

    ```js
    import React, { useState, Fragment } from 'react';
    import MovieDetails from './MovieDetails';

    const Movies = () => {
      const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(
        null
      );
      const [movies, setMovies] = useState([
        {
          imdbID: 'tt2278388',
          title: 'The Grand Budapest Hotel',
          year: '2014',
          type: 'movie',
          poster:
            'https://m.media-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg'
        },
        {
          imdbID: 'tt5712554',
          title: 'The Grand Tour',
          year: '2016–',
          type: 'series',
          poster:
            'https://m.media-amazon.com/images/M/MV5BYjkyOWIyZGYtYzU3ZS00NWM2LThjZGEtMDZjZjg2MTI2NzBhXkEyXkFqcGdeQXVyNjI4OTg2Njg@._V1_SX300.jpg'
        },
        {
          imdbID: 'tt2039345',
          title: 'Grand Piano',
          year: '2013',
          type: 'movie',
          poster:
            'https://m.media-amazon.com/images/M/MV5BMjEyNzkwMDMxM15BMl5BanBnXkFtZTgwOTM0MzE4MDE@._V1_SX300.jpg'
        }
      ]);

      const recommendMovie = () => {
        const validMovieIndex = Math.floor(Math.random() * movies.length);
        setMovieRecommendationIndex(validMovieIndex);
      };

      const clearRecommendations = () => {
        setMovieRecommendationIndex(null);
      };

      const deleteMovie = movieId => {
        const filteredMovies = movies.filter(movie => movie.imdbID !== movieId);
        setMovies(filteredMovies);
      };

      const moviesToShow =
        movieRecommendationIndex !== null ? (
          <div>
            <div>Our recommendation:</div>
            <MovieDetails {...movies[movieRecommendationIndex]} />
          </div>
        ) : (
          movies.map(movie => {
            return (
              <MovieDetails
                key={movie.imdbID}
                {...movie}
                deleteMovie={() => deleteMovie(movie.imdbID)}
              />
            );
          })
        );

      return (
        <div>
          <div>
            <button onClick={recommendMovie}>Recommend a movie</button>
            <button onClick={clearRecommendations}>Clear recommendations</button>
          </div>

          <br />

          {moviesToShow}
        </div>
      );
    };

    export default Movies;

    ```

10. Run the application.
    
11. There is a "real-time" resizing of the array - this is because **setMovies** function updates movies in the state. Every update of the state causes re-rendering of the component that depends on the changed data. 

!['events'](./imgs/events.gif)