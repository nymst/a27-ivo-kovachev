import React, { useState } from 'react';
import './App.css';
import Header from './components/Header/Header';
import todos from './data/todos';
import Todo from './components/Todo/Todo';

const App = () => {
  const [appTodos, updateTodos] = useState(todos);

  const toggle = id => {
    if (appTodos[id]) {
      const updatedTodos = appTodos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            isDone: !todo.isDone
          }
        }

        return {...todo};
      });

      updateTodos(updatedTodos);
    }
  }

  return (
    <div>
      <Header />
      <div className="Todo-container">
        <Todo todo={appTodos[0]} toggle={toggle} />
        <Todo todo={appTodos[1]} toggle={toggle} />
        <Todo todo={appTodos[2]} toggle={toggle} />
      </div>
    </div>
  );
}

export default App;
