import React, { useState } from 'react';
import Header from './Header';
import Content from './Content';

const initialStories = [
  { number: 1, story: 'Story 1', description: 'Interesting story' },
  { number: 2, story: 'Story 2', description: 'Another one' },
  { number: 3, story: 'Story 3', description: 'Amazing story here!' }
];

const App = () => {
  const [stories, setStories] = useState([]);

  const loadStories = () => setStories(initialStories);
  const deleteStory = id => {
    const index = stories.findIndex(s => s.number === id);
    const updatedStories = stories.slice();
    updatedStories.splice(index, 1)
    setStories(updatedStories);
  };

  const appContent = stories.length ? (
    <Content stories={stories} deleteStory={deleteStory} />
  ) : (
      <h3>No stories to show...</h3>
    );

  return (
    <div>
      <Header />
      <button onClick={loadStories}>Load stories</button>
      {appContent}
    </div>
  );
};

export default App;