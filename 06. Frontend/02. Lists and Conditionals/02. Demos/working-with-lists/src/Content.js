import React from 'react';

const Content = props => {
  return (
    <ul>
      {props.stories.map(story => {
        return <li key={story.number}>
          <h2>{story.story}</h2>
          <div>{story.description}</div>
        </li>
      })}


    </ul>
  );
};

export default Content;
