import React from 'react';

const PersonName = ({ name, removePerson }) => {
  return (
    <div>
      <p>Name: {name} <button style={{ marginLeft: "10px" }} onClick={() => removePerson(name)}>X</button></p>
    </div>
  );
}

export default PersonName;