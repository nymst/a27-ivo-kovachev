import React, { useState } from 'react';

const AddPerson = ({ addPerson }) => {
  const [name, setName] = useState('');

  const add = () => {
    // validation
    if (!name){
      alert('Invalid name');
      return;
    }

    addPerson(name);
  }

  return (
    <div>
      Name: <input type="text" id="add-name" name="person-name" onChange={e => setName(e.target.value)} />
      <button onClick={add}>+</button>
    </div>
  );
}

export default AddPerson;