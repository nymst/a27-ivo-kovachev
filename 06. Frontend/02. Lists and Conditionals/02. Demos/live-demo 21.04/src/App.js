import React, { useState } from 'react';
import './App.css';
import AddPerson from './components/AddPerson/AddPerson';
import Header from './components/Header/Header';
import PersonName from './components/PersonName/PersonName';

function App() {

  // const [count, setCount] = useState(0);
  // const [name, setName] = useState(0);

  // const increment = (val) => {
  //   setCount(prev => ++prev);
  //   setName(val);
  // }

  // return (
  //   <div className="App" >
  //     <Header />
  //     <h1 onClick={increment}>Count: {count}</h1>
  //     <h1>Name: {name}</h1>
  //     <input type="text" onChange={e => increment(e.target.value)}/>

  //   </div>
  // );

  const [names, setNames] = useState([`Pesho`, `Gosho`, `Tosho`]);

  const canAddPeople = names.length < 5;

  const removePerson = person => {
    const newNames = names.filter(p => p !== person);
    setNames(newNames);
  }

  const addPerson = name => setNames([...names, name]);



  return (
    <div className="App" >
      <Header />
      {names.length > 0 && names.map((n, i) => <PersonName removePerson={removePerson} name={n} key={i} />)}
      { canAddPeople ? <AddPerson addPerson={addPerson} /> : <h2>List is full!</h2>}
    </div>
  );
}

export default App;