import React, { useState } from 'react';
import Header from './Header';
import Content from './Content';

const App = () => {
  const empty = { number: 0 };
  const [story, setStory] = useState(empty);

  const deleteStory = () => setStory(empty);
  const addStory = () => setStory((prevStory) => {
    return {
      number: prevStory.number + 1,
      story: 'My story!',
      description: 'This is another amazing story!'
    }
  });

  const appContent = story.number ? (
    <Content {...story} />
  ) : (
      <h2>No stories to show..</h2>
    );

  return (
    <div>
      <Header />
      <button onClick={addStory}>Create story</button>
      <button onClick={deleteStory}>Delete story</button>
      {appContent}
    </div >
  );
};

export default App;