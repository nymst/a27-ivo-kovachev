import React from 'react';

const Content = props => {
  return (
    <main>
      <h2>{props.story}</h2>
      <div>Content: {props.number}</div>
      <div>{props.description}</div>

    </main>
  );
};

export default Content;
