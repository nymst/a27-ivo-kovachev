<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [Conditional Rendering](https://reactjs.org/docs/conditional-rendering.html)
- [List and Keys](https://reactjs.org/docs/lists-and-keys.html)
- [Handling Events](https://reactjs.org/docs/handling-events.html)
- [Why do I need Keys in React Lists?](https://medium.com/@adhithiravi/why-do-i-need-keys-in-react-lists-dbb522188bbb)
