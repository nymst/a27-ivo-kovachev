import { Length, IsOptional, IsString } from 'class-validator';

export class UpdateUserDTO {
  @IsOptional()
  @Length(2, 20)
  public username: string;

  @IsOptional()
  @IsString()
  public avatarUrl: string;
}
