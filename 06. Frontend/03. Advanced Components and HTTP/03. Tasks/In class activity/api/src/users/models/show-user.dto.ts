import { User } from "src/database/entities/user.entity";

export class ShowUserDTO {
  constructor(user: User) {
    this.username = user.username;
    this.avatarUrl = user.avatarUrl;
    this.roles = user.roles 
      ? user.roles.map(r => r.name) 
      : [];
  }

  public avatarUrl: string;

  public username: string;

  public roles: string[];
}
