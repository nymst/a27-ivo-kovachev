import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import {
  Controller,
  Post,
  Body,
  UseInterceptors,
  UploadedFile,
  Param,
  HttpStatus,
  HttpCode,
  Put,
  UseGuards,
} from '@nestjs/common';
import { UsersDataService } from './users-data.service';
import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { UserRole } from './enums/user-role.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateUserDTO } from './models/update-user.dto';
import { AdminGuard } from '../common/guards/admin.guard';

@Controller('users')
export class UsersController {
  public constructor(private readonly usersDataService: UsersDataService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async addNewUser(@Body() user: CreateUserDTO): Promise<ShowUserDTO> {
    return await this.usersDataService.createUser(user, UserRole.Basic);
  }

  @Put('/:id/roles')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async updateUserRoles(
    @Param('id') userId: string,
    @Body() updateUserRoles: UpdateUserRolesDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersDataService.updateUserRoles(userId, updateUserRoles);
  }

  @Post('/:id/avatar')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file'))
  public async uploadUserAvatar(
    @Param('id') id: string,
    @UploadedFile() file: any,
  ): Promise<ShowUserDTO> {
    const updateUserProperties: Partial<UpdateUserDTO> = {
      avatarUrl: file?.filename,
    };

    return await this.usersDataService.updateUser(id, updateUserProperties);
  }
}
