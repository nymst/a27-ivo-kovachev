import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { CreateUserDTO } from './models/create-user.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/role.entity';
import { UserRole } from './enums/user-role.enum';
import { ShowUserDTO } from './models/show-user.dto';
import { TodosSystemError } from '../common/exceptions/todos-system.error';
import { UpdateUserDTO } from './models/update-user.dto';

@Injectable()
export class UsersDataService {
  public constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
  ) {}

  public async createUser(
    user: CreateUserDTO,
    ...roles: UserRole[]
  ): Promise<ShowUserDTO> {
    const userRolesFiltering = roles.map(role => ({ name: role }));
    const roleEntities: Role[] = this.rolesRepository.create(
      userRolesFiltering,
    );
    const savedRoleEntities: Role[] = await this.rolesRepository.save(
      roleEntities,
    );

    const foundUser: User = await this.usersRepository.findOne({
      username: user.username,
    });
    if (foundUser) {
      throw new TodosSystemError(
        'User with such username already exists!',
        400,
      );
    }

    const userEntity: User = this.usersRepository.create(user);
    userEntity.roles = savedRoleEntities;
    userEntity.todos = [];

    const createdUser: User = await this.usersRepository.save(userEntity);

    return new ShowUserDTO(createdUser);
  }

  public async updateUserRoles(
    id: string,
    updateUserRoles: UpdateUserRolesDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: User = await this.usersRepository.findOne({ id });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new TodosSystemError('No such user found', 404);
    }

    const roleEntities: Role[] = await this.rolesRepository.find({
      where: {
        name: In(updateUserRoles.roles),
      },
    });

    const entityToUpdate: User = { ...foundUser, roles: roleEntities };
    const updatedUser: User = await this.usersRepository.save(entityToUpdate);

    return new ShowUserDTO(updatedUser);
  }

  public async updateUser(
    id: string,
    user: Partial<UpdateUserDTO>,
  ): Promise<ShowUserDTO> {
    const foundUser: User = await this.usersRepository.findOne({ id });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new TodosSystemError('No such user found', 404);
    }

    const entityToUpdate: User = { ...foundUser, ...user };
    const updatedUser: User = await this.usersRepository.save(entityToUpdate);

    return new ShowUserDTO(updatedUser);
  }
}
