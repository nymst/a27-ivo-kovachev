import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('todos')
export class Todo {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, length: 50 })
  public name: string;

  @Column({ nullable: false, type: 'date' })
  public due: string;

  @Column({ nullable: false, default: false })
  public isDone: boolean;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @ManyToOne(type => User,user => user.todos)
  public user: User;
}
