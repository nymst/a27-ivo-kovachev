import { Todo } from './todo.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';
import { Role } from './role.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: string;

  @Column({ nullable: false, unique: true, length: 50 })
  public username: string;

  @Column({ nullable: false })
  public password: string;

  @Column({ nullable: true })
  public avatarUrl: string;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @OneToMany(type => Todo, todo => todo.user)
  public todos: Todo[];

  @ManyToMany(type => Role)
  @JoinTable()
  public roles: Role[];
}
