import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { TodosSystemError } from '../exceptions/todos-system.error';

@Catch(TodosSystemError)
export class TodosSystemErrorFilter implements ExceptionFilter {
  public catch(exception: TodosSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
