import { Controller, Get, HttpStatus, Redirect } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  @Redirect('/todos', HttpStatus.MOVED_PERMANENTLY)
  public root(): void {
    /* redirect to /todos */
  }
}
