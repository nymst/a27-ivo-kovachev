import { User } from './../database/entities/user.entity';
import { Module } from '@nestjs/common';
import { TodosController } from './todos.controller';
import { TodosDataService } from './todos-data.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todo } from '../database/entities/todo.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Todo, User])],
  controllers: [TodosController],
  providers: [TodosDataService],
})
export class TodosModule {}
