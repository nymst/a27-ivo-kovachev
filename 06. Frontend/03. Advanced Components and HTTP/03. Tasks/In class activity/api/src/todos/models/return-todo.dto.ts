export class ReturnTodoDTO {
  public id: number;
  public name: string;
  public due: string;
  public isDone: boolean;
}