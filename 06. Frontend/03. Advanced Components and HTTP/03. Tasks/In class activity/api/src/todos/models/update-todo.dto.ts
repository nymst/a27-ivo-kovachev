import { IsOptional, Length, IsString, IsBoolean } from 'class-validator';

export class UpdateTodoDTO {
  @IsOptional()
  @Length(2, 20)
  public name: string;

  @IsOptional()
  @IsString()
  public due: string;

  @IsOptional()
  @IsBoolean()
  public isDone: boolean;
}
