import { ShowUserDTO } from "src/users/models/show-user.dto";
import { Todo } from "src/database/entities/todo.entity";

export class TodoDTO {
  constructor(todo: Todo) {
    this.id = todo.id;
    this.name = todo.name;
    this.isDone = todo.isDone;
    this.due = todo.due;
    this.isDeleted = todo.isDeleted;
    this.user = todo.user 
      ? new ShowUserDTO(todo.user)
      : null;
  }

  public id: number;
  public name: string;
  public due: string;
  public isDone: boolean;
  public isDeleted: boolean;
  public user: ShowUserDTO;
}
