import { IsString, Length, IsBoolean } from 'class-validator';

export class CreateTodoDTO {
  @Length(2, 20)
  public name: string;

  @IsString()
  public due: string;

  @IsBoolean()
  public isDone: boolean;
}
