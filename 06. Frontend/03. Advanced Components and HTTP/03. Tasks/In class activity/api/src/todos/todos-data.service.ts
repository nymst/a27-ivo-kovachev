import { TodoDTO, CreateTodoDTO, UpdateTodoDTO } from './models';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../database/entities/todo.entity';
import { Repository } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { TodosSystemError } from '../common/exceptions/todos-system.error';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { ReturnTodoDTO } from './models/return-todo.dto';

@Injectable()
export class TodosDataService {
  public constructor(
    @InjectRepository(Todo) private readonly todosRepository: Repository<Todo>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  public async allTodos(withDeleted = false): Promise<ReturnTodoDTO[]> {
    const todos = withDeleted
      ? await this.todosRepository.find()
      : await this.todosRepository.find({
        where: { isDeleted: false },
        relations: ['user']
      });

    await new Promise(r => setTimeout(r, 1000));

    return todos.map(todo => this.toReturnTodoDTO(todo));
  }

  public async createTodo(
    todo: CreateTodoDTO,
    user: ShowUserDTO,
  ): Promise<ReturnTodoDTO> {
    const todoEntity: Todo = this.todosRepository.create(todo);
    const foundUser: User = await this.usersRepository.findOne({
      username: user.username,
    });

    if (foundUser === undefined || foundUser.isDeleted) {
      throw new TodosSystemError('User with such username does not exist', 400);
    }

    todoEntity.user = foundUser;
    const savedTodo: Todo = await this.todosRepository.save(todoEntity);

    return this.toReturnTodoDTO(savedTodo);
  }

  public async updateTodo(
    id: number,
    todo: Partial<UpdateTodoDTO>,
  ): Promise<ReturnTodoDTO> {
    const oldTodo: Todo = await this.findTodoById(id);
    const entityToUpdate: Todo = { ...oldTodo, ...todo };

    const savedTodo: Todo = await this.todosRepository.save(entityToUpdate);

    return this.toReturnTodoDTO(savedTodo);
  }

  public async deleteTodo(id: number): Promise<TodoDTO> {
    const foundTodo: Todo = await this.findTodoById(id);

    const savedTodo: Todo = await this.todosRepository.save({
      ...foundTodo,
      isDeleted: true,
    });

    return new TodoDTO(savedTodo);
  }

  private toReturnTodoDTO(todo: Todo): ReturnTodoDTO {
    return {
      id: todo.id,
      name: todo.name,
      due: todo.due,
      isDone: todo.isDone,
    };
  }

  private async findTodoById(todoId: number): Promise<Todo> {
    const foundTodo: Todo = await this.todosRepository.findOne(todoId);
    if (foundTodo === undefined || foundTodo.isDeleted) {
      throw new TodosSystemError('No such todo found', 404);
    }

    return foundTodo;
  }
}
