import React, { useState } from 'react';
import './App.css';
import Header from './components/Header/Header';
import todos, { nextTodoId } from './data/todos';
import Todo from './components/Todo/Todo';
import CreateTodo from './components/CreateTodo/CreateTodo';

const App = () => {
  const [appTodos, updateTodos] = useState(todos);
  const [isCreateFormVisible, toggleCreateForm] = useState(false);

  const toggle = id => {
    if (appTodos[id]) {
      const updatedTodos = appTodos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            isDone: !todo.isDone
          }
        }

        return {...todo};
      });

      updateTodos(updatedTodos);
    }
  };

  const createTodo = ({ name, due, isDone}) => {
    const updatedTodos = [
      ...appTodos,
      {
        id: nextTodoId(),
        name,
        due,
        isDone,
      },
    ];

    updateTodos(updatedTodos);
    toggleCreateForm(false);
  };

  const hideCreateForm = () => toggleCreateForm(false);

  return (
    <div>
      <Header />
      <div className="Todo-container">
        {isCreateFormVisible
          ? <CreateTodo create={createTodo} close={hideCreateForm} />
          : <button id="create-todo-btn" onClick={() => toggleCreateForm(true)}>Create Todo</button>}
        {appTodos.map(todo => <Todo key={todo.id} todo={todo} toggle={toggle} />)}
      </div>
    </div>
  );
}

export default App;
