import React from 'react';
import Layout from './components/Layout/Layout';
import Movies from './containers/Movies';

const App = () => {
  return (
    <Layout>
      <Movies />
    </Layout>
  );
};

export default App;
