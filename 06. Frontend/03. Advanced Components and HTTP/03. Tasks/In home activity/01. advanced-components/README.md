<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 08. Advanced Components

### 1. Advanced Components

In React components are often divided into 2 types: **presentational** components and **container** components.

Presentational components are mostly concerned with generating some markup to be outputted.
They don't manage any kind of state, except for state related the the presentation

Container components are mostly concerned with the "backend" operations.
They might handle the state of various sub-components. They might wrap several presentational components.

1. Let's structure our **Movies** project. It's up to you to decide how to organize your project. 
   1. Think and identify which components are presentational and which are container components and whether you'd like to use this logic in the structure.
   2. Note that the structure that you'll see in the solution eventually, is just **one possible** way to go.

In general, **higher order components** allow you to create code that's composable and reusable, and also more encapsulated. They allow us to specizlize existing generic components.

2. Introduce **Layout** higher order component that will hold the application's layout and will have a "placeholder" for the different pages (containers)

    ```js
      const Layout = props => {
        return (
          <Fragment>
            <Header />
            <hr />
            <main>{props.children}</main>
          </Fragment>
        );
      };
    ```

3. Create another high order component that will help us with Button styles. In our page there are several buttons and we always can add components that have buttons. Our **withPrettyButtons** component will make buttons across the components same.
   1. Create **withPrettyButtons** component
   2. Think where you'll add it.
   3. It will accept another component that acceps props, will style its buttons and render it.
   4. Choose button styling by your taste. You can inspire yourself from [Button CSS Generator](https://www.bestcssbuttongenerator.com/)
   5. If you've done your job correctly it should look something like this:

      ```js
        import React from 'react';
        import './styles/button.css';

        const withPrettyButtons = (BaseComponent) => (props) => {
          return (
            <div className="myButton">
              <BaseComponent {...props} />
            </div>
          );
        }

        export default withPrettyButtons;
      ```

6. Now use it in **Movies** component as we want to have its buttons styled:
      ```js
        import React, { useState } from 'react';
        import withPrettyButtons from '../components/Layout/withPrettyButtons';
        import MovieDetails from '../components/Movies/MovieDetails';

        const Movies = () => {..}

        export default withPrettyButtons(Movies);
      ```
7. You wanna make sure this is useful? Create **Footer** component that will have **Contact us** button and use **withPrettyButtons** component to reuse styling:
    ```js
      import React from 'react';
      import withPrettyButtons from '../Layout/withPrettyButtons';

      const Footer = () => {
        const handleClick = () => {
          alert(`We'll be with you shortly`);
        }
        return <button onClick={handleClick}>Contact us</button>;
      };

      export default withPrettyButtons(Footer);
    ```
     1. Why do we need the **handleClick** function? What is the issue with directly assigning the alert to the onClick event? Remember that this is **jsx expression** :)

8. Now add the **Footer** in your **Layout** component. Is this the best place for it? Why?

    ```js
        const Layout = props => {
        return (
          <Fragment>
            <Header />
            <hr />
            <main>{props.children}</main>
            <hr />
            <Footer />
          </Fragment>
        );
      };
    ```
9. Start the application and enjoy your buttons.



