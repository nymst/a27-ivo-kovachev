import React from 'react';
import withButtons from '../Layout/withButtons';

const Footer = () => {
  const handleClick = () => {
    alert(`We'll be with you shortly`);
  }
  return <button onClick={handleClick}>Contact us</button>;
};

export default withButtons(Footer);
