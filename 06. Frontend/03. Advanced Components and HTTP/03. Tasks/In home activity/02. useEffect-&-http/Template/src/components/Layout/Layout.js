import React, { Fragment } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

const Layout = props => {
  return (
    <Fragment>
      <Header />
      <hr />
      <main>{props.children}</main>
      <hr />
      <Footer />
    </Fragment>
  );
};

export default Layout;
