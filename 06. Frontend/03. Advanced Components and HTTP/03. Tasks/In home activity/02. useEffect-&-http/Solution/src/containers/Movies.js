import React, { useState, useEffect } from 'react';
import { CONSTANTS } from '../constants/constants';
import withButtons from '../components/Layout/withButtons';
import MovieDetails from '../components/Movies/MovieDetails';
import Loader from '../components/Loader/Loader';

const Movies = () => {
  const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(
    null
  );
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const search = '<>';

  useEffect(() => {
    setLoading(true);

    fetch(`${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&s=${search}`)
      .then(response => response.json())
      .then(data => {
        if (data.Response === 'True') {
          setMovies(data.Search);
        } else if (data.Response === 'False') {
          setError(data.Error);
        }
      })
      .finally(() => setLoading(false));
  }, []);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <h1 style={{ color: 'Red' }}>{error}</h1>;
  }

  const recommendMovie = () => {
    const validMovieIndex = Math.floor(Math.random() * movies.length);
    setMovieRecommendationIndex(validMovieIndex);
  };

  const clearRecommendations = () => {
    setMovieRecommendationIndex(null);
  };

  const deleteMovie = movieId => {
    const filteredMovies = movies.filter(movie => movie.imdbID !== movieId);
    setMovies(filteredMovies);
  };

  const moviesToShow =
    movieRecommendationIndex !== null ? (
      <div>
        <div>Our recommendation:</div>
        <MovieDetails {...movies[movieRecommendationIndex]} />
      </div>
    ) : (
        movies.map(movie => {
          return (
            <MovieDetails
              key={movie.imdbID}
              {...movie}
              deleteMovie={() => deleteMovie(movie.imdbID)}
            />
          );
        })
      );

  return (
    <div>
      <div>
        <button onClick={recommendMovie}>Recommend a movie</button>
        <button onClick={clearRecommendations}>Clear recommendations</button>
      </div>

      <br />
      {moviesToShow}
    </div>
  );
};

export default withButtons(Movies);
