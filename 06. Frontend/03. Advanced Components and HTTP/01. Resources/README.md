<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [Composition vs. Inheritance](https://reactjs.org/docs/composition-vs-inheritance.html)
- [Higher Order Components](https://reactjs.org/docs/higher-order-components.html)
- [Hooks at a Glance](https://reactjs.org/docs/hooks-overview.html)
- [Best Practices With React Hooks](https://www.smashingmagazine.com/2020/04/react-hooks-best-practices/)
- [React + Fetch - HTTP GET Request Examples](https://jasonwatmore.com/post/2020/01/27/react-fetch-http-get-request-examples)
- [React + Fetch - HTTP POST Request Examples](https://jasonwatmore.com/post/2020/02/01/react-fetch-http-post-request-examples)
- [Why apply Open/Closed principles in React component composition?](https://dev.to/shadid12/why-apply-open-closed-principles-in-react-component-composition-26p1)
- [React useEffect cleanup: How and when to use it](https://dev.to/otamnitram/react-useeffect-cleanup-how-and-when-to-use-it-2hbm)
