import React from "react";

const withStyling = (BaseComponent) => (props) => {
  return (
    <div style={{ color: "red" }}>
      <BaseComponent {...props} />
    </div>
  );
};

export default withStyling;
