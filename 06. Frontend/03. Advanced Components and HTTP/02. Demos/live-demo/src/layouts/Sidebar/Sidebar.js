import React from 'react';
import './Sidebar.css';

const Sidebar = props => {
  return (
    <aside>
      <h4>{props.title}</h4>
      {props.children}
    </aside>
  )
}

export default Sidebar;