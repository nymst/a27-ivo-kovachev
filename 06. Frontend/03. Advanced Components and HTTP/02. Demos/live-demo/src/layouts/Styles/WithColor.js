import React from 'react';

const withColor = (Element, color) => props => <Element {...props} style={{ color: color }} />
export default withColor;