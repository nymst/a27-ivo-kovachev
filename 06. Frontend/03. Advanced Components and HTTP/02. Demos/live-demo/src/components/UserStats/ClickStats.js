import React, { useEffect, useState } from 'react';
import useTime from './UseTime';

const ClickStats = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState('Pesho');
  const [time, setTime] = useTime();

  useEffect(() => {
    console.log(`Hi ${name}, you clicked ${count} times at ${new Date()}`);
    //setCount(count + 2); // NO - will cause cycle!
  }, [name, count]);

  return (
    <div>
      <p>Hi {name}, you clicked {count} times!</p>
      <p>Our clock: {time.toLocaleTimeString()}</p>
      <button onClick={() => setCount(count + 1)}>{name} Click me</button>
      <button onClick={() => setName(name === 'Pesho' ? 'Gosho' : 'Pesho')}>Change user</button>
      <button onClick={() => setTime(prevTime => {
        const updated = new Date(prevTime);
        updated.setSeconds(prevTime
          .getSeconds() + 1);
        return updated;
      })}>Tik-Tok</button>
    </div>
  );
}

export default ClickStats;