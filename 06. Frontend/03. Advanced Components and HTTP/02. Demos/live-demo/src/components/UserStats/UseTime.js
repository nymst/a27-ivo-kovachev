import { useEffect, useState } from 'react';

const useTime = () => {
  const [time, setTime] = useState(new Date());
  useEffect(() => {
    console.log(`Starting statistics at ${new Date()}`);
  }, []);

  return [time, setTime];
}

export default useTime;