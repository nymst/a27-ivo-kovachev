import React from 'react';
import './Header.css';

const Header = () => {

  return (
    <div className="Header">
      <h1>My Awesome App</h1>
    </div>
  )
};

export default Header;
