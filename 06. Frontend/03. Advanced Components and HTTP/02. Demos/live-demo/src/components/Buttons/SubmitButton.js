import React from 'react';
import Button from './Button';

const SubmitButton = () => {
  return <Button text="Submit" />
}

export default SubmitButton;