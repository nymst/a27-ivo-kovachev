import React from 'react';
import withColor from '../../layouts/Styles/WithColor';
import Button from './Button';

const RedLoginButton = props => {
  return <Button {...props} text="Login" onClick={() => alert(`Login logic on click!`)} />
}

export default withColor(RedLoginButton, "red");