import React from 'react';

const Links = ({ linksArr }) => {
  return linksArr.map(l => <a key= {l.title} title={l.title} href={l.href}>{l.title}</a>)
}

export default Links;