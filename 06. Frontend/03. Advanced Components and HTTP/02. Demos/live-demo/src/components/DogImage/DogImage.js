import React, { useEffect, useState } from 'react';
import Button from '../Buttons/Button';

const DogImage = () => {
  const LOADER = "https://wpamelia.com/wp-content/uploads/2018/11/ezgif-2-6d0b072c3d3f.gif";
  const WAITING_STR = "Waiting for an image .....";
  const [image, setImage] = useState(LOADER);
  const [reaction, setReaction] = useState(WAITING_STR);

  const getRandomImage = () => {
    setImage(LOADER);
    fetch('https://dog.ceo/api/breeds/image/random')
      .then(res => res.json())
      .then(res => setImage(res.message));
  }

  useEffect(() => {
    setTimeout(getRandomImage, 5000);
  }, []);

  useEffect(() => {
    if (image !== LOADER) {
      const reactions = ["Wow", "Nice", "So cute", "Well, I'm more of a cat person", "Ha-ha funny"];
      const index = Math.floor(Math.random() * reactions.length);
      setReaction(reactions[index]);
    } else {
      setReaction(WAITING_STR);
    }
  }, [image]);


  return (
    <div>
      <Button onClick={getRandomImage} text="Gimme a doggy!" />
      <br />
      <img alt="dog" src={image} height="320" />
      <h2>{reaction}</h2>
    </div>
  );

}

export default DogImage;