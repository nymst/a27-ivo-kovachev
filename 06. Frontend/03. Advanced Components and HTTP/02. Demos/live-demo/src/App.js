import React from 'react';
import './App.css';
import Button from './components/Buttons/Button';
import RedLoginButton from './components/Buttons/LoginButton';
import SubmitButton from './components/Buttons/SubmitButton';
import DogImage from './components/DogImage/DogImage';
import Header from './components/Header/Header';
import Links from './components/Links/Links';
import ClickStats from './components/UserStats/ClickStats';
import Sidebar from './layouts/Sidebar/Sidebar';

function App() {
  // const list = ["Apple", "Orange", "Banana", "Avocado"];
  // const res = list.map(item => item[0]).filter(ch => ch === "A").reduce((acc, _) => acc++, 0);

  const links = [
    { title: "Google", href: "https://google.com" },
    { title: "Bing", href: "https://bing.com" }
  ]

  return (
    <div className="App" >
      <Header />
      <Sidebar title="Search in:">
        <Links linksArr={links} />
        <hr />
      </Sidebar>
      <Button text="Click me" onClick={() => alert(`Oh yeah!`)} />
      <form style={{ padding: "25px", backgroundColor: "pink", display: "inline-block" }} onSubmit={() => alert(`I will validate some data & submit to server`)}>
        <SubmitButton />
      </form>
      <RedLoginButton onClick={() => alert(`Evil laughter`)} />
      <br />
      <ClickStats />
      <br />
      <DogImage />
    </div>
  );
}

export default App;