import React, { useEffect, useState } from 'react';
import Timer from './components/Timer';
import useTimer from './useTimer';

const App = () => {
  const time = useTimer();

  return (
    <div>
      <h1>My Awesome App!</h1>
      <Timer time={time} />
    </div>
  );
};

export default App;
