import React from "react";

const Posts = (props) => {
  const transformedPosts = props.posts.map((post) => {
    return (
      <li key={post.id}>
        {post.body}
        <button onClick={() => props.deletePost(post.id)}>-</button>
      </li>
    );
  });

  return (
    <main>
      <div>Check out all of our amazing posts!</div>
      <ul>{transformedPosts}</ul>
    </main>
  );
};

export default Posts;
