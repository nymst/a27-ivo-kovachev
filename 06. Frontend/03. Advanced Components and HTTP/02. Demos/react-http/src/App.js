import React, { useEffect, useState } from "react";
import Header from "./components/Header";
import Posts from "./components/Posts";
import Loader from "./components/Loader/Loader";

const App = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch("http://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((data) => setPosts(data))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const handleDeletePost = id => {
    setLoading(true);

    fetch(`http://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "DELETE",
      mode: "cors",
    })
      .then((response) => response.json())
      .then(() => {
        const index = posts.findIndex((post) => post.id === id);
        const updatedPosts = posts.slice();
        updatedPosts.splice(index, 1);

        setPosts(updatedPosts);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }

  const showError = () => {
    if (error) {
      return <h4><i>An error has occured: </i>{error}</h4>
    }
  }

  const showLoader = () => {
    if (loading) {
      return <Loader />
    }
  }

  return (
    <div>
      <Header />
      {showLoader()}
      {showError()}
      {posts.length ? (
        <Posts posts={posts} deletePost={handleDeletePost} />
      ) : (
          <div>No posts to show ...</div>
        )}
    </div>
  );
};

export default App;
