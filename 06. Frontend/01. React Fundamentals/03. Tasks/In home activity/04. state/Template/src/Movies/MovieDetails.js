import React from 'react';
import PropTypes from 'prop-types';

const MovieDetails = props => {
  return (
    <div id={props.imdbID}>
      <img src={props.poster} alt="poster" />
      <div>{props.title}</div>
      <div>
        Year: {props.year} | Type: {props.type}
      </div>
    </div>
  );
};

MovieDetails.propTypes = {
  imdbID: PropTypes.string,
  poster: PropTypes.string,
  title: PropTypes.string,
  year: PropTypes.string,
  type: PropTypes.string
};

export default MovieDetails;
