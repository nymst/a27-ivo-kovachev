<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 04. State

### 1. State

The state object is where you store property values that belongs to the component. When the state object changes, the component re-renders.

We will use state to provide our users with movie recommendation functionality.

1. Add **movieRecommendationIndex** state, using **useState** hook. In the state we'll keep currently recommended movie's index. Let's have the first movie (index 0) as default recommendation. We will update its value in the state object using **setMovieRecommendationIndex** function.

    ```js
    const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(0);
    ```

2. Create a section for showing a recommended movie's title

    ```js
    <div>Our recommendation: {movies[movieRecommendationIndex].title}</div>
    ```

3. Add a button that will show a different recommendation
4. Attach an event to randomly update the movieRecommendationIndex (don't think in-depth about events for now)

    ```js
    <button onClick={handleButtonClick}>Find me another!</button>
    ```

5. The `return` section in the component should look like:

    ```js
    return (
        <div>
          <div>
            <div>Our recommendation: {movies[movieRecommendationIndex].title}</div>
            <button onClick={handleButtonClick}>Find me another!</button>
          </div>

          <br />

          <MovieDetails {...movies[0]} />
          <MovieDetails {...movies[1]} />
          <MovieDetails {...movies[2]} />
        </div>
      );
    };
    ```

6. Add a handler that will update the **movieRecommendationIndex** state to a random valid index
   
    ```js
      // event handler
      const handleButtonClick = () => {
        const validMovieIndex = Math.floor(Math.random() * movies.length);
        setMovieRecommendationIndex(validMovieIndex);
      };
    ```

7. Open `Components` tab from Developer Tools in Chrome and see how app's re-rendered every time the state is updated:

    !['state'](./imgs/state.gif)
