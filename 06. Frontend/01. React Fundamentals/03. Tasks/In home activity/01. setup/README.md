<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 01. Setup

### 1. Scaffold

1. Run `npx create-react-app movies` and scaffold the project
1. While waiting, read about [**npx**](https://blog.npmjs.org/post/162869356040/introducing-npx-an-npm-package-runner) and how it is working with [**create-react-app**](https://github.com/facebook/create-react-app#npx)

### 2. Start the Project

1. Run `npm start` and review the generated project

### 3. Generated Files and Folder Structure

1. Review the generated project.

    The important files that you should take a look at are the **index.html**, **index.js** and **App.js**. 

   - **public/index.html** - The application’s page. Here is our element with id=“root” and here Webpack will include all of our scripts after the build is completed. 

   - **index.js** - The application’s startup script. Here we render the App component in the DOM with the ReactDOM library. 

   - **App.js** - The root component of the application. Here we will include all other components that will shape the application’s UI. 

    There are also the familiar files **package.json** and **package-lock.json** and more complex ones like **serviceWorker.js** or **manifest.json**, which we will not review now.

 2. You can delete some files you'll not need now to reduce the initial complexity of the application. It's okay to end up with the following structure:
   - root
     - package.json
     - package-lock.json
     - .gitignore
   - public
     - index.html
   - src
     - App.js
     - index.js
     - setupTests.js
  
2. Change the scaffolded **index.html** and **create** one that is simpler:
   
   ```html
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="Web site created using create-react-app" />
        <title>Movies</title>
      </head>

      <body>
        <noscript>You need to enable JavaScript to run this app.</noscript>
        <div id="root"></div>
      </body>

    </html>
   ```

3. Change other files as follows:
   1. **App.js** - This is our main component. For now it will only render `div` element with text `Movies` in it.
   
   ```javascript
    import React from 'react';

    const App = () => {
      return <div>Movies</div>;
    };

    export default App;
   ```

   1.  **index.js** - Find the `root` element and load `App component`.
   
   ```javascript
    import React from 'react';
    import ReactDOM from 'react-dom';
    import App from './App';

    ReactDOM.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>,
      document.getElementById('root')
    );
   ```
   
4. Start the project again. Does your main component load?
5. Install [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en#:~:text=Created%20from%20revision%20fed4ae024%20on,%22%20and%20%22%E2%9A%9B%EF%B8%8F%20Profiler%22.) and review html structure.

Congrats! You've created your very first react application!
