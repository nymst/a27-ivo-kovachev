import React from 'react';
import Header from './Header/Header';
import Movies from './Movies/Movies';

const App = () => {
  return (
    <div>
      <Header />
      <hr />
      <Movies />
    </div>
  );
};

export default App;
