<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movie Project - 02. Components

You have setup your project. Now let's add some components.

### 1. Components

#### Header Component

This component will be responsible for rendering page's title.

1. Add a simple **Header** component. Create `src\Header\Header.js` file.
2. Write component's code to render the title:
   
   ```javascript
    import React from 'react';
   
    const Header = () => {
      return <h1>Watch Here..</h1>;
    };

    export default Header;
   ```
3. Add it the the **App** component's view:

    ```javascript
      import React from 'react';
      import Header from './Header/Header';
      import Movies from './Movies/Movies';

      const App = () => {
        return (
          <div>
            <Header />
          </div>
        );
      };

      export default App;
    ```

#### MovieDetails Component

1. Add a sample **MovieDetails** component. Create `src\Movies\MovieDetails.js` file.
2. Show the data from a **movie object**

    ```js
    // the data will be eventually retrieved from here: https://www.omdbapi.com/
    const movie = {
      imdbID: 'tt2278388',
      title: 'The Grand Budapest Hotel',
      year: '2014',
      type: 'movie',
      poster:
        'https://m.media-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg'
    };
    ```

3. Write component's code to render the movie:

    ```js
    return (
        <div id={movie.imdbID}>
          <img src={movie.poster} />
          <div>{movie.title}</div>
          <div>
            Year: {movie.year} | Type: {movie.type}
          </div>
        </div>
      );
    };
    ```

#### Movies Component

1. Add **Movies** component. Create `src\Movies\Movies.js` file. 
2. In **Movies** component add multiple instances of **MovieDetails** component
   
   ```js
    import React from 'react';
    import MovieDetails from './MovieDetails';

    const Movies = () => {
      return (
        <div>
          <MovieDetails />
          <MovieDetails />
          <MovieDetails />
        </div>
      );
    };

    export default Movies;
   ```

3. Add **Movies** to the **App** component's view:

    ```js
    import React from 'react';
    import Header from './Header/Header';
    import Movies from './Movies/Movies';

    const App = () => {
      return (
        <div>
          <Header />
          <hr />
          <Movies />
        </div>
      );
    };

    export default App;
    ```
