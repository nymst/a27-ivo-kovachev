<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [Getting-Started](https://bg.reactjs.org/docs/getting-started.html)
- [Create a New React App](https://reactjs.org/docs/create-a-new-react-app.html)
- [Create React App](https://create-react-app.dev/docs/getting-started/)
- [React Intro](https://www.w3schools.com/react/react_intro.asp)
- [Why learn React and what makes it the hottest technology today?](https://www.telerikacademy.com/about/news/why-learn-react-the-hottest-technology-today)
