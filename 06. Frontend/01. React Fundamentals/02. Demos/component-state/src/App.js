import React, { useState } from 'react';

const App = () => {
  const [number, setNumber] = useState(0);
  const handleClick = () => setNumber(() => Math.floor(Math.random() * 10));

  return (
    <div>
      <div>{number}</div>
      <button onClick={handleClick}>Click me!</button>
    </div>
  );
};

export default App;
