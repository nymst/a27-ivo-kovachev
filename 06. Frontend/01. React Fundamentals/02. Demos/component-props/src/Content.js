import React from 'react';

const Content = props => {
  return (
    <main>
      <div>Content: {props.number}</div>
      <div>{props.description}</div>
      <div>{props.story}</div>
    </main>
  );
};

export default Content;
