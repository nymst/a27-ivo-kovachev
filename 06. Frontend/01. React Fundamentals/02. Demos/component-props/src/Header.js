import React from 'react';

const Header = props => {
  return (
    <div>
      <h1>Welcome!</h1>
      {props.children}
    </div>
  );
};

export default Header;
