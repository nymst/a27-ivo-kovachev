import './App.css';
import { useState } from 'react';
import aliensPoster from './posters/action_aliens.jpg';
import Footer from './components/Footer/Footer';
import Navigation from './components/Navigation/Navigation';
import SingleMovie from './components/SingleMovie/SingleMovie';
import About from './components/About/About';

const App = () => {
  const [movie, setMovie] = useState(() => ({
    id: 1,
    title: `Aliens`,
    genre: `Action`,
    year: 1986,
    description: `Fifty-seven years after surviving an apocalyptic attack aboard her space vessel by merciless space creatures, Officer Ripley awakens from hyper-sleep and tries to warn anyone who will listen about the predators.`,
    director: `James Cameron`,
    stars: [`Sigourney Weaver`, `Michael Biehn`, `Carrie Henn`],
    poster: aliensPoster,
  }));

  const [view, changeView] = useState('about');

  const renderView = () => view === 'about' ? <About /> : <SingleMovie movie={movie} />

  return (
    <div className="App">
      <header></header>
      <Navigation changeView={changeView} />
      <div id="container">
        {renderView()}
        {/* <SingleMovie movie={movie} /> */}
        {/* <About /> */}
      </div>
      <Footer />
    </div>
  );
}

export default App;
