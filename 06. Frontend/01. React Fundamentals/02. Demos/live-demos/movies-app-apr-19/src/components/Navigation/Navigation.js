const Navigation = ({ changeView }) => {

  return (
    <nav>
      <a href="#" className="nav-link">Home</a> |
      <a href="#" className="nav-link">Categories</a> |
      <a href="#" className="nav-link" onClick={() => changeView('single-movie')}>Single movie (test)</a> |
      <a href="#" className="nav-link">Favorites</a>
      <input id="search" type="text" placeholder="Search" />
      <a href="#" className="nav-link" onClick={() => changeView('about')}>About</a>
    </nav>
  );
};

export default Navigation;
