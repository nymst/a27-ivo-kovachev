import { Button } from 'react-bootstrap';

const About = () => {

  return (
    <div id="about">
      <div className="content">
        <h1>About the app</h1>
        <h2>Authors: Telerik Academy</h2>
        <h2>Date: 2021</h2>
      </div>

      <Button>Go back to whatever</Button>
    </div>
  );
};

export default About;
