const SingleMovie = ({ movie }) => {

  return (
    <div id="movie">
    <h1>{movie.title} ({movie.year})</h1>
    <div className="content">
      <div className="movie-detailed">
        <div className="poster">
          <img src={movie.poster} />
        </div>
          <div className="movie-info">
            <p>Genre: {movie.genre}</p>
            <p>Director: {movie.director}</p>
            <p>Staring: {movie.stars.join(', ')}</p>
            <p>Plot: {movie.description}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SingleMovie;
