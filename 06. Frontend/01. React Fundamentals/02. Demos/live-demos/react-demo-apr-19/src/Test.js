import PropTypes from 'prop-types';

const Test = ({ greeting, log }) => {
  // log();

  return (
  <div>
    <h1>
      {greeting}
    </h1>
  </div>
  );
};

Test.propTypes = {
  greeting: PropTypes.string,
  log: PropTypes.func,
};

export default Test;
