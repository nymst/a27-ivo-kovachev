import './App.css';
import Test from './Test';
import Message from './Message';
import { useState } from 'react';
import { messagesData } from './data/messages';

// props change || set Change -> rerender
const App = () => {
  const [greeting, setGreeting] = useState('Hello!');
  const [messages, setMessages] = useState(messagesData);
  console.log(greeting);

  const logGreeting = () => console.log(greeting);

  const changeGreeting = (newGreeting) => setGreeting(newGreeting);

  const deleteMessage = (index) => {
    const newMessages = messages.filter((m) => m.id !== index);
    setMessages(newMessages);
  };

  const addMessage = (message) => {
    // messages.push(message);
    setMessages([...messages, message]);
  };

  return (
    <div className="App">
      {/* <h1 style={{ color: 'red' }}>{greeting}</h1> */}
      <Test greeting={greeting} log={logGreeting} className="Test" /><br />
      <button onClick={() => changeGreeting(greeting === 'Hello!' ? 'Hi!' : 'Hello!')}>Change greeting!</button>
      <h1>All messages</h1>
      {messages[0] ? <Message message={messages[0].message} index={messages[0].id} deleteMessage={deleteMessage} /> : null}
      {messages[1] ? <Message message={messages[1].message} index={messages[1].id} deleteMessage={deleteMessage} /> : null}
    </div>
  );
}

export default App;
