import React from 'react';

const Content = () => {
  return (
    <main>
      <div>Here you can check out all of our amazing stories!</div>
      <ul>
        <li>Story 1</li>
        <li>Story 2</li>
        <li>Story 3</li>
      </ul>
    </main>
  );
};

export default Content;
