import React from 'react';
import PropTypes from 'prop-types';

const Content = props => {
  return (
    <main>
      <div>Content: {props.number}</div>
      <div>{props.description}</div>
      <div>{props.story}</div>
    </main>
  );
};

Content.propTypes = {
  number: PropTypes.number,
  description: PropTypes.string,
  story: PropTypes.string
};

export default Content;
