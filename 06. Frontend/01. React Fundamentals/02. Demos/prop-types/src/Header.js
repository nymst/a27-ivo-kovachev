import React from 'react';
import PropTypes from 'prop-types';

const Header = props => {
  return (
    <div>
      <h1>Welcome!</h1>
    </div>
  );
};

Header.propTypes = {
  children: PropTypes.element.isRequired
};

export default Header;
