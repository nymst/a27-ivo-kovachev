import React from 'react';
import Header from './Header';
import Content from './Content';

const App = () => {
  const contentProps = {
    number: 2,
    description: 3, // should be string
    story: 'My story!'
  };

  return (
    <div>
      <Header>
        <h2>Inner header!</h2>
      </Header>

      <Content {...contentProps} />
    </div>
  );
};

export default App;
