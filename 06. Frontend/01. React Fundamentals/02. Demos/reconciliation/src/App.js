import React, { useEffect, useState } from 'react';

// Magic - don't think about it right now.
// Inspect the timer and see the changing elements.
const App = () => {
  // Open the console. Every time the state is updated, the component is called again
  console.log('called');

  const [time, setTime] = useState(new Date());

  useEffect(() => {
    const timer = setTimeout(
      () =>
        setTime(prevTime => {
          const updated = new Date(prevTime);
          updated.setSeconds(prevTime.getSeconds() + 1);
          return updated;
        }),
      1000
    );

    return () => clearTimeout(timer);
  });

  return (
    <div>
      <h1>My Awesome App!</h1>
      <div>
        Current time: {time.getHours()}:{time.getMinutes()}:{time.getSeconds()}
      </div>
    </div>
  );
};

export default App;
