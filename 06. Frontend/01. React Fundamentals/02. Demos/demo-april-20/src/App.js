import { useState } from 'react';
import './App.css';
import CreateTodo from './CreateTodo';
import Todo from './Todo.js';
import todosData from './todos.js';

const Header = () => <h1 className="header">Todo App</h1>

const App = () => {
  const [todos, setTodos] = useState(todosData);

  const changeTodo = (todo) => {
    todo.isDone = !todo.isDone;

    setTodos([...todos]);
  }

  const addTodo = (name, dueDate) => {
    const max = Math.max(...todos.map(t => t.id));

    const newTodo = {
      isDone: false,
      name: name,
      due: dueDate,
      id: max + 1
    }

    setTodos([...todos, newTodo]);
  }

  //const asTodo = 

  return (
    <div className="wrapper">
      <Header></Header>
      {todos.map((todo) => {
        return <Todo key={todo.id} todo={todo} changeTodo={changeTodo} />
      })}
      <CreateTodo addTodo={addTodo}/>
    </div>
  );
}

export default App;
