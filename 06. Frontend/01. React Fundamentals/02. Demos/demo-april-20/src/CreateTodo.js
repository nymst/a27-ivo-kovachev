import { useState } from 'react';

const CreateTodo = ({ addTodo }) => {
  const [name, setName] = useState('');
  const [dueDate, setDueDate] = useState('');
  const [nameIsDirty, setNameIsDirty] = useState(false);

  const handleClick = () => {
    if (!name || !dueDate) {
      // some message
      console.log('not valid')
    } else {
      addTodo(name, dueDate);
      setName('');
      setNameIsDirty(false);
      setDueDate('');
    }
  }

  const handleNameChange = (value) => {
    setName(value);
    setNameIsDirty(true);
  }

  return (
    <>
      Name: <input
        value={name}
        className={(nameIsDirty && name === '') ? "invalid" : ""}
        type="text"
        onChange={(e) => handleNameChange(e.target.value)}
      />

      Due: <input
        value={dueDate}
        className={dueDate ? "" : "invalid"}
        type="date"
        onChange={(e) => { setDueDate(e.target.value) }}
      />

      <button onClick={handleClick}>Create Todo</button>
    </>
  );
}

export default CreateTodo;