const todos = [
  {
    id: 0,
    name: 'Buy Milk',
    dueDate: '2020-07-20',
    isDone: true
  },
  {
    id: 1,
    name: 'Learn JavaScript',
    dueDate: '2020-09-01',
    isDone: false
  },
  {
    id: 2,
    name: 'Learn React',
    dueDate: '2020-8-20',
    isDone: false
  },
];

export default todos;
