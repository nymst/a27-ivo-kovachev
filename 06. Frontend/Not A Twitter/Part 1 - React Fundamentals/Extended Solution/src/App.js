import React, { useState } from 'react';
import './App.css';
import CreateTweet from './components/CreateTweet/CreateTweet';
import Tweet from './components/Tweet/Tweet';
import tweetsData from './data/tweets';

function App() {
  const [tweets, setTweets] = useState(tweetsData);
  const [showAdd, toggleShowAdd] = useState(false);

  const removeTweet = id => {
    setTweets(prevTweets => prevTweets.filter(t => t.id !== id));
  }

  // make sure tweetData is in format {text: "", isPublic: ""}
  const updateTweet = (id, tweetData) => {
    const copy = [...tweets];
    const index = tweets.findIndex(t => t.id === id);
    copy[index] = { ...copy[index], ...tweetData };
    // update real data and if ok update state or else error handling
    setTweets(copy);

  }

  // make sure tweetData is in format {text: "", isPublic: ""}
  const createTweet = tweetData => {
    const newTweet = {
      id: tweets.length,
      author: 'Pesho',
      date: new Date(),
      ...tweetData
    };

    setTweets([...tweets, newTweet]);
  }

  return (
    <div className="App">
      <h1>Not a Twitter</h1>
      {tweets && tweets.map(t => (
        <div>
          <Tweet {...t} removeTweet={removeTweet} updateTweet={updateTweet} key={t.id} />
        </div>
      ))}
      {showAdd ?
        <CreateTweet createTweet={createTweet} toggleShowAdd={toggleShowAdd} /> :
        <button onClick={() => toggleShowAdd(true)}>+</button>
      }
    </div>
  );
}

export default App;
