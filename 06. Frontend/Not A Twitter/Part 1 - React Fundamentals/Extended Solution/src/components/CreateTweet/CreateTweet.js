import React from 'react';
import PropTypes from 'prop-types';
import './CreateTweet.css';
import EditableTweet from '../EditableTweet/EditableTweet';

const CreateTweet = ({ createTweet, toggleShowAdd }) => {

  const create = (tweetData) => {
    createTweet(tweetData);
    toggleShowAdd(false);
  }

  return (
    <div className="createTweet">
      <h4>New Tweet</h4>
      <EditableTweet text='' isPublic={true} handleTweetData={create} />
    </div >)

}


CreateTweet.propTypes = {
  createTweet: PropTypes.func.isRequired,
  toggleShowAdd: PropTypes.func.isRequired
}
export default CreateTweet;
