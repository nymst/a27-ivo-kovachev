import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './Tweet.css';
import EditableTweet from '../EditableTweet/EditableTweet';

const Tweet = ({ id, text, date, isPublic, author, removeTweet, updateTweet }) => {
  const [isUpdate, toggleUpdate] = useState(false);

  const update = (tweetData) => {
    toggleUpdate(false);
    updateTweet(id, tweetData);
  }

  return (
    <div className="tweet">
      <p>
        {author} - {new Date(date).toDateString()}
      </p>

      {isUpdate ? (
        <EditableTweet text={text} isPublic={isPublic} handleTweetData={update} />
      ) : (
        <div>
          <label>Public: {isPublic ? "Yes" : "No"}</label>
          <p>{text}</p>
          <button onClick={() => removeTweet(id)}>&#128465;</button>
          <button onClick={() => toggleUpdate(true)}>&#9986;</button>
        </div>
      )
      }

    </div >
  );
}

Tweet.propTypes = {
  id: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  isPublic: PropTypes.bool.isRequired,
  author: PropTypes.string.isRequired,
  removeTweet: PropTypes.func.isRequired,
  updateTweet: PropTypes.func.isRequired
}

export default Tweet;
