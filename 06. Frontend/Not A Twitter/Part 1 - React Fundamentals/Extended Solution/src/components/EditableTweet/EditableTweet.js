import React, { useState } from 'react';
import PropTypes from 'prop-types';

const EditableTweet = ({ text, isPublic, handleTweetData }) => {
  const [viewIsPublic, setViewIsPublic] = useState(isPublic);
  const [viewText, setViewText] = useState(text);

  const handle = () => {
    // validations
    if (!viewText || viewText.length > 240) {
      alert('Your tweet should be non empty text shorter than 240 symbols!');
      return;
    }

    handleTweetData({ text: viewText, isPublic: viewIsPublic });
  }

  return (
    <div>
      <label>Public: </label>
      <input
        type="checkbox"
        checked={viewIsPublic}
        onChange={() => setViewIsPublic(!viewIsPublic)}
      />
      <p>
        <input
          type="text"
          value={viewText}
          onChange={e => setViewText(e.target.value)} />
      </p>
      <button onClick={handle}>&#10003;</button>
    </div>
  );
}

EditableTweet.propTypes = {
  text: PropTypes.string.isRequired,
  isPublic: PropTypes.bool.isRequired,
  handleTweetData: PropTypes.bool.isRequired
}
export default EditableTweet;
