<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Not-a-twitter Client

### 1. Description

The rumors were true! There is going to be a web client that will consume the not-a-twitter API and because you are underpaid overachiever it is your job to do it!

To remind you, the goal is to develop an application that looks like Twitter, works like Twitter, but is totally not a Twitter app. Nobody wants to be sued.

<br>

## Sprint I

### 1. Setup the Project

In order to start, scaffold the application with **create-react-app**.

1. Run `npx create-react-app not-a-twitter`
1. Delete the unnecessary files to reduce the complexity.
1. Introduce **eslint** in order to increase the code quality and consistency.
1. Use the provided tweets. In future sprints we will switch them to real data.

```js
const tweets = [
  {
    id: 0,
    text: 'Covid is not real!',
    date: new Date(),
    isPublic: true,
    author: 'Pesho',
  },
  {
    id: 1,
    text: 'The Earth is flat!',
    date: new Date(),
    isPublic: false,
    author: 'Gosho',
  },
];

export default tweets;
```

### 2. Show Tweets

1. Create a **Tweet** component that will accept a tweet's data as props and will display it using JSX. As long as it is functional, the design is entirely up to you.
1. Import the tweet data in the **App** component. Iterate over the data and show it using the **Tweet** component.

### 3. Delete Tweet

1. Add a button in the **Tweet** component, that will delete the tweet.
1. Only the **App** component should have access to the data, so create the delete functionality inside it using a function.
1. Pass the function to the **Tweet** component as props and connect the function to the delete button.

### 4. Update Tweet

1. Add a edit button in the **Tweet** component, that will change the tweet to be in update mode.
1. In update mode the element that shows the tweet's text is replaced with an input field that the user can interact with.
1. In update mode the button for **edit** is replaced with button for **save**.
1. Connect to the input's **value** and **onChange** properties using state.
1. When clicked, the save button will replace the input field with the old element that shows the tweet's text.
1. When clicked, the save button will call a callback from the **App** component (passed as props again) that will update the real tweet data.

### 5. Create Tweet

1. Create a **CreateTweet** component.
1. The component should have input for tweet's text, input for tweet's isPublic property and button for submit.
1. Connect to the inputs using state (the same as **Update Tweet**).
1. When the submit button is clicked validate the data from the inputs and notify the user.
1. Pass a callback for **creating tweet** from the **App** component with the needed logic and when the submit button is clicked, call it with the data from the inputs.
