import React, { useState } from 'react';

const CreateTweet = (props) => {
  const { create } = props;

  const [text, setText] = useState('');
  const [isPublic, setIsPublic] = useState(false);

  const createTweet = () => {
    if (text.trim().length === 0) {
      alert('You must enter text in the input!');
      return;
    }

    create({ text, isPublic });
    setText('');
  };

  return (
    <div>
      <h3>Create Tweet!</h3>

      <label>Tweet:</label>
      <input
        type="text"
        value={text}
        onChange={(ev) => setText(ev.target.value)}
      />

      <br />

      <label>Public:</label>
      <input
        type="checkbox"
        checked={isPublic}
        onChange={() => setIsPublic((prev) => !prev)}
      />

      <br />

      <button onClick={createTweet}>Create</button>
    </div>
  );
};

export default CreateTweet;
