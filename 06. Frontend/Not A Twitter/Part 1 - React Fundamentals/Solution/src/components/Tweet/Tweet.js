import React, { useState } from 'react';
import './Tweet.css';

const Tweet = (props) => {
  const { text, date, isPublic, author, remove, update } = props;

  const [viewText, setViewText] = useState(text);
  const [viewIsPublic, setViewIsPublic] = useState(isPublic);

  const [updateMode, setModeUpdate] = useState(false);

  const toggleUpdateMode = () => {
    setModeUpdate((prevState) => !prevState);
  };

  const saveEdit = () => {
    update({ text: viewText, isPublic: viewIsPublic });
    toggleUpdateMode();
  };

  return (
    <div className="Tweet">
      {updateMode ? (
        <input
          value={viewText}
          onChange={(ev) => setViewText(ev.target.value)}
        />
      ) : (
        <p>{viewText}</p>
      )}

      <p>
        {author} - {new Date(date).toDateString()}
      </p>

      <div>
        <label>Public:</label>
        <input
          type="checkbox"
          defaultChecked={viewIsPublic}
          onChange={() => setViewIsPublic((prev) => !prev)}
        />
      </div>

      {updateMode ? (
        <button onClick={saveEdit}>Save</button>
      ) : (
        <button onClick={toggleUpdateMode}>Edit</button>
      )}
      <button onClick={remove}>Delete</button>
    </div>
  );
};

export default Tweet;
