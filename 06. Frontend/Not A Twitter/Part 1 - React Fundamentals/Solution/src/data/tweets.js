const tweets = [
  {
    id: 0,
    text: 'Covid is not real!',
    date: new Date(),
    isPublic: true,
    author: 'Pesho',
  },
  {
    id: 1,
    text: 'The Earth is flat!',
    date: new Date(),
    isPublic: false,
    author: 'Gosho',
  },
];

export default tweets;
