import React, { useState } from 'react';
import './App.css';
import CreateTweet from './components/CreateTweet/CreateTweet';
import Tweet from './components/Tweet/Tweet';
import tweetsData from './data/tweets';

function App() {
  const [tweets, setTweets] = useState(tweetsData);

  const removeTweet = (id) => {
    setTweets((tweets) => tweets.filter((tweet) => tweet.id !== id));
  };

  const updateTweet = (id, tweetData) => {
    const index = tweets.findIndex((tweet) => tweet.id === id);
    const copy = [...tweets];

    copy[index] = { ...copy[index], ...tweetData };
    setTweets(copy);
  };

  const createTweet = (tweetData) => {
    const newTweet = {
      id: tweets.length,
      date: new Date(),
      author: 'Pesho',
      ...tweetData,
    };

    const newTweets = [...tweets, newTweet];
    setTweets(newTweets);
  };

  return (
    <div>
      <CreateTweet create={createTweet} />

      {tweets.map((tweet) => {
        return (
          <Tweet
            {...tweet}
            update={(updateData) => updateTweet(tweet.id, updateData)}
            remove={() => removeTweet(tweet.id)}
            key={tweet.id}
          />
        );
      })}
    </div>
  );
}

export default App;
