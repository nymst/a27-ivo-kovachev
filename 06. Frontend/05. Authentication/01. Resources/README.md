<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [Context](https://reactjs.org/docs/context.html)
- [Managing Authentication with React’s useContext Hook](https://www.bradcypert.com/react-usecontext-hook/)


- [JWT](https://jwt.io/introduction/)
- [Building Basic React Authentication](https://medium.com/better-programming/building-basic-react-authentication-e20a574d5e71)
- [Guarded Routes](https://blog.netcetera.com/how-to-create-guarded-routes-for-your-react-app-d2fe7c7b6122)