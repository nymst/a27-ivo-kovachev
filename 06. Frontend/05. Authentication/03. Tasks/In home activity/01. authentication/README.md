<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## React Movies Project - 12. Authentication

### 1. Server

We will add some authentication to our movie project. First things, first. Go to **./server.js** and review the simple server that is implemented there.

The server provides several endpoints taht we'll be using in order to implement authentication in the project:
  - `http://localhost:3001/register`
  - `http://localhost:3001/login`
  - `http://localhost:3001/protected-resource`
  - `http://localhost:3001/logout`

You can start the server using node:

```bash
node server.js 
```

### 2. Authentication

1. Create a **register component**
   1. Add simple inputs for username and password

      ```html
        <div>
          <h3>Register</h3>
          <input
            type="text"
            placeholder="username.."
            value={username}
            onChange={(ev) => setUsername(ev.target.value)}
          />
          <input
            type="password"
            placeholder="password.."
            value={password}
            onChange={(ev) => setPassword(ev.target.value)}
          />
          <button onClick={register}>Submit!</button>
        </div>
      ```

   2. On submit make a request to the server, sending the submitted data. Do not forget to use `useState` to preserve data between elements

      ```js
        const [username, setUsername] = useState('');
        const [password, setPassword] = useState('');

        const register = () => {
          fetch('http://localhost:3001/register', {
            body: { username, password },
            method: 'POST',
          })
            .then((res) => res.json())
            .then(console.log);
        };
      ```

   3. Test the register
      1. Add route in **App.js**

          ```html
          <Route path="/register" component={Register} />
          ```

      2. Go to the new route and test out the registration. Open console to see the result

      ![register_test](imgs/register_test.png) 


2. Create a **login component** by reusing the register and changing the endpoint to the correct one.
   1. When making the request for login, save the token inside the local storage on successful response
   
    ```html
      const [username, setUsername] = useState('');
      const [password, setPassword] = useState('');

      const login = () => {
        fetch('http://localhost:3001/login', {
          body: { username, password },
          method: 'POST',
        })
      };

      return (
          <div>
            <h3>Login</h3>
            <input
              type="text"
              placeholder="username.."
              value={username}
              onChange={(ev) => setUsername(ev.target.value)}
            />
            <input
              type="password"
              placeholder="password.."
              value={password}
              onChange={(ev) => setPassword(ev.target.value)}
            />
            <button onClick={login}>Submit!</button>
          </div>
        );
      };
    ```

   2. Add route in **App.js**

        ```html
        <Route path="/login" component={Login} />
        ```
3. Use local storage to store token for the authenticated user. Create state variables to define whether there is such.

      ```js
        const isAuth = !!localStorage.getItem('token');
        const [auth, setAuth] = useState(isAuth);

        const setLoginState = (isLogged, token = null) => {
          if (isLogged) {
            localStorage.setItem('token', token);
            setAuth(true);
          } else {
            localStorage.removeItem('token');
            setAuth(false);
          }
        };
      ```

4. Create an **AuthContext** that will hold the auth state and wrap the application with it
   1. Create **/context/authContex.js** and use ContextAPI to create new context object:
   
      ```js
        import { createContext } from 'react';

        export const AuthContext = createContext({
          isLoggedIn: false,
          setLoginState: (isLoggedIn, token = null) => {},
        });
      ```

5. Setup the authContext inside the App component and provide the **setLoginState** function that will set the auth state and manipulate the local storage. 

    ```html
      return (
          <BrowserRouter>
            <AuthContext.Provider value={{ isLoggedIn: auth, setLoginState }}>
              <Layout>
                <Switch>
                  <Redirect from="/" exact to="/home" />
                  <Route path="/home" component={Home} />
                  <Route path="/movies" exact component={Movies} />
                  <Route path="/movies/:id" component={IndividualMovie} />
                  <Route path="/register" component={Register} />
                  <Route path="/login" component={Login} />
                  <Route path="*" component={NotFound} />
                </Switch>
              </Layout>
            </AuthContext.Provider>
          </BrowserRouter>
        );
      };
    ```

6.  Use the **useContext** hook in the login component and change the login state on successful response (the local storage will be updated by it also)

    ```js
      const [username, setUsername] = useState('');
      const [password, setPassword] = useState('');

      const { setLoginState } = useContext(AuthContext);

      const login = () => {
        fetch('http://localhost:3001/login', {
          body: { username, password },
          method: 'POST',
        })
          .then((res) => res.json())
          .then(({ token }) => setLoginState(true, token));
      };
    ```

    This way inside the App component we initialized the **AuthContext** with **AuthState** depending if token is present in the **local** **storage** or not.

7.  Add navLinks for register and login inside the **Navbar component**
    1.  Use the **AuthContext** and **useContext** inside the navbar to only show the Register and Login if the login state is false
    2.  Create a **Logout** button in the navbar and only show it if the login state is true
    3.  On **Logout click** make a request and on success, delete the token from the local storage and set the login state to false

      ```js
        const { isLoggedIn, setLoginState } = useContext(AuthContext);

        const logout = () => {
          fetch('http://localhost:3001/logout', { method: 'POST' })
            .then((res) => res.json())
            .then(() => setLoginState(false));
        };

        return (
          <nav className="Navigation">
            <div className="basic-navigation">
              <NavLink to="/home">Home</NavLink>
              <NavLink to="/movies">Movies</NavLink>
            </div>

            <div className="auth-navigation">
              {!isLoggedIn && <NavLink to="/register">Register</NavLink>}
              {!isLoggedIn && <NavLink to="/login">Login</NavLink>}
              {isLoggedIn && <button onClick={logout}>Logout</button>}
            </div>
          </nav>
        );
      };
      ```

8.  In the **App component** add a temporary **accessProtectedResource** function that will access a special "protected" endpoint in the server

    ```js
      const accessProtectedResource = () => {
      const headers = new Headers();
      headers.append('authorization', `Bearer ${localStorage.getItem('token')}`);

      fetch('http://localhost:3001/protected-resource', { headers })
        .then((res) => res.json())
        .then(({ message }) => alert(message));
    };
    ```

    If the user is authenticated call it. Make sure you do than inside the AuthContext.Provider!

    ```html
        {isAuth && (
              <button onClick={accessProtectedResource}>
                Access protected resource!
              </button>
            )}
    ```

As you see here we attach the token to the authorization header and we make it  **Bearer**. This is another way of authoring the user.
Whenever the user wants to access a protected route or resource, the user agent should send the JWT, typically in the Authorization header using the Bearer schema. The content of the header should look like the following:

`Authorization: Bearer <token>`

This can be, in certain cases, a stateless authorization mechanism. The server's protected routes will check for a valid JWT in the Authorization header, and if it's present, the user will be allowed to access protected resources. If the JWT contains the necessary data, the need to query the database for certain operations may be reduced, though this may not always be the case.

If the token is sent in the Authorization header, Cross-Origin Resource Sharing (CORS) won't be an issue as it doesn't use cookies or in our case local storage.
