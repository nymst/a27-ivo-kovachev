const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json(), cors());

const users = [];
const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

app.post('/register', (req, res) => {
  const user = req.body;
  users.push(user);

  res.status(200).json({ message: 'Successful register!' });
});

app.post('/login', (req, res) => {
  const userSend = req.body;
  const user = users.find(
    (u) => u.username === userSend.username && u.password === userSend.password
  );

  if (!user) {
    return res.status(400).json({ message: 'Invalid username or password!' });
  }

  res.status(200).json({ message: 'Successful login!', token });
});

app.post('/logout', (req, res) => {
  res.status(200).json({ message: 'Successful logout!' });
});

app.get('/protected-resource', (req, res) => {
  const tokenSend = req.headers.authorization;
  const tokenExpected = `Bearer ${token}`;

  if (!tokenSend || tokenSend !== tokenExpected) {
    return res.status(400).json({
      message: 'You must authenticate in order to see this resource!',
    });
  }
  res.status(200).json({ message: 'Authenticated!' });
});

app.listen(3001, () => console.log('Server running on 3001'));
