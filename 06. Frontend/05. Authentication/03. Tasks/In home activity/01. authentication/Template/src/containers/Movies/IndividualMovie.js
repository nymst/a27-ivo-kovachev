import React, { Fragment } from 'react';
import Loader from '../../components/Loader/Loader';
import { CONSTANTS } from '../../constants/constants';
import MovieDetails from '../../components/Movies/MovieDetails';
import AdditionalMovieInfo from '../../components/Movies/AdditionalMovieInfo';
import useHttp from '../../hooks/useHttp';

const IndividualMovie = props => {
  const imdbID = props.match.params.id;
  const { data, loading, error } = useHttp(
    `${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&i=${imdbID}`
  );
  
  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <h1>{error}</h1>;
  }

  return (
    <div>
      {data && (
        <Fragment>
          <MovieDetails {...data} />
          <AdditionalMovieInfo {...data} />
        </Fragment>
      )}
    </div>
  );
};

export default IndividualMovie;
