import React, { useState } from 'react';
import { CONSTANTS } from '../../constants/constants';
import withButtons from '../../components/Layout/withButtons';
import MovieDetails from '../../components/Movies/MovieDetails';
import Loader from '../../components/Loader/Loader';
import './Movies.css';
import useHttp from '../../hooks/useHttp';

const Movies = props => {
  const [movieRecommendationIndex, setMovieRecommendationIndex] = useState(
    null
  );

  const { data, setLocalData, loading, error } = useHttp(
    `${CONSTANTS.baseUrl}?apiKey=${CONSTANTS.apiKey}&s=grand`,
    {
      Search: []
    }
  );
  const movies = data.Search;

  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <h1>{error}</h1>;
  }

  const recommendMovie = () => {
    const validMovieIndex = Math.floor(Math.random() * movies.length);
    setMovieRecommendationIndex(validMovieIndex);
  };

  const clearRecommendations = () => {
    setMovieRecommendationIndex(null);
  };

  const deleteMovie = movieId => {
    const filteredMovies = movies.filter(movie => movie.imdbID !== movieId);
    setLocalData({ Search: filteredMovies });
  };

  const moviesToShow =
    movieRecommendationIndex !== null ? (
      <div>
        <div>Our recommendation:</div>
        <MovieDetails
          {...movies[movieRecommendationIndex]}
          goToDetails={() =>
            props.history.push(
              `/movies/${movies[movieRecommendationIndex].imdbID}`
            )
          }
        />
      </div>
    ) : (
      <div className="movie-list">
        {movies.map(movie => {
          return (
            <MovieDetails
              key={movie.imdbID}
              {...movie}
              goToDetails={() => props.history.push(`/movies/${movie.imdbID}`)}
              deleteMovie={() => deleteMovie(movie.imdbID)}
            />
          );
        })}
      </div>
    );

  return (
    <div className="Movies">
      <div>
        <button onClick={recommendMovie}>Recommend a movie</button>
        <button onClick={clearRecommendations}>Clear recommendations</button>
      </div>

      <br />

      {moviesToShow}
    </div>
  );
};

export default withButtons(Movies);
