import React, { Fragment } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Navigation from '../Navigation/Navigation';

const Layout = props => {
  return (
    <Fragment>
      <Header />
      <Navigation />
      <hr />
      <main>{props.children}</main>
      <hr />
      <Footer />
    </Fragment>
  );
};

export default Layout;
