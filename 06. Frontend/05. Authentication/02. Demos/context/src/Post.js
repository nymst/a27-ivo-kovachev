import React, { useEffect, useState } from 'react';
import PostDetails from './PostDetails';

const Post = props => {
  const [postData, setPostData] = useState(null);
  const { id } = props.match.params;

  useEffect(() => {
    fetch(`http://jsonplaceholder.typicode.com/posts/${id}`, { mode: 'cors' })
      .then(response => response.json())
      .then(data => setPostData(data));
  }, [id]);

  return postData && <PostDetails {...postData} isIndividual={true} />;
};

export default Post;
