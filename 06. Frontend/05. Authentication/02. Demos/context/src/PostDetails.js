import React from 'react';
import { withRouter } from 'react-router-dom';

const PostDetails = props => {
  const handleButtonClick = () => props.history.goBack();

  return (
    <div>
      <div>Title: {props.title}</div>
      <div>Body: {props.body}</div>

      {props.isIndividual && <button onClick={handleButtonClick}>Back</button>}
    </div>
  );
};

export default withRouter(PostDetails);
