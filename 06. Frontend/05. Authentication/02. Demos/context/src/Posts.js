import React, { useState, useEffect, useContext } from 'react';
import Loader from './Loader/Loader';
import PostDetails from './PostDetails';
import { AuthContext } from './Context/AuthContext';

const Posts = props => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const { isLoggedIn } = useContext(AuthContext);

  useEffect(() => {
    setLoading(true);

    fetch('http://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(data => setPosts(data))
      .catch(error => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <h3>{error}</h3>;
  }

  const transformedPosts = posts.map(post => {
    return (
      <li key={post.id} onClick={() => props.history.push(`/posts/${post.id}`)}>
        <PostDetails {...post} isIndividual={false} />
      </li>
    );
  });

  return (
    <main>
      {isLoggedIn && <h3>Hello User,</h3>}
      <div>Check out all of our amazing posts!</div>
      <ul>{transformedPosts}</ul>
    </main>
  );
};

export default Posts;
