import React, { createContext, useState } from 'react';
import Header from './Header/Header';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Posts from './Posts';
import Post from './Post';
import Home from './Home';
import NotFound from './NotFound';
import { AuthContext } from './Context/AuthContext';

const App = () => {
  const [authValue, setAuthValue] = useState(false);

  return (
    <BrowserRouter>
      <AuthContext.Provider
        value={{ isLoggedIn: authValue, setLoginState: setAuthValue }}
      >
        <Header />

        <hr />

        <Switch>
          <Redirect path="/" exact to="/home" />
          <Route path="/home" component={Home} />
          <Route path="/posts/:id" component={Post} />
          <Route path="/posts" component={Posts} />
          <Route path="*" component={NotFound} />
        </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};

export default App;
