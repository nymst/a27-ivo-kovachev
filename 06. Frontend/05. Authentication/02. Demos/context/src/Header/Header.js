import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';
import { AuthContext } from '../Context/AuthContext';

const Header = () => {
  const { isLoggedIn, setLoginState } = useContext(AuthContext);

  return (
    <div>
      <h2>My Awesome Application!</h2>

      <nav className="navigation">
        <NavLink to="/home">Home</NavLink>
        <NavLink to="/posts">Posts</NavLink>

        {isLoggedIn ? (
          <button onClick={() => setLoginState(false)}>Logout</button>
        ) : (
          <button onClick={() => setLoginState(true)}>Login</button>
        )}
      </nav>
    </div>
  );
};

export default Header;
