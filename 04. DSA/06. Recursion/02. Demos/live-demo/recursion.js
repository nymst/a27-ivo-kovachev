// let count = 0;

// const countDown = () => {
//   count++;

//   countDown();
// };

// try {
//   countDown();
// } catch (error) {
//   console.log(count);
// }


// const person = {
//   name: 'Pesho',
//   bestFriend: person,
// };

// person.bestFriend.bestFriend

// F(n) = F(n-1) + F(n-2)
// | F(0) = 0
// | F(1) = 1
// | F(2) = 1

// const fib = n => {
//   if (n === 0) return 0;
//   if (n < 3) return 1;

//   return fib(n - 1) + fib(n - 2);
// };

// console.log(fib(46));


// F(n) = F(n-1) + F(n-2)
// | F(0) = 0
// | F(1) = 1
// | F(2) = 1

// const memo = new Map();

const fibMemo = (n, memo = new Map()) => {
  if (n === 0) return 0;
  if (n < 3) return 1;

  if (!memo.has(n - 2)) {
    memo.set(n - 2, fibMemo(n - 2, memo));
  }
  if (!memo.has(n - 1)) {
    memo.set(n - 1, fibMemo(n - 1, memo));
  }

  return memo.get(n - 1) + memo.get(n - 2);
};

console.log(fibMemo(5));

// k of n

// combinations of n
// 111, 112, 113, 121, 122, 123

const combine = (n, depth = 0, result = '') => {
  if (depth === n) {
    return console.log(result);
  }

  for (let i = 1; i <= n; i++) {
    // record i somewhere
    combine(n, depth + 1, result + i);
  }

};

combine(3);


// find solution to the shortest path from start to end
const matrix = [
  [0, 0, 0, 0],
  [0, 'start', 0, 0],
  [0, 0, 0, 0],
  [0, 0, 0, 'end'],
];

const arrToLinkedList = (arr = []) => arr.reduceRight((next, value) => ({ value, next }), null);

console.log(arrToLinkedList([1, 2, 3]));
