/**
 * @function reversingFunction
 * @param {string} input 
 * @returns reversed string
 */
// const input = 'Telerik';
// const reverse = (input) => {
//   if (input.length <= 1) {
//     return input;
//   }

//   const firstLetter = input[0];
//   const restLetters = input.slice(1);

//   return reverse(restLetters) + firstLetter;
// }

// console.log(reverse(input));





/**
 * @function printChildren
 */
// const tree = {
//   name: 'John',
//   children: [
//     {
//       name: 'Jim',
//       children: []
//     },
//     {
//       name: 'Zoe',
//       children: [
//         { name: 'Kyle', children: [] },
//         { name: 'Sophia', children: [] }
//       ]
//     }
//   ]
// }
// function printChildrenRecursive(t) {
//   if (t.children.length === 0) {
//     return
//   }
//   t.children.forEach(child => {
//     console.log(child.name)
//     printChildrenRecursive(child)
//   })
// }

// printChildrenRecursive(tree);





/**
 * @function fibonacciNumbers
 */


// let print = console.log;
// const gets = ((array, index) => () => array[index++])(input, 0);

const fibonacciNumbers = (n, memo = new Map()) => {
  if (n === 0) return 0;
  if (n <= 2) return 1;

  if (!memo.has(n - 2)) {
    memo.set(n - 2, fibonacciNumbers(n - 2, memo));
  }

  if (!memo.has(n - 1)) {
    memo.set(n - 1, fibonacciNumbers(n - 1, memo));
  }

  return memo.get(n - 1) + memo.get(n - 2);
}
// print(fibonacciNumbers(3));





/**
 * @function factorial
 */

// let n = +gets();

// const solve = (n) => {
//   console.log(n);
//   if (n <= 1) return 1;

//   return n * solve(n - 1);
// }

// console.log(solve(5));





// const multiply = (n3, n4) => n3 * n4;

// const solve = (n1, n2) => {
//   console.log('kureshka', n1, n2)
//   if (n2 < 1) {
//     return 0;
//   }

//   return n1 + n2 + multiply(n1, n2) + solve(n1, n2 - 1);
// }

// console.log(solve(1, 3)); // 1 + 3 + 1*3 +    1 + 2 + 1*2 +    1 + 1 + 1*1 + 0





const reverseArr = (array = [], n = array.length) => {
  if (n === 0) return [];

  return [array[n - 1]].concat(reverseArr(array, --n));
}
// console.log(reverseArr([1, 2, 3, 4]))





// const recursiveLoop = (start, end) => {
//   if (start === end) return;

//   recursiveLoop(start + 1, end);
//   console.log(start);
// }

// recursiveLoop(0, 5);


/**
 * чрез рекурсия да пресметнеш колко уши имат n брой зайци, ако всеки заек има по 2 уши
 * 
   същото като горното обаче всеки нечетен заек има 2 уши а всеки четен заек има 3 уши
   
   по дадени N и K числа, да се изчисли power-а като base = n; power = k
	
   по даден стринг да върнеш колко пъти се повтаря х в стринга
	
   по даден стринг да преброиш колко пъти се повтаря hi в него
	
   ако строиш нещо като пирамидка където реда ти е равен на броя блокове в него, по дадени брой редове да изкараш колко общо блока има пирамидката;
	
   по дадено число да препроиш колко пъти се повтаря числото 7 в него;
	
   да сумираш цифрите в дадено число;
 */


const bunnyEars = n => {
  if (n === 0) {
    return 0;
  }

  return 2 + bunnyEars(n - 1);
}

// console.log(bunnyEars(15));





const bunnyEarsExtended = n => {
  if (n === 0) {
    return 0;
  }
  if (n % 2 === 0) {
    return 3 + bunnyEarsExtended(n - 1);
  } else {
    return 2 + bunnyEarsExtended(n - 1);
  }
}

// console.log(bunnyEarsExtended(5));




// по дадени N и K числа, да се изчисли power-а като base = n; power = k
const pow = (n, k) => {
  if (k === 0) {
    return 1;
  }

  return n * pow(n, --k);
}

// console.log(pow(3, 3));





// по даден стринг да върнеш колко пъти се повтаря х в стринга
const timesFound = str => {
  if (str.length === 0) {
    return 0;
  }

  if (str[0] === 'x') {
    return 1 + timesFound(str.slice(1));
  } else {
    return timesFound(str.slice(1));
  }
}

// console.log(timesFound('xaxaxxx'));




// да сумираш цифрите в дадено число;
const sumOfElements = n => {
  let numToStr = n.toString();
  if (numToStr.length === 0) return 0;

  return +numToStr[0] + sumOfElements(numToStr.slice(1));
}

// console.log(sumOfElements('123'));





// по даден стринг да преброиш колко пъти се повтаря hi в него
const findHi = (str1, str2 = 'hi') => {
  if (str1.includes(str2)) {
    return 1 + findHi(str1.replace(str2, ''));
  }

  return 0;
}

// console.log(findHi('hirerehi'));






// по дадено число да препроиш колко пъти се повтаря числото 7 в него;
const countSeven = n => {
  const numToStr = n.toString();

  if (numToStr.length === 0) return 0;

  if (numToStr[0] === '7') {
    return 1 + countSeven(numToStr.slice(1));
  } else {
    return countSeven(numToStr.slice(1));
  }
}

// console.log(countSeven(12347877));




const includes = (values = [], value) => {
  if (values.length === 0) {
    return false;
  }

  if (values[0] === value) {
    return true;
  }

  return includes(values.filter(el => el !== values[0]), value);
}

// console.log(includes([1, 2, 3, 4, 3, 6, 5], 3))




// find the biggest num
const findBiggestNum = (arr = []) => {
  if (arr.length === 0) return 0;

}





    
const indexOf = (values=[], search, n = 0) => {​​​​​​​​
    if (values[n] === search) {​​​​​​​​
        return n
    }​​​​​​​​ return indexOf(values,search, n+1)
    
}​​​​​​​​


