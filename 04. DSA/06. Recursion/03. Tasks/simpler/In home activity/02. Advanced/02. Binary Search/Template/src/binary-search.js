//Implement binary search here

// prerequisite - the array should be sorted
const arr = ["Alex", "Anne", "Brian", "Chris", "David", "Elan", "Frank", "James", "Hellen", "Ivan"];

const binarySearch = (array, target, start, end) => {
  // validate the input

  // get the middle

  // copy the array

  // base case - we found the element

  // base case - we have no more elements to check

  // recursive step - the target is bigger than the element we chose
  // search on the right of the middle

  // recursive step - the target is smaller than the element we chose
  // search on the left of the middle
}

const target = "Frank";
const index = binarySearch(arr, target, 0, arr.length);

console.log(index > -1 ? `${target} is on index ${index}` : `There is no ${target} in the list`);