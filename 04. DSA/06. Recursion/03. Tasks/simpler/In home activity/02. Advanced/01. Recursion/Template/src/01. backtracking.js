// A recursive structure with branches to iterate over
const root = {
  number: 1,
  left: {
    number: 2,
    left: {
      number: 3,
      left: null,
      right: null,
    },
    right: null,
  },
  right: {
    number: 4,
    left: null,
    right: {
      number: 5,
      left: null,
      right: null,
    },
  },
};

const findNumberNode = (number, currentNode) => {
 // code goes here
};

console.log(findNumberNode(5, root));
