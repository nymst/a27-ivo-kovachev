<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## DSA - 05.1. Advanced Recursion

### Tasks

1. Find element by value in a recursive structure.

    > What is a recursive structure? Is the path object you worked with a recursive structure?

    Let's review the following recursive structure:

    ```js
    // A recursive structure with branches to iterate over
    const root = {
      number: 1,
      left: {
        number: 2,
        left: {
          number: 3,
          left: null,
          right: null,
        },
        right: null,
      },
      right: {
        number: 4,
        left: null,
        right: {
          number: 5,
          left: null,
          right: null,
        },
      },
    };
    ```
    The task is to find first element with a given value. Let's think how we'd do this manually. How do you find a file with specific name on your hard disk? You probably go and search each folder. if you find the file you stop. If not, you search folder's subfolders and if you do not find it you return to the root and start browsing another one.

    This technique is called **backtracking** - its simplest explanation would be - go and search until there is nothing more to search in. Then go back and start over in some other direction. Eventually you will know whether there is a solution or not.

    Recursion goes naturally here - the **base case** would be when we've found our value or when there is no more values to search among.
    The **recursive step** is the search itself.

    Now let's think over one more aspect - how many directions can we go? Reviewing the structure we see that it's built up from "nodes". Each node has a value - `number` and **could** have left and right nodes. Starting from the root we can go either right or left. 

    Now let's go and implement it:

    1. We'll have a recursive function that takes the value we search for and the node we want to start our search. We chose this function signature, having in mind that it has to be good to be used in the recursive step.

        ```js
        const findNumberNode = (number, currentNode) => { ... }
        ```
    2. Now let's write the base cases - we have two - we've found node with number equal to searched value or we cannot search anymore:

        ```js
        if (currentNode === null) {
          return null;
        }

        if (number === currentNode.number) {
          return currentNode;
        }
        ```

    3. Aaand do the recursion - first let's go left. If we find node that has searched value we are good to go - no need to go right:

        ```js
        const leftResult = findNumberNode(number, currentNode.left);
        if (leftResult) {
          return leftResult;
        }
        ```

    4. if we didn't have any luck going left, let's search right:

        ```js
        const rightResult = findNumberNode(number, currentNode.right);
        if (rightResult) {
          return rightResult;
        }
        ```

    5. Yet, there is chance we did not find anything, so return `null`

        ```js
        return null;
        ```

    6. Let's test it:

        ```js
        console.log(findNumberNode(5, root));
        ```

      > How will modify the function if the recursive structure was build from nodes that have the following properties:

      ```js
      node: {
        number, /* value */
        left, /* node */
        right, /* node */
        middle /* node */
      }
      ```

      > What if wanted to know **how many** nodes with the search value are there in the structure?

2. Find the n-th fibonacci number. We will agree that n starts from 1. 
   
   The Fibonacci sequence: 
   ```
   0, 1, 1, 2, 3, 5, 8, 13, 21, 34...
   ``` 

   It is calculated using the formula:

   ```
   F(n) = F(n-1) + F(n-2)
   ```

   The recursion is pretty obvious:
    1. Base case - numbers 1 and 2

        ```js
        if (n === 1 || n === 2) {
          return 1;
        }
        ```

    2. Recursive step - we calculate n-1-st and n-2-nd fibonacci elements and return their sum

        ```js
        return fibonacci(n - 1) + fibonacci(n - 2);
        ```
    Now again we need to think - working with recursion requires a lot of thinking. Once we understand it we tend to use it everywhere we can. But this is not a cheap operation. Every recursive call comes with its scope and variables and waits for the next calls to be executed. 

    Using the fibonacci task we will optimise our approach to recursive tasks.

    **Memoization**
    Each Fibonacci sequence member can be remembered once it is calculated and the result can be returned directly when needed again. This will decrease the steps we make to calculate the result, hence the time complexity.

    We will use an additional array to "remember" already calculated fibonacci members. 

    So the algorithm goes like this - if you calculated the n-th fibonacci element then the memo array n-th element will have its value. Otherwise calculate it and put it there.

      ```js
      const fibonacciFast = (n, memo) => {
        if (memo[n] === undefined) {
          // base case
          if (n === 1 || n === 2) {
            memo[n] = 1;
          } else {
            // Memoization & recursion step
            memo[n] = fibonacciFast(n - 1, memo) + fibonacciFast(n - 2, memo);
          }
        }

        return memo[n];
      };

      const memo = [];
      console.log(fibonacciFast(8, memo));
      ```

      > Now what we do - we gain time, but we loose space? Yep, you can rarely optimise both time and space complexity. Usually you either sacrifice the one or the other.