// up the stack
const recursiveLoopUp = (start, end) => {
  // code goes here
};

recursiveLoopUp(0, 5); // print in increasing order

// down the stack
const recursiveLoopDown = (start, end) => {
  // code goes here
};

recursiveLoopDown(0, 5); // print in decreasing order
