import { deepCompareObjects, formatObject } from './common/utils.js';

/**
 * Finds the element with the searched id inside a sample DOM tree using recursion
 * @param {string} elementId The id of the searched element
 * @param {object} currentDomElement The DOM element to search into
 * @returns {object | undefined} Returns the found element or undefined if it fails
 */
const dfs = (elementId, currentDomElement) => {
  // your implementation:
};

// Tests:
const testCases = [
  {
    test: {
      elementId: 'awesome-paragraph',
      domTree: {
        tag: 'html',
        id: null,
        children: [
          {
            tag: 'head',
            id: null,
            children: [
              {
                tag: 'link',
                id: null,
                children: [],
              },
              {
                tag: 'title',
                id: null,
                children: [],
              },
              {
                tag: 'icon',
                id: null,
                children: [],
              },
            ],
          },
          {
            tag: 'body',
            id: null,
            children: [
              {
                tag: 'div',
                id: null,
                children: [
                  {
                    tag: 'p',
                    id: 'awesome-paragraph',
                    children: [],
                  },
                ],
              },
              {
                tag: 'script',
                id: null,
                children: [],
              },
            ],
          },
        ],
      },
    },
    expected: { tag: 'p', id: 'awesome-paragraph', children: [] },
  },
  {
    test: {
      elementId: 'main',
      domTree: {
        tag: 'html',
        id: null,
        children: [
          {
            tag: 'head',
            id: null,
            children: [
              {
                tag: 'link',
                id: null,
                children: [],
              },
              {
                tag: 'title',
                id: null,
                children: [],
              },
              {
                tag: 'icon',
                id: null,
                children: [],
              },
            ],
          },
          {
            tag: 'body',
            id: null,
            children: [
              {
                tag: 'div',
                id: null,
                children: [
                  {
                    tag: 'p',
                    id: null,
                    children: [],
                  },
                ],
              },
              {
                tag: 'script',
                id: null,
                children: [],
              },
            ],
          },
        ],
      },
    },
    expected: undefined,
  },
];

testCases.forEach(({ test, expected }, index) => {
  // arrange & act
  const actual = dfs(test.elementId, test.domTree);

  // assert
  const result = deepCompareObjects(expected, actual);

  const message = result
    ? 'Pass.'
    : `Fail. Expected: ${formatObject(expected)}. Actual: ${formatObject(
        actual
      )}`;

  console.log(`Test ${index + 1}: ${message}`);
});
