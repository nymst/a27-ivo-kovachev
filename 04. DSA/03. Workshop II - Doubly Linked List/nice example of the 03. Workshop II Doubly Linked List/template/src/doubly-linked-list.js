import LinkedListNode from './linked-list-node.js';

export default class DoublyLinkedList {
    #head;    
    #tail;
    #count;

    constructor() {
        this.#head = null;
        this.#tail = null;
        this.#count = 0;
    }

    get head() {
        return this.#head;
    }

    get tail() {
        return this.#tail;
    }

    get count() {
        return this.#count;
    }
    
    addFirst(value) {
        const newNode = new LinkedListNode(value);

        if(this.count === 0) {
            this.#head = newNode;
            this.#tail = newNode;
        } else {
            newNode.next = this.#head;
            this.#head.prev = newNode;
            this.#head = newNode;
        }

        this.#count++;
    }

    addLast(value) {
        const newNode = new LinkedListNode(value);

        if(this.#count === 0) {
            this.#head = newNode;
            this.#tail = newNode;
        } else {
            this.#tail.next = newNode;
            newNode.prev = this.#tail;
            this.#tail = newNode;
        }

        this.#count++;
    }

    removeFirst() {
        if(this.#count === 0) {
            throw new Error('The list is empty!');
        }

        const firstNode = this.#head;

        if(this.count === 1) {
            this.#head = null;
            this.#head = null;
        } else {
            this.#head = this.#head.next;
            this.#head.prev = null;
            firstNode.next = null;
        }

        this.#count--;

        return firstNode.value;
    }

    removeLast() {
        if(this.#count === 0) {
            throw new Error('The list is empty!');
        }

        const lastNode = this.#tail;

        if(this.#count === 1) {
            this.#head = null;
            this.#tail = null;
        } else {
            this.#tail = this.#tail.prev;
            this.#tail.next = null;
            lastNode.prev = null;

        }

        this.#count--;

        return lastNode.value;
    }

    insertBefore(node, value) {
        if(this.#count === 0) {
            throw new Error('The list is empty!');
        }
        
        const newNode = new LinkedListNode(value);

        if(this.#count === 1 || (this.head === node)) {
            this.addFirst(value);
        } else {    
            let current = this.#head;
            while (current) {
                if (current === node) {
                    const beforeNode = node.prev;

                    beforeNode.next = newNode;
                    node.prev = newNode;
                    newNode.next = node;
                    newNode.prev = beforeNode;
                }
                current = current.next;
            }
            this.#count++;
        }

    }

    insertAfter(node, value) {
        if(this.#count === 0) {
            throw new Error('The list is empty!');
        }

        const newNode = new LinkedListNode(value);
        if(this.#count === 1 || (this.#tail === node)) {
            this.addLast(value);
        } else {
            let current = this.#head;
            while (current) {
                if (current === node) {
                    const afterNode = node.next;

                    node.next = newNode;
                    afterNode.prev = newNode;
                    newNode.prev = node;
                    newNode.next = afterNode;
                }
                current = current.next;
            }
            this.#count++;
        }
       
    }

    find(value) {
        let current = this.#head;

        while (current) {
            if(current.value === value) {
                return current;
            }
            current = current.next;
        }

        return null;
    }

    values() {
        let arrVal = [];
        let current = this.#head;

        while (current) {
            arrVal.push(current.value);
            current = current.next;
        }

        return [...arrVal];
    }
}