import DoublyLinkedList from './src/doubly-linked-list.js';

const list = new DoublyLinkedList();

console.log(list.values()); // []
console.log(list.count);    // 0
console.log(list);          // DoublyLinkedList { head: null, tail: null, count: 0 }


list.addFirst(11);
console.log(list.values()); // [ 11 ]

list.addLast(22);
console.log(list.values()); // [ 11, 22 ]
console.log(list.count);    // 2

console.log(list.removeFirst()); // 11
console.log(list.values());      // [ 22 ]

list.addLast(33);
list.addLast(44);
list.addLast(55);
list.addLast(66);
console.log(list.values());     // [ 22, 33, 44, 55, 66 ]

console.log(list.removeLast()); // 66
console.log(list.values());     // [ 22, 33, 44, 55 ]

const node = list.find(33);
console.log(node);              // Node
list.insertAfter(node, 99);
console.log(list.values());     // [ 22, 33, 99, 44, 55 ]

list.insertBefore(node, 88);
console.log(list.values());     // [ 22, 88, 33, 99, 44, 55 ]

console.log(list.count);