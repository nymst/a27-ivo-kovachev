import LinkedListNode from './linked-list-node.js';

export default class DoublyLinkedList {
  #head = null;
  #tail = null;
  #count = 0;

  addFirst(value) {
    const node = new LinkedListNode(value);

    if (this.#head === null) {
      this.#head = node;
      this.#tail = node;
    } else {
      const refToHead = this.#head;
      this.#head = node;
      node.next = refToHead;
      refToHead.prev = node;
    }

    this.#count++;
  }

  addLast(value) {
    const node = new LinkedListNode(value);

    if (this.#head === null) {
      this.#head = node;
      this.#tail = node;
    } else {
      const refToTail = this.#tail;
      this.#tail = node;
      node.prev = refToTail;
      refToTail.next = node;
    }

    this.#count++;
  }

  removeFirst() {
    if (this.#head === null) {
      throw new Error('No elements to remove');
    }
    const refToHead = this.#head.value;
    this.#head = this.#head.next;
    this.#count--;

    return refToHead;
  }

  removeLast() {
    if (this.#head === null) {
      throw new Error('No elements to remove');
    }

    const refToTail = this.#tail.value;
    this.#tail = this.#tail.prev;
    this.#count--;

    return refToTail;
  }

  insertBefore(node, value) {
    const newNode = new LinkedListNode(value);

    if (this.#head === null) {
      throw new Error('Cannot use this method on an empty list.');
    }

    if (node === this.#head) {
      this.#head.prev = newNode;
      newNode.next = this.#head;
      this.#head = newNode;
    } else {
      const refToPrev = node.prev;
      node.prev = newNode;
      newNode.next = node;
      newNode.prev = refToPrev;
      refToPrev.next = newNode;
    }

    this.#count++;
  }

  insertAfter(node, value) {
    const newNode = new LinkedListNode(value);

    if (this.#head === null) {
      throw new Error('Cannot use this method on an empty list.');
    }

    newNode.prev = node;
    newNode.next = node.next;
    node.next = newNode;

    if (node === this.#tail) {
      this.#tail = newNode;
    }

    this.#count++;
  }

  find(val) {
    let refToHead = this.#head;

    if (refToHead === null) {
      return null;
    }

    while (refToHead !== null) {
      if (val === refToHead.value) {
        return refToHead;
      }
      refToHead = refToHead.next;
    }

    return null;
  }

  values() {
    const valuesArray = [];
    let refToHead = this.#head;

    if (refToHead === null) {
      return valuesArray;
    }

    while (refToHead !== null) {
      valuesArray.push(refToHead.value);
      refToHead = refToHead.next;
    }

    return valuesArray;
  }

  get head() {
    return this.#head;
  }

  get tail() {
    return this.#tail;
  }

  get count() {
    return this.#count;
  }
}