// SET

const set = new Set();
// console.log('Size: ' + set.size);
set.add(1);
set.add(3);
set.add(2);
// console.log('Size: ' + set.size);
set.delete(3);
// console.log('Deleted 3: ' + set.delete(3)); // false
// console.log(set.has(2));

for (const values of set) {
  // console.log(values);
}

// const sum = [...set].reduce((s, e) => s + e);
// console.log(sum);
const even = [...set].filter(e => e % 2 === 0);
// console.log(even);
const formatted = [...set].map((e, i) => `At pos ${i + 1}: ${e}`);
// console.log(formatted);


const areAllElementsUnique = iterable => {
  const iterableAsSet = new Set(iterable);

  return iterableAsSet.size === iterable.length;
}



const distinct = iterable => {
  const uniqueElements = new Set(iterable);

  return [...uniqueElements];
}

const uniqueChars = distinct('abbbc').join('');
// console.log(uniqueChars);
const uniqueNumbers = distinct([1, 2, 3, 3, 4, 1]);  // [ 1, 2, 3, 4 ]
// console.log(uniqueNumbers);

const union = (first, second) => new Set([...first, ...second]);

const union1 = union(new Set('abc'), new Set('bce'));
// console.log(union1);


const intersection = (first, second) => {
  const intersection = new Set();


  for (const element of second) {
    if (first.has(element)) {
      intersection.add(element);
    }

  }
  return intersection;

}

const intersection1 = intersection(new Set('abc'), new Set('bce'));             // { 'b', 'c' }
const intersection2 = intersection(new Set('abc'), new Set('def'));             // {}
const intersection3 = intersection(new Set([1, 5, 9]), new Set([3, 5, 8]));     // { 5 }

// console.log(intersection1);
// console.log(intersection2);
// console.log(intersection3);

const diff = (set1, set2) => [...set1].filter(x => ![...set2].includes(x));

const set1 = new Set([1, 3, 5, 7, 9]);
const set2 = new Set([2, 3, 4, 6]);

// console.log(diff(set1, set2));


//MAP

const map = new Map();

map.set('Sofia', 10000);
map.set('Varna', 2000);
map.set('Sofia', 3000);

// console.log([...map]);

map.delete('Sofia');

// console.log([...map]);

// console.log(map.get('Varna'));

map.set('Plovediv', 4000);
for (const [key, value] of map) {
  // console.log(`${key} population: ${value}`);
}

const sum = [...map.keys()].reduce((s, k) => s + map.get(k), 0);
// console.log(sum);


const countOccurences = array => {
  const map = new Map();

  for (const element of array) {
    const count = map.get(element) || 0;
    map.set(element, count + 1);
  }

  return map;
}

const occurrences = countOccurences(['js', 'c#', 'js', 'c#', 'c++']);

// console.log(occurrences);



const areIsomorphic = (s1, s2) => {
  let map = new Map();

  if (s1.length !== s2.length) {
    return false;
  }

  for (let i = 0; i < s1.length; i++) {
    if (!map.has(s1[i])) {
      map.set(s1[i], s2[i]);
    } else if (map.get(s1[i]) !== s2[i]) {
      return false;
    }
  }

  return true;
}
console.log(areIsomorphic('tidal', 'paper'));