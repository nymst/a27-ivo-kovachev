


class Hashtable {
  #fillPercentage = 0.75;
  #count = 0;

  #arr = Array.from({ length: 16 }, () => null);
  #hashCode = (value) => {
    if (typeof value === 'number') {
      return value;
    }

    let hash = 0;
    for (let i = 0; i < value.length; i++) {
      const character = value.charCodeAt(i);
      hash = ((hash<<5)-hash)+character;
      hash = hash & hash; // Convert to 32bit integer
    }
  
    return Math.abs(hash);
  }



  /**
   * 
   * @param {number|string} key 
   * @param {string} value 
   */
  set(key, value) {
    // check if internal storage needs resizing
    if ((this.#count / this.#arr.length) > this.#fillPercentage) {
      // let newArr = Array.from({ length: this.#arr.length * 2 }, () => null);

      // // foreach element in arr
      //   // compute index
      //   // set at newArr[index]

      // this.#arr = newArr;
    }

    const index = this.#hashCode(key) % this.#arr.length;

    this.#arr[index] = value;
    this.#count++;
  }

  size() {
    return this.#count;
  }

  /**
   * 
   * @param {number|string} key 
   * @returns {string}
   */
  get(key) {
    const index = this.#hashCode(key) % this.#arr.length;

    return this.#arr[index];
  }

  /**
  * 
  * @param {number|string} key 
  */
  delete(key) {
    const index = this.#hashCode(key) % this.#arr.length;
    
    this.#arr[index] = null;
    this.#count--;
  }

  /**
   * 
   * @param {number|string} key 
   * @param {string} value 
   */
  update(key, value) {
    if (!this.has(key)) {
      throw new Error('update key does not exist')
    }
    const index = this.#hashCode(key) % this.#arr.length;

    this.#arr[index] = value;
  }

  /**
  * 
  * @param {number|string} key 
  * @returns {boolean}
  */
  has(key) {
    const index = this.#hashCode(key) % this.#arr.length;

    return this.#arr[index] !== null;
  }
}

const ht = new Hashtable();

ht.set('pesho', '08129839');

console.log(ht.get('pesho'));
