
/**
 * Determines if s1 an s2 are isomorphic.
 * @param {string} s1
 * @param {string} s2
 * @returns {boolean} true if isomorphic 
 */
const areIsomorphic = (s1, s2) => {
    let map1 = new Map();
    let map2 = new Map();

    if (s1.length !== s2.length) {
        return false;
    }

    for (let i = 0; i < s1.length; i++) {

        if (!map1.has(s1[i]) && !map2.has(s2[i])) {
            map1.set(s1[i], s2[i]);
            map2.set(s2[i], s1[i]);
        } else if (map1.get(s1[i]) !== map2.get(s2[i]) || map1.get(s2[i]) !== map2.get(s1[i])) {
            return false;
        }
    }

    return true;
}

// Tests:
const testCases = [
    { s1: 'egg', s2: 'add', expected: true },
    { s1: 'aab', s2: 'xyz', expected: false },
    { s1: 'paper', s2: 'title', expected: true },
    { s1: 'tidal', s2: 'paper', expected: false },
];

testCases.forEach(({ s1, s2, expected }, index) => {
    // arrange & act
    const result = areIsomorphic(s1, s2);

    // assert
    const message = result === expected
        ? 'Pass.'
        : `Fail. Expected: ${expected}. Actual: ${result}`

    console.log(`Test ${index + 1}: ${message}`);
});
