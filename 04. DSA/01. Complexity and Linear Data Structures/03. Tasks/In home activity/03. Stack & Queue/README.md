<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## DSA - 03. Stack & Queue

### Definition

Both **Stack & Queue** are linear data structures that provide interface for accessing and adding one element at a time. These structures' data operations generally support destructive update - this means that when we access an element - we **remove** it from the structure.

The interesting thing here is the order in which you add and get an element.

- **Stack** is `LIFO` (Last In First Out) structure​
  - Elements inserted (push) at “top”​
  - Elements removed (pop) from “top”

- **Queue** is `FIFO` (First In First Out) structure​
  - Items inserted at the tail (enqueue)​
  - Items removed from the head (dequeue)​

You can implement both structures in two ways:
  - `Statically` - using resizable array​
  - `Dynamically` - using linked nodes

### Operations

The operations that one may use with the **Stack** are:​

- `push(item)`  →  adds to the `top` of the stack  →  **​O(1)**
- `pop()` →  returns and removes from the `top` of the stack  →  **​O(1)**
- `peek()`  →   returns the top element without removing​  →  **​O(1)**

> Nice, huh! Think why these operations have these complexities?

The operations that one may use with the **Queue** are:​

- `enqueue(item)`  →  adds to the `back` of the queue​  →  **​O(1)**
- `dequeue()` →  returns and removes from the `front​`  →  **​O(1)**
- `peek()`  →   returns the front element without removing  →  **​O(1)**

> Nice, huh! Think why these operations have these complexities?

### Implementation

**Array-based Stack**

Now let's implement our own Stack. First, we'll do the implementation with the array.

1. Create **stack-with-array.js** and define class **Stack**
2. Add private array field **items**. It should be empty at first:

  ```js
    #items = [];
  ```

3. Define the interface of your data structure. It should support all the operations mentioned above:
   
  ```js
    class Stack {
      #items = []

      push(val) {}

      pop() {}

      peek() {}
    }
  ```

4. Now let's implement them one by one
   1. Use array's `push` method to implement adding an item

    ```js
      push(val) {
        this.#items.push(val)
      }
    ```

   2. Use array's `pop` method to implement getting an item - it works as we need it - it removes the item from the structure. Notice that this is a benefit you get from javascript - arrays here allow us to do so.

    ```js
      pop() {
        return this.#items.pop()
      }
    ```

   3. Use array's `indexer ([])` to get value of the item on the top:

    ```js
      peek() {
        return this.#items[this.#items.length - 1];
      }
    ```

5. Great! Now how can we make our stack better than every other array one can create?
   1. What if we try to peek or pop from an empty stack? What would the build-in array do? Let's be safe and add one more method to make sure the structure is not empty. It's okay to expose it - people will need this knowledge:

    ```js
      isEmpty() {
        return this.#items.length === 0;
      }
    ```

   2. Now use it in `pop` and `peek` to make sure you are not poping or indexing empty array.
   3. Let's add the `Symbol.iterator` again, so our testing is easier:

      ```js
        *[Symbol.iterator]() {
          for(let i = 0; i < this.#items.length; i++){
              yield this.#items[i];
          }
        }
      ```

**LinkedList-based Stack**

Now, we'll do the implementation with the linked list.

1. Create **stack-with-linked-list.js** and define class **LinkedStack**
2. Add private array field **top**. It should be null at first - this is an empty stack:

  ```js
    #top = null;
  ```

3. Define the interface of your data structure. It should support all the operations mentioned above:
   
  ```js
    class LinkedStack {
      #top = null

      push(val) {}

      pop() {}

      peek() {}
    }
  ```

4. Now let's implement them one by one
   1. `push` method creates a node and prepends it at the top of the stack. Also changes the top to be the new first element.

    ```js
      push(value) {
        const node = new ListNode(value);
        node.next = this.#top;
        this.#top = node;
      }
    ```

   2. To remove top element, we need to make second node as top and delete memory allocated for first node. We do not have to take care of the deletion - once nobody uses the "old" top element, it will be automatically garbage collected.

    ```js
      pop() {
        const val = this.#top.value;
        this.#top = this.#top.next;

        return val;
      }
    ```

   3. Getting value of the item on the top of the stack is easy:

    ```js
      peek() {
        return this.#top.value;
      }
    ```

5. Great! Now how can we make our stack better than every other array one can create?
   1. What if we try to peek or pop from an empty stack? What will happen if we try to access value of null? Let's be safe and add one more method to make sure the structure is not empty. It's okay to expose it - people will need this knowledge:

    ```js
      isEmpty() {
        return !this.#top;
      }
    ```

   2. Now use it in `pop` and `peek` to make sure you are not poping or indexing empty array.
   3. Let's add the `Symbol.iterator` again, so our testing is easier:

      ```js
        *[Symbol.iterator]() {
          let current = this.#top;
          while (current) {
            yield current.value;
            current = current.next;
          }
        }
      ```

### Testing

Now you can test out your code. Try different operations and make sure your implementation works as the abstract data type should.

```js
  import Stack from './stack-with-array.js';
import LinkedStack from './stack-with-linked-list.js';

const stack = new Stack();

stack.push(1);
stack.push(2);
stack.push(3);
stack.push(4);
stack.push(5);

console.log([...stack]);

console.log(stack.pop());

console.log([...stack]);

console.log(stack.peek());

console.log([...stack]);

const linkedStack = new LinkedStack();

linkedStack.push(1);
linkedStack.push(2);
linkedStack.push(3);
linkedStack.push(4);
linkedStack.push(5);

console.log([...linkedStack]);

console.log(linkedStack.pop());

console.log([...linkedStack]);

console.log(linkedStack.peek());

console.log([...linkedStack]);
```

> Do you notice something interesting? The array-based stack seems to work correctly but when we print it the items are in wrong order? Let's fix this bug, by traversing the array backwards in the `Symbol.iterator`:

```js
  *[Symbol.iterator]() {
    for(let i = this.#items.length - 1; i >= 0 ; i--){
        yield this.#items[i];
    }
  }
```

Now test it again - now both implementations behave the same way.

### Task

This time you have three tasks:

1. Implement array-based queue
2. Implement linkedlist-based queue
3. Implement queue using our Stack / LinkedStack class.

Let's go through the solution of the last one.

Example:
```
const queue = new MyQueue();

queue.enqueue(1);
queue.enqueue(2);  
console.log(queue.peek());  // returns 1
console.log(queue.dequeue());   // returns 1
console.log(queue.isEmpty()); // returns false
```

What do we have?

Queue is FIFO (first in - first out) data structure, in which the elements are inserted from one side - rear and removed from the other - front. Stack is LIFO (last in - first out) data structure, in which elements are added and removed from the same end, called top. 

To satisfy FIFO property of a queue we need to **keep two stacks**. 

They will serve to reverse arrival order of the elements and one of them store the queue elements in their final order.

1. Create **MyQueue** class that have two stack private fields.

  ```js
    #stack1 = new Stack();
    #stack2 = new Stack();
  ```

2. This time we'll start with the `isEmpty` function, so we can use it in our implementation. The queue is empty if both stacks are empty:

  ```js
    isEmpty() {
      return this.#s1.isEmpty() && this.#s2.isEmpty();
    }
  ```

3. To implement `enqueue` let's use s1. The newly arrived element is always added on top of stack s1 and the first element is kept as front queue element:

  ```js
    enqueue(val) {
      this.#s1.push(val);
    }
  ```

3. Now the `dequeue` is the tricky one. We need to get the first element that is added, but this means to grap from the bottom of the stack and this is not possible. Here comes the second stack. If its empty we'll transfer all s1's elements and pop from there:

  ```js
    dequeue() {
      if (this.#s2.isEmpty()) {
        while (!this.#s1.isEmpty()) {
          const elem = this.#s1.pop()
          this.#s2.push(elem);
        }
      }

      return this.#s2.pop();
    }
  ```

  > What is the complexity of `dequeue` operation? Think not only about the items transferring. In the average case we will be adding element in the s1 with constant complexity and getting it from s2 again with constant complexity.

4. We'll do the same with `peek`, just will use s2's peek operation:

   ```js
    peek() {
      if (this.#s2.isEmpty()) {
        while (!this.#s1.isEmpty()) {
          const elem = this.#s1.pop()
          this.#s2.push(elem);
        }
      }

      return this.#s2.peek();
    }
  ```

> Can you see the repeated code - may be extract it in a private method?

5. Test it out.