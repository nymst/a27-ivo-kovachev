<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## DSA - 02. Linked List

### Definition

**Linked List** is a linear data structure that implements the abstract data structure **List** **dynamically**. While using an array defines that elements of the Array List are stored sequentially in memory, Linked List's items are not.

The main idea is that each element knows what is / are its neighbours. There are two possible implementations:
- Singly linked list - each element knows what is the next one
- Doubly linked list - each element knows what are the next and the previous ones

### Singly Linked List

We will work today with a **singly-linked-list ​with only a head reference**. ​
### Operations

The operations that one may use with the Linked List are:​

- `addFirst(value)`  →  creates node at the front​  →  **​O(1)**
- `removeFirst()` →  removes first node​  →  **​O(1)**
- `insertAfter(node, value)`  →  creates node after given node  →  ​**​O(1)**
- `removeAfter(node) `  →  removes the node after given node  →  ​**​O(1)**
- `find(value)`  →  returns first node with the given value  →  **​O(n)**

*Note:* the functions and their complexity assume a singly-linked-list ​with only a head reference.

> Think why these operations have these complexities?

### Implementation

Now let's implement our own Singly Linked List.

1. Create **list-node.js** and define class **ListNode**. We will use this class to represent **singly linked node**. This class will represent our list's elements. Each element has a value and knows what is the next element. We don't have strong types in js, but we need to keep in mind that `next` should be instance of the same `ListNode` class.
   
   ```js
    export default class ListNode {
        constructor(value, next) {
            this.value = value;
            this.next = next || null;
        }
    }
   ```

2. Create **linked-list.js** and define class **LinkedList**
3. Add private field **head**. It should be null at first - this is an empty list:

  ```js
    #head = null;
  ```

3. Define the interface of your data structure. It should support all the operations mentioned above:
   
  ```js
    class LinkedList {
      #head = null;

      addFirst(value) {}

      removeFirst() {}

      insertAfter(node, value) {}

      removeAfter(node) {}

      find(value) {}
    }
  ```

4. Now let's implement them one by one
   1. `addFirst` method creates a node and prepends it at the beginning of the list. Also changes the head to be the new first element.

      ```js
        addFirst(value) {
          const node = new ListNode(value);
          node.next = this.#head;
          this.#head = node;
        }
      ```

   2. To remove first node, we need to make second node as head and delete memory allocated for first node. We do not have to take care of the deletion - once nobody uses the "old" first element, it will be automatically garbage collected.
   
      > Don't forget to make sure you are not removing an item from an empty list!

      ```js
        removeFirst() {
          if (this.#head === null) {
              throw new Error('List is empty'); 
          }

          const val = this.#head.value;
          this.#head = this.#head.next;

          return val;
        }
      ```

        > Will this method work if we have only one element? Why?

   3. Now for the `insertAfter` method we are given pointer to a node, and the new node is inserted after the given node:

      ```js
        insertAfter(node, value) {
          const newNode = new ListNode(value);
          newNode.next = node.next;
          node.next = newNode;
        }
      ```

        > How would you implemented `insertLast(value)` method? What would be its complexity?

   4. Our singly linked list nodes do not store the reference to the previous node and only have a pointer for the next node. To delete a node we need to know the address of the previous node of the actual node we want to delete so that we can keep the linked list connected after deletion of a node. This is what `removeAfter` method should do.


        > Make sure you are not trying to renove **after** the last element!

      ```js
        removeAfter(node) {
          if (node.next) {
              node.next = node.next.next;
          }
        }
      ```

      > In some implementations singly linked lists have not only **head** to keep track of the first element, but also **tail** to keep track of the last one. Will this be helpful for implementation of this method?

   5. Eventhough we are missing an indexer, we still need to have a way to get a particular element by its value. Let's implement the `find` method. We need to start with the head and access each node until we reach element with the value we look for or null. 
   
      We will not change the head reference while doing this.

      ```js
        find(value) {
          let ref = this.#head;
          while (ref) {
              if (value === ref.value) {
                  return ref;
              }
              ref = ref.next;
          }

          return null;
        }
      ```

5. Great! But wait, we are missing the `values()` function we had in the Array List. It was implemented in the following manner:
   
    ```js
      values() {
        return [...this.#items];
      }
    ```
    This is possible because we used an array for the representation of the elements in the memory. How can we use `destructuring` or `for...of` over our linked list?
    
    We can use the so-called `Symbol.iterator` that specifies the default iterator for an object. It goes this way:

    ```js
      *[Symbol.iterator]() {
        let current = this.#head;
        while (current) {
            // notice the yield statement - it passes the next value to thе for .. of statement
            yield current.value;
            current = current.next;
        }
      }
    ```

  Now our lst is **iterable**.

### Testing

Now you can test out your code. Try different operations and make sure your implementation works as the abstract data type should.

```js
  import LinkedList from './linked-list.js';

  const list = new LinkedList();

  list.addFirst(4);
  list.addFirst(3);
  list.addFirst(2);
  list.addFirst(1);

  for (const val of list) {
      console.log(val);
  }

  const nodeToRemore = list.find(2);
  list.removeAfter(nodeToRemore);

  console.log([...list]);
```

It's a bit tedious to add elements one by one. Perhaps we can implement another method `from(array)` that will take an array and make it list. 

Be careful - the only way of adding an elements you know is by putting them in from of the list!

Did you implemented it similarly to:

```js
  from(arr) {
    for (let i = arr.length - 1; i >= 0; i--) {
      this.addFirst(arr[i]);
    }
  }
```

### Task

**Delete N nodes after M nodes of a linked list**

Given a linked list and two integers M and N. Traverse the linked list such that you retain M nodes then delete next N nodes, continue the same till end of the linked list.

Example:
```
M = 2, N = 2
Input:
Linked List: 1->2->3->4->5->6->7->8
Output:
Linked List: 1->2->5->6
```

The idea is very simple. We traverse the given list and skip first M nodes and delete next N nodes in it and recur for remaining nodes. The solution is simple but we need to make sure that all boundary conditions are handled properly in the code.

1. Start by constructing the variables. We'll make good use of the `from` function.

  ```js
    const list = new List();
    list.from([1, 2, 3, 4, 5, 6, 7, 8]);
    const m = 2;
    const n = 2;

    console.log([...list]);
  ```

2. Now we need to skip first M elements. What to keep in mind that if M is bigger number than the elements in our list at the end of the traversing we'll have no node to remove after.

We need to remeber the last node we will not skip. As we implemented the `Symbol.iterator` to return just the value of the element, we'll use `find` to get the node object. 

Or you go and change the implementation of the `Symbol.iterator` and `yield` the whole element, instead.

  ```js
    let currentItemIndex = 1;
    let lastSurvival = null;

    for (let item of list) {
      if (currentItemIndex === m) {
        lastSurvival = list.find(item);
        break;
      }
      currentItemIndex++;
    }

    if (!lastSurvival) {
      console.log(`You try to skip ${m} elements, but you only have ${currentItemIndex - 1}`);
    } else {
      // remove next n elements
    }
  ```

3. Now we need to remove the next N elements. The corner case here is if we try to remove more elements that we have in the list. You remember that we handled this in the `removeAfter` method, but it will be nice to stop the cycle if we know we reached the end:

```js
  for (let i = 0; i < n; i++) {
    list.removeAfter(lastSurvival)
    if(!lastSurvival.next){
      console.log(`The list reached its end. We removed ${i} elements.`);
      break;
    }
  }
```

1. Let's print out the list and make sure the algorithm works:

```js
console.log([...list]);
```

> How would you solve this task if you haven't implemented the `Symbol.iterator`?

> What are the main differences between **Linked List** and **Array List**?

> What data structure you'd use if you have to modify your data a lot - you add and remove elements all the time? Why?

> What data structure you'd use if you need to access its elements frequently, but rarely modify them? Why?