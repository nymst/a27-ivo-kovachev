<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## DSA - 01. Array List

### Definition

**Array List** is a linear data structure that implements the abstract data srtucture **List** by using an **array**. 

### Operations

The operations that one may use with the Array List are:​

- `add(item)`  →  appends given element at the end  →  **​O(1)**
- `get(index)` →  gets element at the specified index  →  **​O(1)**
- `insert(index, item)`  →  insert item at index  →  ​**​O(n)**
- `remove(index)`  →  removes the item at index  →  ​**​O(n)**
- `size()`  →  return the current number of items​  →  **​O(1)**


> Think why these operations have these complexities?

### Implementation

Now let's implement our own Array List.

1. Create **list.js** and define class **List**
2. Add private array field **items**. It should be empty at first:

  ```js
    #items = [];
  ```

3. Define the interface of your data structure. It should support all the operations mentioned above:
   
  ```js
    class List {
      #items = []

      add(item) {}

      get(index) {}

      set(index, item) {}

      insert(index, item) {}

      remove(index) {}

      size() {}
  }
  ```

4. Now let's implement them one by one
   1. Use array's `push` method to impelemt adding an item

    ```js
      add(item) {
        this.#items.push(item);
      }
    ```

   2. Use `array's indexer ([])` to get a particular item

    ```js
      get(index) {
        return this.#items[index];
      }
    ```

   3. Use array's `indexer ([])` to set value to particular item:

    ```js
      set(index, item) {
        return this.#items[index] = item;
      }
    ```

   4. Use `splice` to implement insert operation:

    ```js
      insert(index, item) {
        this.#items.splice(index, 0, item);
      }
    ```

   5. Use `splice` to implement remove operation:

    ```js
      remove(index) {
          this.#items.splice(index, 1);
      }
    ```

   6. Use array's `length` method to get the size of your list
   
    ```js
      size() {
        return this.#items.length;
      }
    ```

5. Great! Now how can we make our list better than every other array one can create?
   1. We have lots of operations that use array's index. Let's make sure we always work with valid for the specific instance index. Add private method to validate the index:

    ```js
      #checkRange(index) {
        if (typeof index !== 'number' || index < 0 || index >= this.#items.length) {
            throw new Error(`Index ${index} out of range`);
        }
      }
    ```

   2. Now use it in every method you rely on the index. This way if we have for example list with 10 items and someone tries to get the 100th item, they will get nice and explanatory error.
   3. It will be nice to have an easy way to get the values of the list. Let's implement method to get them:
   
    ```js
      values() {
        return [...this.#items];
      }
    ```

> Why do we make copy? What important practice of the functional programming we follow this way?

### Testing

Now you can test out your code. Try different operations and make sure your implementation works as the abstract data type should.

```js
  import List from './list.js';

  const list = new List();

  list.add(1);
  list.add(2);
  list.add(3);


  list.insert(1, 7);
  //list.insert(10, 7);

  console.log(list.values());

  console.log(list.size());
```

### Task

Find the biggest number in list of integers and move it at the beginning of the list. Use the class **List** you've created. 

1. You need to build the following list. You sure can use the `add` method:

  ```js
    [12, 15, 17, 1, 10, 17]
  ```

2. Let's find the max element of the list
   1. Use `size()` to iterate through the list
   2. Use `get(index)` to get an item and check if it's the biggest

    ```js
      let maxElemValue = Number.MIN_VALUE;
      let maxElemIndex = -1;
      for (let index = 0; index < list.size(); index++) {
        const currentElem = list.get(index);
        if (currentElem > maxElemValue){
          maxElemValue = currentElem;
          maxElemIndex = index;
        }
      }
    ```

3. Now move the lement in front of the list. This actually are two operations:
   1. Remove the item with the index on which you've found the max element
   2. Insert at first place the maximum value

    ```js
      list.remove(maxElemIndex);
      list.insert(0, maxElemValue);
    ```

4. Let's see the result
   
    ```js
      console.log(list.values());
    ```

> What do you think is the complexity of finding this element? Why?

> What if you needed to remove all elements that have maximum value and keep only one in the head of the list? How you'd change the code to solve this task?

> Iterating through the items does not seem very functional, right? How about we add one more method to our list - `forEach`. It should work similarly to the one we have in the array prototype:

  ```js
    list.forEach(func(index, currentElement) => {});
  ```

Did you implemented it like:

  ```js
    forEach(func) {
    for (let index = 0; index < this.#items.length; index++) {
      func(index, this.#items[index]);
    }
  }
  ```

  Now we can get rid of the for cycle:

  ```js
    list.forEach((index, currentElem) => {
      if (currentElem >= maxElemValue) {
        maxElemValue = currentElem;
        maxElemIndex = index;
      }
    })
  ```
