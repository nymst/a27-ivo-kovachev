import { Stack } from './common/stack.js';
/**
 * 
 * @param {string} expression Expression to validate
 * @returns {boolean} true if all brackets match 
 */
const validateParentheses = (expression) => {
    // TODO: your implementation:
}

// Tests:
const testCases = [
    { test: `(1 + (2 * 3))`, expected: true },
    { test: `1 + (2 * 3))`, expected: false },
    { test: `(1 + )2 * 3))`, expected: false },
    { test: `(1 + (2 * 3)`, expected: false },
    { test: `((((5 / 2) + 8) - 1 ) * 3) + 12`, expected: true },
    { test: `)12 + 3 + (2 * 8)`, expected: false }
];

testCases.forEach(({ test, expected }, index) => {
    // act
    const result = validateParentheses(test);

    // assert
    const message = (result === expected)
        ? 'Pass.'
        : `Fail. Expected: ${expected}. Actual: ${result}`

    console.log(`Test ${index + 1}: ${message}`);
});
