import { ListNode, createLinkedList, compareLists, serializeList } from './common/linked-list.js';
/**
 * 
 * @param {ListNode} head Reference to the head node of a list
 * @returns {ListNode} Reference to the head of the reversed list 
 */
const reverseList = (head) => {
    let prev = null;

    while(head) {
        const tempNext = head.next;
        head.next = prev;
        prev = head;
        head = tempNext;
    }

    return prev;
}

// Tests:
const testCases = [
    { test: [1, 2, 3], expected: [3, 2, 1] },
    { test: [1, 2, 3, 4], expected: [4, 3, 2, 1] },
    { test: [1, 2], expected: [2, 1] },
    { test: [1], expected: [1] }
];

testCases.forEach(({ test, expected }, index) => {
    // arrange
    const testList = createLinkedList(...test);
    const expectedList = createLinkedList(...expected);

    // act
    const reversed = reverseList(testList);

    // assert
    const result = compareLists(expectedList, reversed);
    const message = result
        ? 'Pass.'
        : `Fail. Expected: ${serializeList(expectedList)}. Actual: ${serializeList(reversed)}`

    console.log(`Test ${index + 1}: ${message}`);
});
