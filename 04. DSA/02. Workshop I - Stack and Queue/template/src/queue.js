import LinkedListNode from "./linked-list-node.js";

export default class Queue {
  #head = null;
  #tail = null;
  #length = 0;
  enqueue(value) {
    const node = new LinkedListNode(value, null);

    if (!this.#head) {
      this.#head = node;
    } else {
      this.#tail.next = node;
    }

    this.#tail = node;
    this.#length++;
  }

  dequeue() {
    if (!this.#head) {
      throw new Error('Queue is empty');
    }

    const val = this.#head.value;
    this.#head = this.#head.next;
    this.#length--;

    return val;
  }

  peek() {
    if (!this.#head) {
      throw new Error('Queue is empty');
    }

    return this.#head.value;
  }

  get count() {
    return this.#length;
  }

  get isEmpty() {
    return !this.#head;
  }
}