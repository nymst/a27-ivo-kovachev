import LinkedListNode from "./linked-list-node.js";

export default class Stack {
  #top = null;
  #length = 0;

  push(val) {
    const node = new LinkedListNode(val);
    node.next = this.#top;
    this.#top = node;
    this.#length++;
  }

  pop() {
    if(!this.#top) {
      throw new Error('Stack is empty');
    }

    const saveVal = this.#top.value;
    this.#top = this.#top.next;
    this.#length--;

    return saveVal;
  }

  peek() {
    if(!this.#top) {
      throw new Error('Stack is empty');
    }

    return this.#top.value;
  }

  get count() {
    return this.#length;
  }

  get isEmpty() {
    return !this.#top;
  }
}