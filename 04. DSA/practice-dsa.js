const LL = {
  val: 5,
  next: {
    val: 4,
    next: {
      val: 8,
      next: null
    }
  }
}

const getAtPosition = (linkedList, position) => {
  while (--position > 0) {
    if (linkedList.next == null) {
      return null;
    }
    linkedList = linkedList.next;
  }

  return linkedList.val;
}

// console.log(getAtPosition(LL, 2));



let node = {
  val: 1,
}

node.next = {
  val: 2
}

node.next.next = {
  val: 3
}

node.next.next.next = {
  val: 4,
  next: null
}

function printList(node) {
  while (node != null) {
    console.log(node.val)
    node = node.next;
  }
}
// printList(node);
function insertAfter(node, val) {
  node.next = {
    val: val,
    next: node.next
  }
}

// insertAfter(node.next.next, 15)

// printList(node);