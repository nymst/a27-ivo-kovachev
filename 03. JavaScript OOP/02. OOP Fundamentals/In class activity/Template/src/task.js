import { status } from './status.js';

/** The Task class holds all relevant data and behavior a task might have. */
export class Task {
  // One way to indicate that a field should not be touched is to put an underscore before its name.
  // This won't prevent your colleges from directly interacting with it.

  _name;
  _dueDate;
  _status;

  static minNameLength = 3;
  static maxNameLength = 30;

  constructor(name, dueDate) {
    this._name = name;
    this._dueDate = dueDate;
    this._status = status.TODO;
  }

  changeName(value) {
    if (!value) {
      throw new Error('Invalid name value');
    }

    if (value.length < Task.minNameLength || value.length > Task.maxNameLength) {
      throw new Error('Invalid name length');
    }

    this._name = value;
  }

  changeDueDate(value) {
    if (!value) {
      throw new Error('Invalid date value');
    }

    if (value < new Date().toLocaleString()) {
      throw new Error('Cannot set to a date in the past');
    }

    this._dueDate = value;
  }

  reset() {
    this._status = status.TODO;
  }

  advance() {
    this._status = status.IN_PROGRESS;
  }

  complete() {
    this._status = status.DONE;
  }

  toString() {
    return `Name: ${this._name}\n` +
           `Status: ${this._status}\n`+
           `Due: ${this._dueDate.toLocaleDateString()} ${this._dueDate.toLocaleTimeString()}`;
    
  }
}

const task1 = new Task ('random', new Date());
console.log(task1.toString());