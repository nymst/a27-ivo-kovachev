/** The Board class holds tasks */
export class Board {
  _tasks;

  constructor(){
    this._tasks = [];
  }

  add(task) {
    const taskIndex = this._tasks.findIndex(existingTask => existingTask === task);
    
    if(taskIndex >= 0){
      throw new Error('This task already exists in the board');
    }

    this._tasks.push(task);
  }

  remove(task) {
    const taskIndex = this._tasks.findIndex(existingTask => existingTask === task);

    if(taskIndex < 0){
      throw new Error('This task does not exist in the board');
    }

    this._tasks.splice(taskIndex,1);
  }

  toString() {
    const taskBoardText = '---Task Board---\n';
    const tasksText = 'Tasks:\n';

    if (this._tasks.length < 1) {
      return `${taskBoardText}\n${tasksText}No tasks at the moment.`;
    } else {
      return `${taskBoardText}\n${tasksText}\n${this._tasks.join('\n-----\n').toString()}`;
    }
  }
}