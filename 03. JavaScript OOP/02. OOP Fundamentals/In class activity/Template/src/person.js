class Person {
  _firstName;
  _lastName;

  constructor (firstName, lastName) {
    this.firstName = firstName;
    this.changeLastName(lastName);
  }

  get firstName(){
    return this._firstName;
  }

  set firstName(value){
    this._firstName = value;
  }

  changeLastName(value){
    this._lastName = value;
  }

  returnLastName(){
    return this._lastName;
  }
}

const person1 = new Person ('Ivo', 'Kovachev');
console.log(person1.firstName, person1.returnLastName());
