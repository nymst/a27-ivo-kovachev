/* eslint-disable no-undef */
import { Board } from '../src/board';

describe('Board', () => {

  describe('Constructor', () => {

    it('should initialize tasks as an empty array', () => {
      const board = new Board();

      expect(board._tasks).toBeInstanceOf(Array);
      expect(board._tasks.length).toBe(0);
    });

  });

  describe('add', () => {

    it('should correctly add the task to the tasks array', () => {
      const board = new Board();
      const mockTask = {};

      board.add(mockTask);
      
      expect(board._tasks).toContain(mockTask);
    });

    it('should throw when we add the same task more than once', () => {
      const board = new Board();
      const mockTask = {};

      board.add(mockTask);
      
      expect(() => board.add(mockTask)).toThrow();
    });

  });

  describe('remove', () => {

    it('should remove the correct task from the tasks array', () => {
      const board = new Board();
      const mockTask1 = {};
      const mockTask2 = {};
      board._tasks = [mockTask1, mockTask2];

      board.remove(mockTask1);

      expect(board._tasks.length).toBe(1);
      expect(board._tasks).toContain(mockTask2);
      expect(board._tasks).not.toContain(mockTask1);
    });

    it('should throw if the task to is not in the array', () => {
      const board = new Board();
      const mockTask1 = {};
      const mockTask2 = {};
      board._tasks = [mockTask1];

      expect(() => board.remove(mockTask2)).toThrow();
    });

  });

  describe('toString', () => {

    it('should return the correct value when there are not tasks in the board', () => {
      const board = new Board();

      expect(board.toString()).toBe('No tasks at the moment.');
    });

    it('should return the correct value when there are some tasks in the board', () => {
      const board = new Board();
      const mockTask1 = {
        toString: function() {
          return 'mock task 1';
        },
      };
      const mockTask2 = {
        toString: function() {
          return 'mock task 2';
        },
      };
      board._tasks = [mockTask1, mockTask2];

      expect(board.toString()).toContain('---Task Board---');
      expect(board.toString()).toContain('mock task 1');
      expect(board.toString()).toContain('mock task 2');
    });

  });

});