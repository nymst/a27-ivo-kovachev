/* eslint-disable no-undef */
import { Task } from '../src/task';
import { status } from '../src/status';

describe('Task', () => {

  describe('Constructor', () => {

    it('should initialize all the fields correctly', () => {
      const task = new Task('test task', new Date('2020/09/03'));

      expect(task._name).toBe('test task');
      expect(task._dueDate.valueOf()).toBe(new Date('2020/09/03').valueOf());
      expect(task._status).toBe(status.TODO);
    });

  });

  describe('changeName', () => {

    it('should throw if no name was passed', () => {
      const task = new Task('test task', new Date('2020/09/03'));

      expect(() => task.changeName(undefined)).toThrow();
    });
    
    it('should throw if name with invalid length was passed', () => {
      const task = new Task('test task', new Date('2020/09/03'));

      expect(() => task.changeName('test')).toThrow();
    });

    it('should correctly update the name when valid name was passed', () => {
      const task = new Task('test task', new Date('2020/09/03'));
      task.changeName('new name');

      expect(task._name).toBe('new name');
    });

  });

  describe('changeDueDate', () => {

    it('should throw if no dueDate was passed', () => {
      const task = new Task('test task', new Date('2020/09/03'));

      expect(() => task.changeDueDate(undefined)).toThrow();
    });
    
    it('should throw if the date passed is in the pass', () => {
      const task = new Task('test task', new Date('2020/09/03'));

      expect(() => task.changeDueDate(new Date('1999/09/03'))).toThrow();
    });

    it('should correctly update dueDate when valid date was passed', () => {
      const task = new Task('test task', new Date('2020/09/03'));
      task.changeDueDate(new Date('2030/09/03'));

      expect(task._dueDate.valueOf()).toBe(new Date('2030/09/03').valueOf());
    });

  });

  describe('reset', () => {

    it('should set the status to TODO', () => {
      const task = new Task('test task', new Date('2020/09/03'));
      task._status = status.IN_PROGRESS;

      task.reset();

      expect(task._status).toBe(status.TODO);
    });

  });

  describe('advance', () => {

    it('should set the status to IN_PROGRESS', () => {
      const task = new Task('test task', new Date('2020/09/03'));
      task._status = status.TODO;

      task.advance();

      expect(task._status).toBe(status.IN_PROGRESS);
    });

  });

  describe('complete', () => {

    it('should set the status to DONE', () => {
      const task = new Task('test task', new Date('2020/09/03'));
      task._status = status.TODO;

      task.complete();

      expect(task._status).toBe(status.DONE);
    });

  });

  describe('toString', () => {

    it('should return the correct string', () => {
      const task = new Task('test task', new Date('2020/09/03'));

      expect(task.toString()).toBe('Name: test task\nStatus: Todo\nDue: 9/3/2020 12:00:00 AM');
    });

  });

});