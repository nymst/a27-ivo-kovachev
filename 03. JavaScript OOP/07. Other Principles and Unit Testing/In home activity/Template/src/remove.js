/**
 * Removes a specified range of characters from an input string
 * @param {string} str the input string
 * @param {start} number the starting index (inclusive)
 * @param {end?} number the ending index (exclusive, defaults to str.length)
 * @returns {string} string with specified range removed
 */
export const remove = (str, start, end = str.length) => {
  if (str === undefined || str === null) {
    throw new Error('str must be defined');
  }

  if (start < 0 || end > str.length) {
    throw new Error('start and end must be within the boundaries of the string');
  }

  if (end <= start) {
    throw new Error('end must be greater than start');
  }

  const buffer = [];
  for (let i = 0; i < start; i++)
    buffer.push(str.charAt(i));
  for (let i = end; i < str.length; i++)
    buffer.push(str.charAt(i));

  return buffer.join('');
};

const test = 'random string';
const cutTest = remove(test, 3, 5);
console.log(cutTest);