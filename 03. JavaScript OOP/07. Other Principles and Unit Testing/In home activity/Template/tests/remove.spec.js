/* eslint-disable no-undef */
import { remove } from './../src/remove.js';

describe('remove', () => {
  it('should produce correct string', () => {
    // Arrange
    const test = 'telerik academy';
    const expected = 'tele academy';

    // Act
    const actual = remove(test, 4, 7);

    // Assert
    expect(actual).toBe(expected);
  });

  it('should produce correct string (only start index given)', () => {
    // Arrange
    const test = 'random string';
    const expected = 'ran';

    // Act
    const actual = remove(test, 3);
    
    // Assert
    expect(actual).toBe(expected);
  });

  
});

/**
 * 
 */