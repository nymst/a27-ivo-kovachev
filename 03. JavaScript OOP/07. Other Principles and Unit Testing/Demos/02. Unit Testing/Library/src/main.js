import { Book } from './models/book.model.js';
import { Library } from './models/library.model.js';

const sofiaLib = new Library('Sofia Central Library');
const ydkjsBook = new Book('YDKJS', 'Kyle Simpson');

sofiaLib.books.push(ydkjsBook);
console.log(sofiaLib.books);

sofiaLib.addBook(ydkjsBook);
console.log(sofiaLib.books);

// sofiaLib.addBook(''); - Throws error
// sofiaLib.addBook({name: 'Awesome book'}); - Throws error
// sofiaLib.addBook({name: 'Awesome book', authorName: 'Awesome Person'}); - Throws error