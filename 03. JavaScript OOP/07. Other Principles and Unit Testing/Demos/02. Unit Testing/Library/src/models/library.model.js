import { Book } from './book.model.js';

export class Library {
  #name;
  #books;

  constructor(name) {
    this.name = name;
    this.#books = [];
  }

  get name() {
    return this.#name;
  }

  set name(value) {
    if(!value) {
      throw new Error('Library name cannot be empty!');
    }

    this.#name = value;
  }

  /** 
   * @returns A copy of the books array held inside. 
   * @see Use .addBook() to modify the array.
   */
  get books() {
    // Returns a copy so that nobody can interact directly with the array in #books.
    // This forces people to use .addBook() and .removeBook().
    return this.#books.slice();
  }

  addBook(book) {
    if (!book || !(book instanceof Book)) {
      throw new Error('The passed value is not a valid instance of Book!')
    }

    if (this.#books.some(existingBook => existingBook.name === book.name)) {
      throw new Error('This book already exists in this library!');
    }

    // We access the fields directly because the property returns a copy of this array.
    this.#books.push(book);
  }
}
