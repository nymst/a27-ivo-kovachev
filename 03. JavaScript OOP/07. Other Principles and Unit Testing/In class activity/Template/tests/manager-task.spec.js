/* eslint-disable no-undef */

import { projectType } from '../src/common/project-type.enum.js';
import { Task } from '../src/models/board-items/task.model.js';
import { ManagerTask } from '../src/models/board-items/tasks/manager-task.model.js';
import { Employee } from '../src/models/employee.model.js';

const tomorrow = () => {
  let date = new Date();
  date.setDate(date.getDate() + 1);
  return date;
};

describe('Manager Task', () => {
  describe('Class', () => {
    it('should extend Task', () => {
      expect(ManagerTask.prototype).toBeInstanceOf(Task);
    });
  });

  describe('Constructor', () => {
    it('constructor should throw if passed projectType is invalid', () => {
      expect(() => new ManagerTask('Bat Gosho', tomorrow(), 'invalid', new Employee('Pesho'))).toThrow();
    });

    it('constructor should throw if passed assignee is not a Employee object', () => {
      expect(() => new ManagerTask('Bat Gosho', tomorrow(), projectType.EXTERNAL_PRODUCT, null)).toThrow();
    });

    it('constructor should assign correctly the values of projectType and assignee', () => {
      //TODO: inArray checks on keys but not on values!!
      const assignee = new Employee('Pesho');
      const managerTask = new ManagerTask('Bat Gosho', tomorrow(), projectType.EXTERNAL_PRODUCT, assignee.name);

      expect(managerTask.projectType).toBe(projectType.EXTERNAL_PRODUCT);
      expect(managerTask.assignee).toBe(assignee);
    });
  });
});