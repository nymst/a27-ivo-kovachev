import { Task } from '../task.model.js';
import * as validators from '../../../common/validators.js';
import { projectType } from '../../../common/project-type.enum.js';

export class ManagerTask extends Task {
  #projectType;

  constructor(name, dueDate, typeOfProject, assignee) {
    super(name, dueDate);
    validators.inArray(typeOfProject, Object.keys(projectType));
    this.#projectType = typeOfProject;
    this.assignee = assignee;
  }

  get projectType() {
    return this.#projectType;
  }
  
}
