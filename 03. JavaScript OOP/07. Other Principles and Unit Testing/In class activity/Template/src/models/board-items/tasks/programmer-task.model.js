import { Task } from '../task.model.js';
import * as validators from '../../../common/validators.js';
import { programmerLevel } from '../../../common/programmer-level.enum.js';

export class ProgrammerTask extends Task{
  #programmerLevel;

  constructor(name, dueDate, levelOfProgrammer, assignee) {
    super(name, dueDate);
    validators.inArray(levelOfProgrammer, Object.keys(programmerLevel));
    this.#programmerLevel = levelOfProgrammer;
    this.assignee = assignee;
  }

  get programmerLevel() {
    return this.#programmerLevel;
  }
}