import { Task } from './board-items/task.model.js';
import { Board } from './board.model.js';
import * as validators from '../common/validators.js';


export class TaskBoard extends Board {
  
  add(item) {
    validators.instanceOf(item, Task);

    const itemIndex = this.items.findIndex(existingItem => existingItem === item);

    if (itemIndex >= 0) {
      throw new Error('The provided item already exists in this board!');
    }

    this.items.push(item);
  }
}
