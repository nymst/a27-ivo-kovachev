export class Employee {
  #id;
  #name;

  static LATEST_ID = 1;
  static MIN_NAME_LENGTH = 5;
  static MAX_NAME_LENGTH = 40;

  constructor(name) {
    this.#id = Employee.LATEST_ID++;

    if (!name || name === '') {
      throw new Error('Name cannot be empty');
    }

    if (name < Employee.MIN_NAME_LENGTH || name > Employee.MAX_NAME_LENGTH) {
      throw new Error('Name length not within the constraints');
    }

    this.#name = name;
  }

  get id() {
    return this.#id;
  }

  get name() {
    return this.#name;
  }
}
