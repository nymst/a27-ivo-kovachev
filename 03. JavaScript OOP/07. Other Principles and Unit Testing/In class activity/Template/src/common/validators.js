
const inArray = (value, array) => {
  if (array.indexOf(value) === -1) {
    throw new Error('Does not exist in the array');
  }
};

const instanceOf = (value, instance) => {
  if (!(value instanceof instance)) {
    throw new Error(`Not instance of ${instance}`);
  }
};
export { inArray, instanceOf };