import { Task } from './task.model.js';

/** The Board class holds tasks */
export class Board {
  #tasks;

  constructor() {
    this.tasks = [];
  }

  /**
   * Returns the count of the tasks in the tasks array.
   */
  get taskCount(){
    return this.tasks.length;
  }

  /** Validates if the task is not already existing in the board and if it is not
   *  from a different instance. Afterwards adds a new task to the board. 
   *  @param {object} task the task to be added in the board
   */
  add(task) {
    const taskIndex = this.tasks.findIndex(existingTask => existingTask === task);

    if (taskIndex >= 0) {
      throw new Error('This task already exists in the board');
    }

    if (task instanceof Task === false) {
      throw new Error('Cannot add tasks from a different instance.');
    }

    this.tasks.push(task);
  }

  /** Validates if the task to be removed exists in the board
   *  and after that removes it from the board.
   *  @param {object} task the task to be removed from the board
   */
  remove(task) {
    const taskIndex = this.tasks.findIndex(existingTask => existingTask === task);

    if (taskIndex < 0) {
      throw new Error('Such task does not exist!');
    }

    this.tasks.splice(taskIndex, 1);
  }

  /** Transforms the board data into a formatted string. */
  toString() {
    const titles = '---Task Board---\n\nTasks:\n\n';

    if (this.tasks.length) {
      return titles + this.tasks.join('\n--------\n');
    }

    return `${titles}No tasks at the moment.`;
  }
}
