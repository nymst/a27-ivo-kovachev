import { status } from '../common/status.enum.js';

/** The Task class holds all relevant data and behavior a task might have. */
export class Task {
  // One way to indicate that a field should not be touched is to put an underscore before its name.
  // This won't prevent your colleges from directly interacting with it.

  #name;
  #dueDate;
  #status;

  static MIN_LENGTH = 6;
  static MAX_LENGTH = 20;

  /**
   * 
   * @param {string} name the parameter taking the task's name
   * @param {string} dueDate the parameter receiving the task's due date. 
   */
  constructor(name, dueDate) {
    this.name = name;
    this.dueDate = dueDate;
    this.status = status.TODO;
  }

  /**
   * @readonly
   * @returns returns the private field "#name"'s value, which is the task name
   */
  get name() {
    return this.#name;
  }

  /**
   * Takes the value parameter, validates it and sets it to the task name
   * @param {string} value the name of the task
   */
  set name(value) {
    if (!value || value === '') {
      throw new Error('Name cannot be empty');
    }

    if (value.length < Task.MIN_LENGTH || value.length > Task.MAX_LENGTH) {
      throw new Error('Name length not within constraints');
    }

    this.#name = value;
  }

  /**
   * @readonly
   * @returns returns the private field "#dueDate"'s value, which is the task's due date
   */
  get dueDate() {
    return this.#dueDate;
  }

  /**
   * Takes the value parameter, validates it and sets it to the task's due date
   * @param {string} value the due date of the task
   */
  set dueDate(value) {
    if (!value || value === '') {
      throw new Error('You must select a date!');
    }
    if (value < new Date()) {
      throw new Error('Cannot select a date in the past!');
    }
    this.#dueDate = value;
  }

    /**
   * The status is set to 'Todo' in the constructor by default, in case there is no status passed
   * to the task at first
   * @readonly
   * @returns returns the private field "#status"'s value, which is the task's status
   */
  get status() {
    return this.#status;
  }
    /**
   * Takes the value parameter, validates it and sets it to the task's status
   * @param {string} value the status of the task
   */
  set status(value) {
    if ([status.TODO, status.DONE, status.IN_PROGRESS].indexOf(value) < 0) {
      throw new Error('Invalid status');
    }
    this.#status = value;
  }

  /** Sets the task status to TODO. */
  reset() {
    this.#status = status.TODO;
  }

  /** Sets the task status to IN_PROGRESS. */
  advance() {
    this.#status = status.IN_PROGRESS;
  }

  /** Sets the task status to DONE. */
  complete() {
    this.#status = status.DONE;
  }

  /** Transforms the task data into a formatted string */
  toString() {
    return `Name: ${this.#name}\n` +
      `Status: ${this.#status}\n` +
      `Due: ${this.#dueDate.toLocaleDateString()} ${this.#dueDate.toLocaleTimeString()}`;
  }
}
