/* eslint-disable no-undef */
import factory from '../../src/factory/factory';

describe('createTask', () => {

  describe('Baseline test', () => {

    it('should be a function', () => {

      expect(factory.createTask).toBeInstanceOf(Function);
  
    });

    it('should return an object', () => {
      const task = factory.createTask('test name', new Date(), 'In Progress');
  
      expect(task).toBeInstanceOf(Object);
    });

  });

  describe('Validation tests', () => {

    it('should throw when invalid name is passed', () => {

      expect(() => factory.createTask(undefined, new Date())).toThrow();
    });
  
    it('should throw when invalid date was passed', () => {
      expect(() => factory.createTask('test name', undefined)).toThrow();
    });
  
    it('should throw when invalid status is passed', () => {
  
      expect(() => factory.createTask('test name', new Date(), 'Invalid status')).toThrow();
    });

  });

  describe('Fields tests', () => {

    it('should set the name field correctly', () => {
      const task = factory.createTask('test name', new Date(), 'In Progress');
  
      expect(task.name).toBe('test name');
    });
  
    it('should set the due field correctly', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'In Progress');
  
      expect(task.due).toBeInstanceOf(Date);
      expect(task.due.toLocaleDateString()).toBe('9/3/2020');
    });

    it('should set the status field correctly', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'In Progress');
  
      expect(task.status).toBe('In Progress');
    });

    it('should set the status field to default value when no value was passed', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'));
  
      expect(task.status).toBe('Todo');
    });

  });

  describe('Method tests', () => {

    it('reset should be a function', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'In Progress');
  
      expect(task.reset).toBeInstanceOf(Function);

    });

    it('reset should change the status to "Todo"', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'In Progress');
      task.reset();
  
      expect(task.status).toBe('Todo');

    });

    it('advance should be a function', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'Todo');
  
      expect(task.advance).toBeInstanceOf(Function);

    });

    it('advance should change the status to "In Progress"', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'Todo');
      task.advance();
  
      expect(task.status).toBe('In Progress');

    });

    it('complete should be a function', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'Todo');
  
      expect(task.complete).toBeInstanceOf(Function);

    });

    it('complete should change the status to "Done"', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'Todo');
      task.complete();
  
      expect(task.status).toBe('Done');

    });

    it('toString should return the correct string', () => {
      const task = factory.createTask('test name', new Date('2020/09/03'), 'Done');
  
      expect(task.toString()).toBe('Name: test name\nStatus: Done\nDue: 9/3/2020 12:00:00 AM');
    });

  });

});
