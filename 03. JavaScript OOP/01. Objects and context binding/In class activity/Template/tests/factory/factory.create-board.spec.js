/* eslint-disable no-undef */
import factory from '../../src/factory/factory';

describe('createBoard', () => {

  describe('Baseline test', () => {

    it('should be a function', () => {

      expect(factory.createBoard).toBeInstanceOf(Function);
  
    });

    it('should return an object', () => {
      const board = factory.createBoard();
  
      expect(board).toBeInstanceOf(Object);
    });

  });

  describe('Field tests', () => {

    it('tasks should be an empty array when a board is initialized', () => {
      const board = factory.createBoard();

      expect(board.tasks).toBeInstanceOf(Array);
      expect(board.tasks.length).toBe(0);
    });

  });
  
  describe('Add', () => {

    it('should be a function', () => {
      const board = factory.createBoard();

      expect(board.add).toBeInstanceOf(Function);
    });

    it('should throw if invalid task was passed', () => {
      const board = factory.createBoard();

      expect(() => board.add(undefined)).toThrow();
    });

    it('should add valid tasks to the tasks array', () => {
      const board = factory.createBoard();
      const task = { name: 'task', due:  new Date(), status: 'Todo' };

      board.add(task);

      expect(board.tasks.length).toBe(1);
    });

    it('should throw if a duplicate task is being added', () => {
      const board = factory.createBoard();
      const task = { name: 'task', due:  new Date(), status: 'Todo' };

      board.add(task);

      expect(() => board.add(task)).toThrow();
    });

  });

  describe('Remove', () => {

    it('should be a function', () => {
      const board = factory.createBoard();

      expect(board.remove).toBeInstanceOf(Function);
    });

    it('should throw if invalid task was passed', () => {
      const board = factory.createBoard();

      expect(() => board.remove(undefined)).toThrow();
    });

    it('should remove the correct task', () => {
      const board = factory.createBoard();
      const task1 = { name: 'task', due:  new Date(), status: 'Todo' };
      const task2 = { name: 'task', due:  new Date(), status: 'Todo' };
      board.tasks = [task1, task2];

      board.remove(task1);

      expect(board.tasks.length).toBe(1);
      expect(board.tasks).toContain(task2);
      expect(board.tasks).not.toContain(task1);
    });

  });

  describe('toString', () => {

    it('should return the correct string when there are no tasks', () => {
      const board = factory.createBoard();

      expect(board.toString()).toContain('No tasks at the moment.');
    });

    it('should return the correct string when there are one or more tasks', () => {
      const board = factory.createBoard();
      const task1 = {
        toString: function() {
          return 'task1';
        },
      };
      const task2 = {
        toString: function() {
          return 'task2';
        },
      };
      board.tasks = [task1, task2];

      expect(board.toString()).toContain('---Task Board---');
      expect(board.toString()).toContain('task1');
      expect(board.toString()).toContain('task2');
    });

  });

});