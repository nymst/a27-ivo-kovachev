// Factory functions in JavaScript are functions that may have parameters and return an object
// https://medium.com/javascript-scene/javascript-factory-functions-with-es6-4d224591a8b1/
const currentDate = new Date().toLocaleString();
const MIN_NAME_LENGTH = 3;
const MAX_NAME_LENGTH = 120;

const createTask = (name, due, status) => {
  // validate the name
  if(!name){
    throw new Error('Cannot be null or undefined');
  }

  if(name === ''){
    throw new Error('Cannot be an empty string');
  }

  if(name.length < MIN_NAME_LENGTH || name.length > MAX_NAME_LENGTH){
    throw new Error('Invalid name length');
  }

  // validate due
  if(due < currentDate){
    throw new Error('Invalid date');
  }

  // validate the status
  if(!status || status === ''){
    status = 'Todo';
  }

  if(status !== 'Todo' && status !=='In Progress' && status !== 'Done'){
    throw new Error('Invalid status');
  }

  return {
    name,
    status,
    due:due.toLocaleString(),
    reset: function() {
      this.status = 'Todo';
    },
    advance: function() {
      this.status = 'In Progress';
    },
    complete: function() {
      this.status = 'Done';
    },
    toString: function() {
      return `Name: ${name}
Status: ${status}
Due: ${due}`;
    },
  };
};

const createBoard = () => {
  
    return {
      tasks: [],
      add: function(task) {
        if()
        this.tasks.push(task);
      },
      remove: function(task) {
        this.tasks.pop(task);
      },
      toString: function() {
        const taskBoardText = '---Task Board---\n';
        const tasksText = 'Tasks:\n';

        if (this.tasks.length < 1) {
          return `${taskBoardText}\n${tasksText}No tasks at the moment.`;
        } else {
          return `${taskBoardText}\n${tasksText}\n${this.tasks.join('\n-----\n').toString()}`;
        }
      },
    };
};

export default {
  createTask,
  createBoard,
};
