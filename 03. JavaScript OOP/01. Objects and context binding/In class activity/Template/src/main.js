import factory from './factory/factory.js';

const newline = () => console.log('\n \x1b[35m* * * * *\x1b[37m \n');

const board = factory.createBoard();

const task1 = factory.createTask('Validate fields', new Date('2020/09/03'));
const task2 = factory.createTask('Write unit tests', new Date('2020/09/04'));
const task3 = factory.createTask('Remove console.log', new Date('2020/09/05'));
console.log(board.toString());

newline();

board.add(task1);
board.add(task2);
board.add(task3);

task1.advance();
task2.complete();

console.log(board.toString());

newline();

board.remove(task3);

console.log(task1.toString());

