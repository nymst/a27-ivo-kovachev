import { BaseModel } from './base-model.js';
import * as validators from '../common/validators.js';

export class Sprinter extends BaseModel {
  #personalRecords;

  constructor(firstname,lastname,country, personalRecords) {
    super(firstname,lastname, country);

    validators.instanceOf(personalRecords, Map);

    this.#personalRecords = personalRecords;
  }


  get personalRecords() {
    return this.#personalRecords;
  }

  additionalInfo() {
    let records = '';

    this.personalRecords.forEach((value, key) => {
      records += `${key}m: ${value}\n`;
    });

    return `PERSONAL RECORDS\n${records}`;
  }

  print() {
    return `SPRINTER: ${super.print()}\n` + this.additionalInfo();
  }
}