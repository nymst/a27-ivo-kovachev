import { boxingCategory } from '../common/boxing-category.enum.js';
import { BaseModel } from './base-model.js';
import * as validators from '../common/validators.js';


export class Boxer extends BaseModel {
  #category;
  #wins;
  #losses;

  static MIN_WINS_LOSSES_AMOUNT = 0;
  static MAX_WINS_LOSSES_AMOUNT = 100;
  constructor(firstname, lastname, country, category, wins, losses) {

    Boxer.validateCategory(category);
    Boxer.validateWins(wins);
    Boxer.validateLosses(losses);

    super(firstname, lastname, country);

    this.#category = category;
    this.#wins = wins;
    this.#losses = losses;
  }

  get category() {
    return this.#category;
  }

  get wins() {
    return this.#wins;
  }

  get losses() {
    return this.#losses;
  }

  static validateCategory(value) {
    validators.inArray(value, Object.keys(boxingCategory));
  }

  static validateWins(value) {
    validators.notEmpty(value, 'wins');
    validators.typeOf(value, 'number');
    validators.minValue(value, Boxer.MIN_WINS_LOSSES_AMOUNT);
    validators.maxValue(value, Boxer.MAX_WINS_LOSSES_AMOUNT);
  }

  static validateLosses(value) {
    validators.notEmpty(value, 'losses');
    validators.typeOf(value, 'number');
    validators.minValue(value, Boxer.MIN_WINS_LOSSES_AMOUNT);
    validators.maxValue(value, Boxer.MAX_WINS_LOSSES_AMOUNT);
  }

  additionalInfo() {
    return `Category: ${this.category}\n` +
      `Wins: ${this.wins}\n` +
      `Losses: ${this.losses}\n`;
  }
  print() {
    return `BOXER: ${super.print()}\n` + this.additionalInfo();
  }
}