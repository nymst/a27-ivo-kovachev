import * as validators from '../common/validators.js';

export class BaseModel {
  #firstname;
  #lastname;
  #country;

  static MIN_NAME_LENGTH = 2;
  static MAX_NAME_LENGTH = 20;
  static MIN_COUNTRY_LENGTH = 2;
  static MAX_COUNTRY_LENGTH = 20;

  constructor(firstname, lastname, country) {
    BaseModel.validateFirstName(firstname);
    BaseModel.validateLastName(lastname);
    BaseModel.validateCountry(country);
    
    this.#firstname = firstname;
    this.#lastname = lastname;
    this.#country = country;
  }

  get firstname() {
    return this.#firstname;
  }

  get lastname() {
    return this.#lastname;
  }

  get country() {
    return this.#country;
  }

  static validateFirstName(value) {
    validators.notEmpty(value, 'firstname');
    validators.typeOf(value, 'string');
    validators.lengthCheck(value, BaseModel.MIN_NAME_LENGTH, BaseModel.MAX_NAME_LENGTH);
  }

  static validateLastName(value) {
    validators.notEmpty(value, 'lastname');
    validators.typeOf(value, 'string');
    validators.lengthCheck(value, BaseModel.MIN_NAME_LENGTH, BaseModel.MAX_NAME_LENGTH);
  }

  static validateCountry(value) {
    validators.notEmpty(value, 'country');
    validators.typeOf(value, 'string');
    validators.lengthCheck(value, BaseModel.MIN_COUNTRY_LENGTH, BaseModel.MAX_COUNTRY_LENGTH);
  }

  print() {
    return `${this.firstname} ${this.lastname} from ${this.country}`;
  }
}