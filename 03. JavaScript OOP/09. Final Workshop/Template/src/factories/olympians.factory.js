import { Boxer } from '../models/boxer.model.js';
import { Sprinter } from '../models/sprinter.model.js';

export class OlympiansFactory {

  static createBoxer(firstname, lastname, country, category, wins, losses) {
    return new Boxer(firstname, lastname, country, category, wins, losses);
  }

  static createSprinter(firstname, lastname, country, personalRecord) {
    return new Sprinter(firstname, lastname, country, personalRecord);
  }

}
