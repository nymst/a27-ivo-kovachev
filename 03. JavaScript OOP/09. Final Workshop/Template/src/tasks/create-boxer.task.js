import { OlympiansFactory } from '../factories/olympians.factory.js';
import { OlympicsCommittee } from '../providers/olympics-committee.js';
import { Task } from './base.task.js';

export class CreateBoxerTask extends Task {

  run(firstname, lastname, country, category, wins, losses) {
    const boxer = OlympiansFactory.createBoxer(firstname, lastname, country, category, wins, losses);
    OlympicsCommittee.olympians.push(boxer);

    return 'Boxer created';
  }

}
