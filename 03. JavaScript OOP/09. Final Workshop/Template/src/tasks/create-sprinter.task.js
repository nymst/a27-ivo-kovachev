import { OlympiansFactory } from '../factories/olympians.factory.js';
import { OlympicsCommittee } from '../providers/olympics-committee.js';
import { Task } from './base.task.js';
export class CreateSprinterTask extends Task {

  run(firstname, lastname, country, personalRecords) {
    const sprinter = OlympiansFactory.createSprinter(firstname, lastname, country, personalRecords);
    OlympicsCommittee.olympians.push(sprinter);

    return 'Sprinter created';
  }
}
