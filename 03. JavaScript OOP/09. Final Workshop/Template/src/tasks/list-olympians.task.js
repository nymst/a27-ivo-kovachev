import { OlympicsCommittee } from '../providers/olympics-committee.js';
import { Task } from './base.task.js';

export class ListOlympiansTask extends Task {

  #defaultKey = 'firstname';
  #defaultOrder = 'asc';
  #orderDirection = {
    asc: 1,
    desc: -1,
  };

  run(key = this.#defaultKey, order = this.#defaultOrder) {
    if (OlympicsCommittee.olympians.length === 0) {
      return 'No olympians added!';
    }

    let output = `Sorted by [key: ${key}] in [order: ${order}]\n`;

    order = this.#orderDirection[order];

    OlympicsCommittee.olympians.sort((a, b) => {
      let fieldA = a[key].toUpperCase();
      let fieldB = b[key].toUpperCase();

      if (fieldA < fieldB) {
        return -order;
      }

      if (fieldA > fieldB) {
        return order;
      }

      return 0;
    });

    OlympicsCommittee.olympians.forEach((olympian) => {
      output += olympian.print();
    });


    return output;
  }

}
