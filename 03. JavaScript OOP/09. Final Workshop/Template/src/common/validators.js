
const notEmpty = (value, label) => {
  if (value === undefined || value === null || value === '') {
    throw new Error(`The ${label} cannot be empty`);
  }
};

const typeOf = (value, type) => {
  if (typeof value !== type) {
    throw new Error('Wrong input type');
  }
};

const lengthCheck = (value, minLength, maxLength) => {
  if (value.length < minLength || value.length > maxLength) {
    throw new Error('Length not within the constraints');
  }
};

const minValue = (value, minValue) => {
  if (value < minValue) {
    throw new Error('Value cannot be a negative number');
  }
};

const maxValue = (value, maxValue) => {
  if (value > maxValue) {
    throw new Error('Value not within constraints');
  }
};

const inArray = (value, array) => {
  if (array.indexOf(value) === -1) {
    throw new Error('Does not exist in the array');
  }
};

const instanceOf = (value, instance) => {
  if (!(value instanceof instance)) {
    throw new Error(`Not instance of ${instance}`);
  }
};

export { notEmpty, typeOf, lengthCheck, minValue, maxValue, inArray, instanceOf };