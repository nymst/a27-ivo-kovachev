<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Cosmetics Factory

### 1. Description

Cosmetics factory is s scalable system for managing an inventory of cosmetic products. It is build with classes and following the principles of **OOP Design**.

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **ESLint**, **Jest**

<br>

### 3. Goals

The **goal** is to fully implement several related classes (called *models*) which have partial or no implementation. To help you cover the business requirements of the system you are provided with unit tests.

- Working with classes - writing classes and creating instances of those classes.
- Modeling classes using `fields` (state) and `methods` (behavior).
- Changing the state through getters and setters (remember having both getter and a setter allows to read an write to the property, having only a getter makes the property **readonly** and having only a setter makes the property **writeonly**)
- Protecting the state by encapsulation
- Inheriting a base class to reuse its logic
- Lifting repeating logic from similar classes down to the base class

<br>

### 4. Setup

To get the project up and running, follow these steps:

1. Go inside the `Template` folder.
1. Run `npm install` to restore all dependencies.
1. After that, there are a few scripts you can run:

   - `npm run start` (shorthand: `npm start`) - Runs the `src/main.js` file.
   - `npm run lint` - Lints the code inside the `src` folder using **ESLint**.
   - `npm run test` (shorthand: `npm test` or `npm t`) - Runs the unit tests in the console.
   - `npm run test:browser` - Runs the unit tests in the browser in interactive mode.

<br>

### 5. Project structure

All of the code you will be working with is in the `src` folder. The entry point (the file from which the program is started) is `src/main.js`. There are several folders with different roles:

- `common` - holds shared resources like enums, constants, etc.
- `engine` - contains the system's engine class which acts as a manager for all other classes - factories and models. It is responsible for putting all of the logic together - initializing the **cosmetics factory** and taking care of all model interactions like adding and removing products from categories, etc.
- `factory` - contains the **cosmetics factory** which is responsible for creating instances of models
- `models` - holds all cosmetic models - the **shopping card**, the **cosmetics category** and all the **product** models

You will work exclusively in the `models` folder and implement the following models - `CosmeticsCategory`, `Product`, `Shampoo`, and `Toothpaste`.

```bash
npm test
```

or if you want to run the tests in interactive mode inside the browser you can type

```bash
npm run test:browser
```

Unit tests will report your progress with the implementation and show you what the expected result of running a single piece of code and how it is different from your implementation. Some of the tests might look as they are passing, but in fact they can show *false positive* when the test expectation is the code to throw if invalid data is passed, but in reality the code throw because there are problems with the implementations.

Use the tests wisely.

**Do not concern yourself with checking if a passed value is an instance of a class.**

<br>

### 6. The `CosmeticsCategory` model

The `CosmeticsCategory` model is already partially implemented, you need to add the missing implementation to the following methods:

- `addProduct(product)` - should throw if the product is already added to `#products` and it should throw if the passed value is not valid. Otherwise it should add the product to the array
- `removeProduct(product)` - should throw if the passed value is not valid. It should try to find if there is a product with the same **name** property in the products array and throw if there is not (product not found). Otherwise it should remove the product from the array.

<br>

### 7. The `Product` model

The `Product` model is missing any implementation. You will also notice there are two other models in the same folder - `Shampoo` and `Toothpaste`. `Shampoo` has partial implementation and `Toothpaste` has none. Think of a way to use the `Product` class. The name of the model files is a big clue.

Pay attention to repeating logic in the application. 

<br>

### 8. The `Shampoo` model

The `Shampoo` model is also partially implemented. Properties are already encapsulated, however there are a few more requirements which need to be covered:

Properties:

- `name` - should be a **readonly** string with length in the range `[3 - 10]`
- `brand` - should be a **readonly** string with length in the range `[2 - 10]`
- `price` - should be a **readonly** number in the range `[0 - Infinity]`
- `gender` - should a valid **readonly** value of the `genderType` enum
- `milliliters` - should be a valid **readonly** number in the range `[0 - Infinity]`
- `usage` - should a valid **readonly** value of the `usageType` enum

All values are passed to the `Shampoo` constructor in the order provided.

Constructor should throw if any of the values passed is invalid.

Methods:

- `print()` - should return a string with product information, i.e. a shampoo with the name `MyWoman` and brand `Abc` (rest of the properties as read from below) should produce the following:

```txt
#MyWoman Abc
 #Price: $20.99
 #Gender: Women
 #Milliliters: 1000
 #Usage: EveryDay
```

<br>

### 9. The `Toothpaste` model

The `Toothpaste` has no implementation and it's practically identical to the `Shampoo` model with a few tweaks:

Properties:

- `name` - should be a **readonly** string with length in the range `[3 - 10]`
- `brand` - should be a **readonly** string with length in the range `[2 - 10]`
- `price` - should be a **readonly** number in the range `[0 - Infinity]`
- `gender` - should a valid **readonly** value of the `genderType` enum
- `ingredients` - should be a non-empty **readonly** string

All values are passed to the `Toothpaste` constructor in the order provided.

Constructor should throw if any of the values passed is invalid.

Methods:

- `print()` - should return a string with product information, i.e. a toothpaste with the name `White` and brand `Colgate` (rest of the properties as read from below) should produce the following:

```txt
#White Colgate
 #Price: $10.99
 #Gender: Men
 #Ingredients: calcium,fluoride
```

<br>

### 10. Testing the cosmetics system

The testing code is already in the `main.js` file. You can comment in and out parts of the code to test one feature or another. Running this code should produce the output shown below (do not be concerned too much with indentation):

```js
import { CosmeticsEngine } from './engine/engine.js';
import { genderType } from './common/gender-type.enum.js';
import { usageType } from './common/usage.type.enum.js';

const main = () => {
  try {
    const engine = CosmeticsEngine;
    const reports = [
      engine.createShampoo('MyMan', 'Nivea', 10.99, genderType.Men, 1000, usageType.EveryDay),
      engine.createShampoo('MyGeneric', 'Abc', 10.99, genderType.Men, 1000, usageType.EveryDay),
      engine.createShampoo('MyWoman', 'Abc', 20.99, genderType.Women, 1000, usageType.EveryDay),
      engine.createToothpaste('White', 'Colgate', 10.99, genderType.Men, ['calcium', 'fluoride'].join()),
      engine.createCategory('Shampoos'),
      engine.createCategory('Toothpastes'),
      engine.addToCategory('Shampoos', 'MyMan'),
      engine.addToCategory('Shampoos', 'MyGeneric'),
      engine.addToCategory('Shampoos', 'MyWoman'),
      engine.addToCategory('Toothpastes', 'White'),
      engine.addToShoppingCart('MyMan'),
      engine.addToShoppingCart('White'),
      engine.showCategory('Shampoos'),
      engine.showCategory('Toothpastes'),
      engine.getTotalPrice(),
      engine.removeFromCategory('Shampoos', 'MyMan'),
      engine.showCategory('Shampoos'),
      engine.removeFromShoppingCart('MyMan'),
      engine.getTotalPrice(),
    ];

    console.log(reports.join('\n'));
  } catch (error) {
    console.log(error);
  }
};

main();
```

Sample output text:

```text
Shampoo with name MyMan was created!
Shampoo with name MyGeneric was created!     
Shampoo with name MyWoman was created!       
Toothpaste with name White was created!      
Category with name Shampoos was created!     
Category with name Toothpastes was created!  
Product MyMan added to category Shampoos!    
Product MyGeneric added to category Shampoos!
Product MyWoman added to category Shampoos!  
Product White added to category Toothpastes! 
Product MyMan was added to the shopping cart!
Product White was added to the shopping cart!
#Category: Shampoos
#MyWoman Abc
 #Price: $20.99
 #Gender: Women
 #Milliliters: 1000
 #Usage: EveryDay
 ===
#MyGeneric Abc
 #Price: $10.99
 #Gender: Men
 #Milliliters: 1000
 #Usage: EveryDay
 ===
#MyMan Nivea
 #Price: $10.99
 #Gender: Men
 #Milliliters: 1000
 #Usage: EveryDay
 ===
#Category: Toothpastes
#White Colgate
 #Price: $10.99
 #Gender: Men
 #Ingredients: calcium,fluoride
 ===
$21.98 total price currently in the shopping cart!
Product MyMan removed from category Shampoos!
#Category: Shampoos
#MyWoman Abc
 #Price: $20.99
 #Gender: Women
 #Milliliters: 1000
 #Usage: EveryDay
 ===
#MyGeneric Abc
 #Price: $10.99
 #Gender: Men
 #Milliliters: 1000
 #Usage: EveryDay
 ===
Product MyMan was removed from the shopping cart!
$10.99 total price currently in the shopping cart!
```