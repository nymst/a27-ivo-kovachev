/* eslint-disable no-undef */
import { Product } from '../../src/models/products/product.model';
import { Toothpaste } from '../../src/models/products/toothpaste-product.model';
import { genderType } from '../../src/common/gender-type.enum';

describe('Toothpaste', () => {

  describe('class', () => {

    it('should extend Product', () => {
      expect(Toothpaste.prototype).toBeInstanceOf(Product);
    });

  });

  describe('ingredients', () => {
  
    it('constructor should throw if no ingredients was passed or the passed value is empty string', () => {
      expect(() => new Toothpaste('Test name', 'Test brand', 10, genderType.Men, undefined)).toThrow();
      expect(() => new Toothpaste('Test name', 'Test brand', 10, genderType.Men, '')).toThrow();
    });

    it('constructor should set ingredients with the correct value', () => {
      const toothpaste = new Toothpaste('Test name', 'Test brand', 10, genderType.Unisex, 'sodium');

      expect(toothpaste.ingredients).toBe('sodium');
    });

    it('should not be able to set the value of ingredients from outside the class', () => {
      const toothpaste = new Toothpaste('Test name', 'Test brand', 10, genderType.Unisex, 'sodium');

      expect(() => toothpaste.ingredients = 'Xenon').toThrow();
    });
    
  });

  describe('additionalInfo', () => {

    it('should override Product\'s additionalInfo method', () => {
      expect(Toothpaste.prototype.additionalInfo).toBeInstanceOf(Function);
    });

    it('should return the correct string', () => {
      const toothpaste = new Toothpaste('Test name', 'Test brand', 10, genderType.Unisex, 'Carbon');

      const result = toothpaste.additionalInfo();
      
      expect(result).toContain('Ingredients: Carbon');
    });

  });

});