import { CosmeticsCategory } from '../../src/models/category/cosmetics-category.model';

/* eslint-disable no-undef */
describe('CosmeticsCategory', () => {

  describe('addProduct', () => {

    it('should throw if no product or invalid value was passed', () => {
      const category = new CosmeticsCategory('test');

      expect(() => category.addProduct(null)).toThrow();
      expect(() => category.addProduct(undefined)).toThrow();
    });

    it('should add the product to #products', () => {
      const category = new CosmeticsCategory('test');
      const product = { name: 'test-product' };

      category.addProduct(product);

      expect(category.products).toEqual([product]);
    });

  });

  describe('removeProduct', () => {

    it('should throw if no product or invalid value was passed', () => {
      const category = new CosmeticsCategory('test');

      expect(() => category.removeProduct(null)).toThrow();
      expect(() => category.removeProduct(undefined)).toThrow();
    });

    it('should throw if the product passed is not in the products array', () => {
      const category = new CosmeticsCategory('test');
      const product = { name: 'test-product' };

      category.mockProductsData = [product];

      expect(() => category.removeProduct({ name: 'another-product' })).toThrow();
    });

    it('should correctly remove the product by name', () => {
      const category = new CosmeticsCategory('test');
      const product = { name: 'test-product' };

      category.mockProductsData = [product];
      category.removeProduct({ name: 'test-product' });

      expect(category.products).toEqual([]);
    });

  });

});
