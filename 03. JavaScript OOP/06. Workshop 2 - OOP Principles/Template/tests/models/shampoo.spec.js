/* eslint-disable no-undef */
import { Shampoo } from '../../src/models/products/shampoo-product.model';
import { Product } from '../../src/models/products/product.model';
import { usageType } from '../../src/common/usage.type.enum';
import { genderType } from '../../src/common/gender-type.enum';

describe('Shampoo', () => {

  describe('class', () => {

    it('should extend Product', () => {
      expect(Shampoo.prototype).toBeInstanceOf(Product);
    });

  });

  describe('milliliters', () => {

    it('should throw if the value of milliliters is not valid', () => {
      expect(() => new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, -1, usageType.EveryDay)).toThrow();
    });

    it('should set milliliters to the correct value', () => {
      const shampoo = new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, 20, usageType.EveryDay);

      expect(shampoo.milliliters).toBe(20);
    });

    it('should not be able to set the value of milliliters from outside the class', () => {
      const shampoo = new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, 20, usageType.EveryDay);

      expect(() => shampoo.milliliters = 30).toThrow();
    });

  });


  describe('usage', () => {

    it('should throw if the value of usage is not valid', () => {
      expect(() => new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, 20, 'InvalidKeyValue')).toThrow();
    });

    it('should set usage to the correct value', () => {
      const shampoo = new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, 20, usageType.EveryDay);

      expect(shampoo.usage).toBe(usageType.EveryDay);
    });

    it('should not be able to set the value of usage from outside the class', () => {
      const shampoo = new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, 20, usageType.EveryDay);

      expect(() => shampoo.usage = usageType.Medical).toThrow();
    });

  });

  describe('additionalInfo', () => {

    it('should override Product\'s additionalInfo method', () => {
      expect(Shampoo.prototype.additionalInfo).toBeInstanceOf(Function);
    });

    it('should return the correct string', () => {
      const shampoo = new Shampoo('Test name', 'Test brand', 10, genderType.Unisex, 20, usageType.EveryDay);

      const result = shampoo.additionalInfo();
      
      expect(result).toContain('Milliliters: 20');
      expect(result).toContain('Usage: EveryDay');
    });

  });

});