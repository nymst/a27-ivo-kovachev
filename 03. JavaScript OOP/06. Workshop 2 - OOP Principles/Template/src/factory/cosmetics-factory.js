import { CosmeticsCategory } from '../models/category/cosmetics-category.model.js';
import { ShoppingCart } from '../models/cart/shopping-cart.model.js';
import { Shampoo } from '../models/products/shampoo-product.model.js';
import { Toothpaste } from '../models/products/toothpaste-product.model.js';

export class CosmeticsFactory {
  createCategory(name) {
    return new CosmeticsCategory(name);
  }

  createShampoo(name, brand, price, gender, milliliters, usage) {
    return new Shampoo(name, brand, price, gender, milliliters, usage);
  }

  createToothpaste(name, brand, price, gender, ingredients) {
    return new Toothpaste(name, brand, price, gender, ingredients);
  }

  createShoppingCart() {
    return new ShoppingCart();
  }
}