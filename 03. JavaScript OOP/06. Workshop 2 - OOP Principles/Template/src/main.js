import { CosmeticsEngine } from './engine/engine.js';
import { genderType } from './common/gender-type.enum.js';
import { usageType } from './common/usage.type.enum.js';

const main = () => {
  try {
    const engine = CosmeticsEngine;
    const reports = [
      engine.createShampoo('MyMan', 'Nivea', 10.99, genderType.Men, 1000, usageType.EveryDay),
      engine.createShampoo('MyGeneric', 'Abc', 10.99, genderType.Men, 1000, usageType.EveryDay),
      engine.createShampoo('MyWoman', 'Abc', 20.99, genderType.Women, 1000, usageType.EveryDay),
      engine.createToothpaste('White', 'Colgate', 10.99, genderType.Men, ['calcium', 'fluoride'].join()),
      engine.createCategory('Shampoos'),
      engine.createCategory('Toothpastes'),
      engine.addToCategory('Shampoos', 'MyMan'),
      engine.addToCategory('Shampoos', 'MyGeneric'),
      engine.addToCategory('Shampoos', 'MyWoman'),
      engine.addToCategory('Toothpastes', 'White'),
      engine.addToShoppingCart('MyMan'),
      engine.addToShoppingCart('White'),
      engine.showCategory('Shampoos'),
      engine.showCategory('Toothpastes'),
      engine.getTotalPrice(),
      engine.removeFromCategory('Shampoos', 'MyMan'),
      engine.showCategory('Shampoos'),
      engine.removeFromShoppingCart('MyMan'),
      engine.getTotalPrice(),
    ];

    console.log(reports.join('\n'));
  } catch (error) {
    console.log(error);
  }
};

main();