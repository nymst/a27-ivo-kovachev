import { ShoppingCart } from '../models/cart/shopping-cart.model.js';
import { CosmeticsFactory } from '../factory/cosmetics-factory.js';

export class CosmeticsEngine {

  static #shoppingCart = new ShoppingCart();

  static #factory = new CosmeticsFactory();

  static #categories = new Map();

  static #products = new Map();

  static removeFromShoppingCart(productName) {
    if (!CosmeticsEngine.#products.has(productName)) {
      return `Product ${productName} does not exist!`;
    }

    const product = CosmeticsEngine.#products.get(productName);

    if (!CosmeticsEngine.#shoppingCart.containsProduct(product)) {
      return `Shopping cart does not contain product with name ${productName}!`;
    }

    CosmeticsEngine.#shoppingCart.removeProduct(product);

    return `Product ${productName} was removed from the shopping cart!`;
  }

  static addToShoppingCart(productName) {
    if (!CosmeticsEngine.#products.has(productName)) {
      return `Product ${productName} does not exist!`;
    }

    const product = CosmeticsEngine.#products.get(productName);
    CosmeticsEngine.#shoppingCart.addProduct(product);

    return `Product ${productName} was added to the shopping cart!`;
  }

  static createShampoo(name, brand, price, gender, milliliters, usage) {
    if (CosmeticsEngine.#products.has(name)) {
      return `Shampoo with name ${name} already exists!`;
    }
    const shampoo = CosmeticsEngine.#factory.createShampoo(name, brand, price, gender, milliliters, usage);
    CosmeticsEngine.#products.set(name, shampoo);

    return `Shampoo with name ${name} was created!`;
  }

  static createToothpaste(name, brand, price, gender, ingredients) {
    if (CosmeticsEngine.#products.has(name)) {
      return `Toothpaste with name ${name} already exists!`;
    }

    const toothpaste = CosmeticsEngine.#factory.createToothpaste(name, brand, price, gender, ingredients);
    CosmeticsEngine.#products.set(name, toothpaste);

    return `Toothpaste with name ${name} was created!`;
  }

  static showCategory(categoryName) {
    if (!CosmeticsEngine.#categories.has(categoryName)) {
      return `Category ${categoryName} does not exist!`;
    }

    const category = CosmeticsEngine.#categories.get(categoryName);

    return category.print();
  }

  static removeFromCategory(categoryNameToRemoveFrom, productToRemove) {
    if (!CosmeticsEngine.#categories.has(categoryNameToRemoveFrom)) {
      return `Category ${categoryNameToRemoveFrom} does not exist!`;
    }

    if (!CosmeticsEngine.#products.has(productToRemove)) {
      return `Product ${productToRemove} does not exist!`;
    }

    const category = CosmeticsEngine.#categories.get(categoryNameToRemoveFrom);
    const product = CosmeticsEngine.#products.get(productToRemove);

    category.removeProduct(product);

    return `Product ${productToRemove} removed from category ${categoryNameToRemoveFrom}!`;
  }

  static addToCategory(categoryNameToAdd, productToAdd) {
    if (!CosmeticsEngine.#categories.has(categoryNameToAdd)) {
      return `Category ${categoryNameToAdd} does not exist!`;
    }

    if (!CosmeticsEngine.#products.has(productToAdd)) {
      return `Product ${productToAdd} does not exist!`;
    }

    const category = CosmeticsEngine.#categories.get(categoryNameToAdd);
    const product = CosmeticsEngine.#products.get(productToAdd);

    category.addProduct(product);

    return `Product ${productToAdd} added to category ${categoryNameToAdd}!`;
  }

  static createCategory(categoryName) {
    if (CosmeticsEngine.#categories.has(categoryName)) {
      return `Category with name ${categoryName} already exists!`;
    }

    const category = CosmeticsEngine.#factory.createCategory(categoryName);
    CosmeticsEngine.#categories.set(categoryName, category);

    return `Category with name ${categoryName} was created!`;
  }

  static getTotalPrice() {
    return CosmeticsEngine.#shoppingCart.productList.length > 0 ?
      `$${CosmeticsEngine.#shoppingCart.totalPrice()} total price currently in the shopping cart!` :
      'No product in shopping cart!';
  }
}