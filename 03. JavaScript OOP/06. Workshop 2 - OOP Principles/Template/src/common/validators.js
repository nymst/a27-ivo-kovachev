
const notEmpty = (value, label) => {
  if (!value || value === '') {
    throw new Error(`The ${label} cannot be empty`);
  }
};

const typeOf = (value, type) => {
  if (typeof value !== type) {
    throw new Error('Wrong input type');
  }
};

const lengthCheck = (value, minLength, maxLength) => {
  if (value.length < minLength || value.length > maxLength) {
    throw new Error('Length not within the constraints');
  }
};

const minValue = (value, minValue) => {
  if (value < minValue) {
    throw new Error('Value cannot be a negative number');
  }
};

const inArray = (value, array) => {
  if (array.indexOf(value) === -1) {
    throw new Error('Does not exist in the array');
  }
};

export { notEmpty, typeOf, lengthCheck, minValue, inArray };