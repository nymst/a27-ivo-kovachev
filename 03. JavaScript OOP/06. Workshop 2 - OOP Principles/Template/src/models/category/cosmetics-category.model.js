export class CosmeticsCategory {
  static #minNameLength = 2;
  static #maxNameLength = 15;
  #name;
  #products;

  constructor(name) {
    if (!name || name.length < CosmeticsCategory.#minNameLength || name.length > CosmeticsCategory.#maxNameLength) {
      throw new Error(`Product name length must be between ${CosmeticsCategory.#minNameLength} and ${CosmeticsCategory.#maxNameLength}`);
    }

    this.#name = name;
    this.#products = [];
  }

  get products() {
    return this.#products.slice();
  }

  get name() {
    return this.#name;
  }

  addProduct(product) {
    const productIndex = this.#products.findIndex(existingProduct => existingProduct === product);

    if (!product || product === '') {
      throw new Error('Invalid product');
    }

    if (productIndex >= 0) {
      throw new Error('This product already exists');
    }

    this.#products.push(product);
  }

  removeProduct(product) {
    const productIndex = this.#products.findIndex(existingProduct => existingProduct.name === product.name);

    if (productIndex < 0) {
      throw new Error('This product does not exist');
    }

    this.#products = this.#products.filter((item) => item.name !== product.name);
  }

  print() {
    if (this.#products.length === 0) {
      return `#Category: ${this.#name}\r\n #No product in this category`;
    }

    let result = '';
    result += `#Category: ${this.#name}\n`;

    this.products
      .sort((a, b) =>
        a.brand === b.brand ?
          (b.price - a.price) :
          a.brand.localeCompare(b.brand))
      .forEach(p => {
        result += `${p.print()}\n`;
      });

    return result.trim();
  }

  // Do not remove this. It is used for the tests.
  set mockProductsData(value) {
    this.#products = value;
  }
}
