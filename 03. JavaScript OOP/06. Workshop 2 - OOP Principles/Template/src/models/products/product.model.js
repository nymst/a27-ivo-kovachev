import { genderType } from '../../common/gender-type.enum.js';
import * as validators from '../../common/validators.js';

export class Product {
  #name;
  #brand;
  #price;
  #gender;

  static MIN_NAME_LENGTH = 3;
  static MAX_NAME_LENGTH = 10;
  static MIN_BRAND_LENGTH = 2;
  static MAX_BRAND_LENGTH = 10;
  static MIN_PRICE = 0;

  constructor(name, brand, price, gender) {
    Product.validateName(name);
    Product.validateBrand(brand);
    Product.validatePrice(price);
    Product.validateGender(gender);
    this.#name = name;
    this.#brand = brand;
    this.#price = price;
    this.#gender = gender;
  }

  get name() {
    return this.#name;
  }
  get brand() {
    return this.#brand;
  }

  get price() {
    return this.#price;
  }

  get gender() {
    return this.#gender;
  }

  static validateName(value) {
    validators.notEmpty(value, 'name');
    validators.typeOf(value, 'string');
    validators.lengthCheck(value, Product.MIN_NAME_LENGTH, Product.MAX_NAME_LENGTH);
  }

  static validateBrand(value) {
    validators.notEmpty(value, 'brand');
    validators.typeOf(value, 'string');
    validators.lengthCheck(value, Product.MIN_BRAND_LENGTH, Product.MAX_BRAND_LENGTH);
  }

  static validatePrice(value) {
    validators.notEmpty(value, 'price');
    validators.typeOf(value, 'number');
    validators.minValue(value, Product.MIN_PRICE);
  }

  static validateGender(value) {
    validators.inArray(value, Object.keys(genderType));
  }
  
  print() {
    return `#${this.name} ${this.brand}\n`+
            `#Price: $${this.price}\n`+
            `#Gender: ${this.gender}\n`;
  }
}