import { Product } from './product.model.js';
import * as validators from '../../common/validators.js';

export class Toothpaste extends Product{

  #ingredients;

  constructor(name, brand, price, gender, ingredients) {
    super(name, brand, price, gender);
    Toothpaste.validateIngredients(ingredients);
    this.#ingredients = ingredients;
  }

  get ingredients() {
    return this.#ingredients;
  }

  static validateIngredients(value) {
    validators.typeOf(value, 'string');
    validators.notEmpty(value);
  }

  print() {
    return super.print() + `${this.additionalInfo()}\r\n ===`;
  }

  additionalInfo() {
    return `#Ingredients: ${this.ingredients}`;
  }
}
 