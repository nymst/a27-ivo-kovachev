import { Product } from './product.model.js';
import * as validators from '../../common/validators.js';
import { usageType } from '../../common/usage.type.enum.js';


export class Shampoo extends Product {

  #milliliters;
  #usage;

  static MIN_MILLILITERS_VALUE = 0;
  constructor(name, brand, price, gender, milliliters, usage) {
    super(name, brand, price, gender);
    Shampoo.validateMilliliters(milliliters);
    Shampoo.validateUsage(usage);
    this.#milliliters = milliliters;
    this.#usage = usage;
  }

  get milliliters() {
    return this.#milliliters;
  }

  get usage() {
    return this.#usage;
  }

  static validateMilliliters(value) {
    validators.notEmpty(value, 'milliliters');
    validators.typeOf(value, 'number');
    validators.minValue(value, Shampoo.MIN_MILLILITERS_VALUE);
  }

  static validateUsage(value) {
    validators.inArray(value, Object.keys(usageType));
  }

  print() {
    return super.print() + `${this.additionalInfo()}\r\n ===`;
  }

  additionalInfo() {
    return `#Milliliters: ${this.milliliters}\n`+
           `#Usage: ${this.usage}`;
  }
}