export class ShoppingCart {
  #productList;

  constructor() {
    this.#productList = [];
  }

  get productList() {
    return this.#productList.slice();
  }

  addProduct(product) {
    if (!product) {
      throw new Error('Invalid product!');
    }

    this.#productList.push(product);
  }

  removeProduct(product) {
    if (!product) {
      throw new Error('Invalid product');
    }
    const productFound = this.#productList.find(p => p.name === product.name);

    if (productFound) {

      this.#productList.splice(this.#productList.indexOf(productFound), 1);
    } else {
      throw new Error('Product cannot be found!');
    }
  }

  containsProduct(product) {
    if (!product) {
      throw new Error('Invalid product!');
    }

    return this.productList.some(p => p.name === product.name);
  }

  totalPrice() {
    return this.#productList.reduce((sum, p) => sum + p.price, 0);
  }
}