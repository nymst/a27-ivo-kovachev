const typeOf = (value, type) => {
  if (typeof value !== type) {
    throw new Error('Wrong input type');
  }
};

const minValue = (value, minValue) => {
  if (value < minValue) {
    throw new Error('Value cannot be a negative number');
  }
};

const instanceOf = (value, instance) => {
  if (!(value instanceof instance)) {
    throw new Error(`Not instance of ${instance}`);
  }
};

const lengthCheck = (value, minLength) => {
  if (value.length < minLength) {
    throw new Error(`Name should be ${minLength}} letters or longer!`);
  }
};

export { typeOf, minValue, instanceOf, lengthCheck };