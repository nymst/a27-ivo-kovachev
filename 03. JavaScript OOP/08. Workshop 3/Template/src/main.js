/* eslint-disable no-unused-vars */
import { ProtossPlayer } from './models/protoss/protoss-player.model.js';
import { Zealot } from './models/protoss/zealot.model.js';
import { TerranPlayer } from './models/terran/terran-player.model.js';
import { ZergPlayer } from './models/zerg/zerg-player.model.js';
import { Marine } from './models/terran/marine.model.js';
import { Viking } from './models/terran/viking.model.js';
import { DarkTemplar } from './models/protoss/dark-templar.model.js';
import { Hydralisk } from './models/zerg/hydralisk.model.js';
import { Lurker } from './models/zerg/lurker.model.js';

const wait = (seconds) => new Promise(r => setTimeout(r, seconds * 1000));

const main = async () => {

  const raynor = new TerranPlayer('Jim Raynor');
  const kerrigan = new ZergPlayer('Sarah Kerrigan');
  const zeratul = new ProtossPlayer('Zeratul');

  const marine1 = new Marine(raynor, 20, 5, 5, 2);
  const marine2 = new Marine(raynor, 20, 5, 5, 2);
  const viking = new Viking(raynor, 200, 50, 12, 8);

  const zealot = new Zealot(zeratul, 40, 6, 6, 2);
  const darkTemplar = new DarkTemplar(zeratul, 150, 2, 12, 4);

  const hydralisk1 = new Hydralisk(kerrigan, 60, 10, 8, 4);
  const hydralisk2 = new Hydralisk(kerrigan, 60, 10, 8, 4);

  // If no error messages show, all the test pass successfully

  console.log('Start testing, it will take a while...');

  // Testing hydralisk morph to lurker ability

  console.assert(kerrigan.army.length === 2, `Expected 2, got ${kerrigan.army.length} instead!`);

  const lurker = hydralisk2.morphToLurker();

  console.assert(kerrigan.army.length === 2, `Expected 2, got ${kerrigan.army.length} instead!`);
  console.assert(kerrigan.army[1] instanceof Lurker, `Expected instance of Lurker, got instance of ${kerrigan.army[1].constructor.name} instead!`);

  // Testing marine stimpack ability

  console.assert(marine1.damage === 5, `Expected 5, got ${marine1.damage} instead!`);
  marine1.stimpack();
  console.assert(marine1.damage === 5 * 1.5, `Expected ${5 * 1.5}, got ${marine1.damage} instead!`);
  await wait(6);
  console.assert(marine1.damage === 5, `Expected 5, got ${marine1.damage} instead!`);

  // Testing zealot
  zealot.charge(marine2);
  console.assert(marine2.health === 20 - 6 * 1.33, `Expected ${20 - 6 * 1.33}, got ${marine2.health} instead!`);

  // Testing viking mode change

  console.assert(viking.damage === 12, `Expected 12, got ${viking.damage} instead!`);
  viking.switch();
  console.assert(viking.damage === 12 * 0.8, `Expected ${12 * 0.8} got ${viking.damage} instead`);

  console.log('End testing.');

};

main()
  .catch(console.log);
