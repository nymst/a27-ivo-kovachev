import { gameUnitType } from '../../common/game-unit-type.enum.js';
import { ZergUnit } from './zerg-unit.model.js';

export class Lurker extends ZergUnit {
    #burrowed = false;

    constructor(player, health, armor, damage, creep) {
        super(player, health, armor, damage, creep);
    }

    get burrowed() {
        return this.#burrowed;
    }

    burrow() {
        this.#burrowed = true;
    }

    unburrow() {
        this.#burrowed = false;
    }
}