import { Player } from '../player.model.js';
import { ZergUnit } from './zerg-unit.model.js';

export class ZergPlayer extends Player {
    constructor(name) {
        super(name);

        this.unitType = ZergUnit;
        this.unitTypeLabel = 'ZergUnit';
    }

    addUnit(unit) {
        this.validateUnit(unit);

        super.addUnit(unit);
    }

    removeUnit(unit) {
        this.validateUnit(unit);

        super.removeUnit(unit);
    }
}