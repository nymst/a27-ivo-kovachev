import { Lurker } from './lurker.model.js';
import { ZergUnit } from './zerg-unit.model.js';

export class Hydralisk extends ZergUnit {
  static #lurkerHealthMultiplier = 1.5;
  static #lurkerArmorMultiplier = 1.2;
  static #lurkerDamageMultiplier = 1.67;

  constructor(player, health, armor, damage, creep) {
    super(player, health, armor, damage, creep);
  }

  morphToLurker() {
    // force to remove from the player's army
    this.health = -1;

    return new Lurker(
      this.player,
      this.health * Hydralisk.#lurkerHealthMultiplier,
      this.armor * Hydralisk.#lurkerArmorMultiplier,
      this.damage * Hydralisk.#lurkerDamageMultiplier,
      this.creep,
    );
  }
}