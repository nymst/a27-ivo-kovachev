import { GameUnit } from '../game-unit.model.js';
import { ZergPlayer } from './zerg-player.model.js';
import { gameUnitType } from '../../common/game-unit-type.enum.js';
import * as validators from '../../common/validators.js';


export class ZergUnit extends GameUnit {
    #creep;

    static CREEP_MIN_VALUE = 1;

    constructor(player, health, armor, damage, creep) {
        validators.instanceOf(player, ZergPlayer);
        validators.typeOf(creep, 'number');
        validators.minValue(creep, ZergUnit.CREEP_MIN_VALUE);

        super(player, health, armor, damage, gameUnitType.Zerg);

        this.#creep = creep;
    }

    get creep() {
        return this.#creep;
    }
}