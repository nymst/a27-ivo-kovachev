import { GameUnit } from '../game-unit.model.js';
import { ProtossUnit } from './protoss-unit.model.js';
import * as validators from '../../common/validators.js';

export class Zealot extends ProtossUnit {
  static #chargeMultiplier = 1.33;

  constructor(player, health, armor, damage, psyEnergy) {
    super(player, health, armor, damage, psyEnergy);
  }

  charge(unit) {
    validators.instanceOf(unit, GameUnit);

    unit.health -= this.damage * Zealot.#chargeMultiplier;
  }
}
