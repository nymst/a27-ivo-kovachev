import { Player } from '../player.model.js';
import { ProtossUnit } from './protoss-unit.model.js';

export class ProtossPlayer extends Player {
    constructor(name) {
        super(name);

        this.unitType = ProtossUnit;
        this.unitTypeLabel = 'ProtossUnit';
    }

    addUnit(unit) {
        this.validateUnit(unit);

        super.addUnit(unit);
    }

    removeUnit(unit) {
        this.validateUnit(unit);

        super.removeUnit(unit);
    }
}