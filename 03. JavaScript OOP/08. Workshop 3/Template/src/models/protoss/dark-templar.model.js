import { ProtossUnit } from './protoss-unit.model.js';

export class DarkTemplar extends ProtossUnit {
    constructor(player, health, armor, damage, psyEnergy) {
        super(player, health, armor, damage, psyEnergy);
    }

    shadowStride() {
        console.log('Unit has teleported!');
    }
}
