import { GameUnit } from '../game-unit.model.js';
import { ProtossPlayer } from './protoss-player.model.js';
import { gameUnitType } from '../../common/game-unit-type.enum.js';
import * as validators from '../../common/validators.js';

export class ProtossUnit extends GameUnit {
    #psyEnergy;

    static MIN_PSYENERGY_VALUE = 0;
    constructor(player, health, armor, damage, psyEnergy) {
        validators.instanceOf(player, ProtossPlayer);
        validators.typeOf(psyEnergy, 'number');
        validators.minValue(psyEnergy, ProtossUnit.MIN_PSYENERGY_VALUE);

        super(player, health, armor, damage, gameUnitType.Protoss);

        this.#psyEnergy = psyEnergy;
    }

    get psyEnergy() {
        return this.#psyEnergy;
    }
}