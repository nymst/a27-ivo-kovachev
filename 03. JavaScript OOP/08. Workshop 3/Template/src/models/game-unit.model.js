import { Player } from './player.model.js';
import { gameUnitType } from '../common/game-unit-type.enum.js';
import * as validators from '../common/validators.js';

export class GameUnit {
  #damage;
  #health;
  #armor;
  #player;
  #unitType;

  static DAMAGE_MIN_VALUE = 1;
  static ARMOR_MIN_VALUE = 1;

  constructor(player, health, armor, damage, unitType) {
    validators.instanceOf(player, Player);

    if (!Object.keys(gameUnitType).some(key => gameUnitType[key] === unitType)) {
      throw new Error(`Invalid game unit type value ${unitType}!`);
    }

    this.#player = player;
    this.health = health;
    this.armor = armor;
    this.damage = damage;
    this.#unitType = unitType;

    this.#player.addUnit(this);
  }

  get unitType() {
    return this.#unitType;
  }

  get player() {
    return this.#player;
  }

  set health(value) {
    validators.typeOf(value, 'number');

    if (value <= 0) {
      value = 0;
      this.#player.removeUnit(this);
    }

    this.#health = value;
  }

  get health() {
    return this.#health;
  }

  set armor(value) {
    validators.typeOf(value, 'number');
    validators.minValue(value, GameUnit.ARMOR_MIN_VALUE);

    this.#armor = value;
  }

  get armor() {
    return this.#armor;
  }

  set damage(value) {
    validators.typeOf(value, 'number');
    validators.minValue(value, GameUnit.DAMAGE_MIN_VALUE);

    this.#damage = value;
  }

  get damage() {
    return this.#damage;
  }

  attack(unit) {
    validators.instanceOf(unit, GameUnit);

    unit.health -= Math.max(this.damage - unit.armor, 0);
  }

}