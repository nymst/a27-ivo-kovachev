import { TerranUnit } from './terran-unit.model.js';

export class Marine extends TerranUnit {

    #hasStimpack = false;

    static STIMPACK_MULTIPLIER = 1.5;
    static STIMPACK_COST = 10;
    static STIMPACK_DURATION = 5000;
    constructor(player, health, armor, damage, tech) {
        super(player, health, armor, damage, tech);
    }

    get damage() {
        return this.#hasStimpack
            ? super.damage * Marine.STIMPACK_MULTIPLIER // stimpack multiplier is 1.5
            : super.damage;
    }

    set damage(value) {
        super.damage = value;
    }

    stimpack() {
        this.#hasStimpack = true;
        this.health -= Marine.STIMPACK_COST; // stimpack cost is 10 health

        setTimeout(() => this.#hasStimpack = false, Marine.STIMPACK_DURATION); // stimpack duration is 5000 ms
    }
}