import { vikingMode } from '../../common/viking-mode.enum.js';
import { TerranUnit } from './terran-unit.model.js';


export class Viking extends TerranUnit {
    static #assaultModeMultiplier = 0.8;

    #mode = vikingMode.Fighter;

    constructor(player, health, armor, damage, tech) {
        super(player, health, armor, damage, tech);
    }

    get damage() {
        return this.#mode === vikingMode.Fighter
            ? super.damage
            : super.damage * Viking.#assaultModeMultiplier;
    }

    set damage(value) {
        super.damage = value;
    }

    switch() {
        this.#mode = this.#mode === vikingMode.Fighter
            ? vikingMode.Assault
            : vikingMode.Fighter;
    }
}