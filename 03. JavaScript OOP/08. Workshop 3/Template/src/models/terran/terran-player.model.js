import { Player } from '../player.model.js';
import { TerranUnit } from './terran-unit.model.js';

export class TerranPlayer extends Player {
    constructor(name) {
        super(name);

        this.unitType = TerranUnit;
        this.unitTypeLabel = 'TerranUnit';
    }

    addUnit(unit) {
        this.validateUnit(unit);

        super.addUnit(unit);
    }

    removeUnit(unit) {
        this.validateUnit(unit);

        super.removeUnit(unit);
    }
}