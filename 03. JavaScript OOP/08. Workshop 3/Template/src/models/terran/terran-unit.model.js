import { GameUnit } from '../game-unit.model.js';
import { TerranPlayer } from './terran-player.model.js';
import { gameUnitType } from '../../common/game-unit-type.enum.js';
import * as validators from '../../common/validators.js';

export class TerranUnit extends GameUnit {
    #tech;

    static TECH_MIN_VALUE = 1;
    constructor(player, health, armor, damage, tech) {
        validators.instanceOf(player, TerranPlayer);
        validators.typeOf(tech, 'number');
        validators.minValue(tech, TerranUnit.TECH_MIN_VALUE);

        super(player, health, armor, damage, gameUnitType.Terran);

        this.#tech = tech;
    }

    get tech() {
        return this.#tech;
    }
}