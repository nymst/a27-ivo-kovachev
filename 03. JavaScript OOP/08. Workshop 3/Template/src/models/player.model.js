import * as validators from '../common/validators.js';
export class Player {
    static #minNameLength = 3;
    static #count = 0;
    #id;
    #army = [];
    #name;
    #unitType;
    #unitTypeLabel;

    constructor(name) {
        validators.typeOf(name, 'string');
        validators.lengthCheck(name, Player.#minNameLength);

        this.#name = name;
        this.#id = Player.#count++;
    }

    get id() {
        return this.#id;
    }

    get name() {
        return this.#name;
    }

    get army() {
        return this.#army.slice();
    }

    set unitType(value) {
        this.#unitType = value;
    }

    set unitTypeLabel(value) {
        this.#unitTypeLabel = value;
    }

    addUnit(unit) {
        if (this.#army.find(u => u === unit)) {
            throw new Error('This unit has already been added!');
        }

        this.#army.push(unit);
    }

    removeUnit(unit) {
        this.#army = this.#army.filter(u => u !== unit);
    }

    validateUnit(unit) {
        if (!(unit instanceof this.#unitType)) {
            throw new Error(`Expected instance of ${this.#unitTypeLabel}!`);
        }
    }
}
