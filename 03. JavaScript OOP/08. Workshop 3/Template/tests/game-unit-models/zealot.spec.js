/* eslint-disable no-undef */
import { ProtossUnit } from '../../src/models/protoss/protoss-unit.model';
import { Player } from '../../src/models/player.model';
import { Zealot } from '../../src/models/protoss/zealot.model';

describe('Zealot', () => {

  it('should extend ProtossUnit', () => {
    expect(Zealot.prototype).toBeInstanceOf(ProtossUnit);
  });

  it('should throw if the passed player is not an instance of ProtossPlayer', () => {
    const player = new Player('test');

    expect(() => new Zealot(player, 10, 10, 10, 10)).toThrow();
  });

});