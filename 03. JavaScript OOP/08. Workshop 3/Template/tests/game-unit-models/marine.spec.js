/* eslint-disable no-undef */
import { Player } from '../../src/models/player.model';
import { Marine } from '../../src/models/terran/marine.model';
import { TerranUnit } from '../../src/models/terran/terran-unit.model';

describe('Marine', () => {

  it('should extend TerranUnit', () => {
    expect(Marine.prototype).toBeInstanceOf(TerranUnit);
  });

  it('should throw if the passed player is not an instance of TerranPlayer', () => {
    const player = new Player('test');

    expect(() => new Marine(player, 10, 10, 10, 10)).toThrow();
  });

});