/* eslint-disable no-undef */
import { Player } from '../../src/models/player.model';
import { ZergUnit } from '../../src/models/zerg/zerg-unit.model';
import { Lurker } from '../../src/models/zerg/lurker.model';

describe('Lurker', () => {

  it('should extend ZergUnit', () => {
    expect(Lurker.prototype).toBeInstanceOf(ZergUnit);
  });

  it('should throw if the passed player is not an instance of ZergPlayer', () => {
    const player = new Player('test');

    expect(() => new Lurker(player, 10, 10, 10, 10)).toThrow();
  });

});