/* eslint-disable no-undef */
import { Player } from '../../src/models/player.model';
import { Hydralisk } from '../../src/models/zerg/hydralisk.model';
import { ZergUnit } from '../../src/models/zerg/zerg-unit.model';

describe('Hydralisk', () => {

  it('should extend ZergUnit', () => {
    expect(Hydralisk.prototype).toBeInstanceOf(ZergUnit);
  });

  it('should throw if the passed player is not an instance of ZergPlayer', () => {
    const player = new Player('test');

    expect(() => new Hydralisk(player, 10, 10, 10, 10)).toThrow();
  });

});