/* eslint-disable no-undef */
import { ProtossUnit } from '../../src/models/protoss/protoss-unit.model';
import { DarkTemplar } from '../../src/models/protoss/dark-templar.model';
import { Player } from '../../src/models/player.model';

describe('DarkTemplar', () => {

  it('should extend ProtossUnit', () => {
    expect(DarkTemplar.prototype).toBeInstanceOf(ProtossUnit);
  });

  it('should throw if the passed player is not an instance of ProtossPlayer', () => {
    const player = new Player('test');

    expect(() => new DarkTemplar(player, 10, 10, 10, 10)).toThrow();
  });

});