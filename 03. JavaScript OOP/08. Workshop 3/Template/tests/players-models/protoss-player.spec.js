/* eslint-disable no-undef */
import { ProtossPlayer } from '../../src/models/protoss/protoss-player.model';
import { Player } from '../../src/models/player.model';

describe('ProtossPlayer', () => {

  it('should extend Player', () => {
    expect(ProtossPlayer.prototype).toBeInstanceOf(Player);
  });

  it('should override addUnit', () => {
    expect(ProtossPlayer.prototype.addUnit).toBeDefined();
    expect(ProtossPlayer.prototype.addUnit).not.toBe(Player.prototype.addUnit);
  });
  
});