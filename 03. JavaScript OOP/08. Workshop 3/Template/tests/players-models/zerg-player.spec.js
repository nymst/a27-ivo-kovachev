/* eslint-disable no-undef */
import { ZergPlayer } from '../../src/models/zerg/zerg-player.model';
import { Player } from '../../src/models/player.model';

describe('ZergPlayer', () => {

  it('should extend Player', () => {
    expect(ZergPlayer.prototype).toBeInstanceOf(Player);
  });

  it('should override addUnit', () => {
    expect(ZergPlayer.prototype.addUnit).toBeDefined();
    expect(ZergPlayer.prototype.addUnit).not.toBe(Player.prototype.addUnit);
  });
  
});