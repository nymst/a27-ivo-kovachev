/* eslint-disable no-undef */
import { Player } from '../../src/models/player.model';
import { TerranPlayer } from '../../src/models/terran/terran-player.model';

describe('TerranPlayer', () => {

  it('should extend Player', () => {
    expect(TerranPlayer.prototype).toBeInstanceOf(Player);
  });

  it('should override addUnit', () => {
    expect(TerranPlayer.prototype.addUnit).toBeDefined();
    expect(TerranPlayer.prototype.addUnit).not.toBe(Player.prototype.addUnit);
  });
  
});