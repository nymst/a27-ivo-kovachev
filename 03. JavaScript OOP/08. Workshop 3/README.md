<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# StarCraft

### 1. Description

StarCraft is a game prototype with the basic logic needed to build a scalable game application.

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **ESLint**, **Jest**

<br>

### 3. Goals

The game prototype is fully implemented, meaning all game logic is already there and the test code runs with no errors.

The **goal** is to refactor the code and make it more scalable, following the principles for good OO design - OOP principles, single responsibility, KISS, DRY, YAGNI, etc.

- Working with classes - writing classes and creating instances of those classes.
- Modeling classes using `fields` (state) and `methods` (behavior).
- Changing the state through getters and setters (remember having both getter and a setter allows to read an write to the property, having only a getter makes the property **readonly** and having only a setter makes the property **writeonly**)
- Protecting the state by encapsulation
- Extracting class constants to static class properties
- Inheriting a base class to reuse its logic
- Lifting repeating logic from similar classes down to the base class
- Applying the principles for good OO design - OOP principles, KISS, DRY, etc.

<br>

### 4. Setup

To get the project up and running, follow these steps:

1. Go inside the `Template` folder.
1. Run `npm install` to restore all dependencies.
1. After that, there are a few scripts you can run:

   - `npm run start` (shorthand: `npm start`) - Runs the `src/main.js` file.
   - `npm run test` (shorthand: `npm test` or `npm t`) - Runs the unit tests in the console.
   - `npm run test:browser` - Runs the unit tests in the browser in interactive mode.

**Note**: Make sure you have Code Lens enabled for JavaScript. You can enable it in user settings (`Ctrl + ,`), type in `codelens` and check all the boxes. Once it's enabled you can track how and where each class, method, field and property is used and referenced and you can easily detect code that is not *yet* used.

<br>

### 5. Project structure

All of the code you will be working with is in the `src` folder. The entry point (the file from which the program is started) is `src/main.js`. There are several folders with different roles:

- `common` - holds shared resources like enums, constants, etc.
- `models` - holds all game models, more specifically:
  - `player.model` - the base player model
  - `game-unit.model` - the base unit model, which all other game units extend
  - `protoss/` - this folder contains all the **Protoss** game units and the unused `ProtossPlayer` model
  - `terran/` - this folder contains all the **Terran** game units and the unused `TerranPlayer` model
  - `zerg/` - this folder contains all the **Zerg** game units and the unused `ZergPlayer` model

You will work exclusively in the `models` folder and are free to modify any of the model files inside.

```bash
npm test
```

or if you want to run the tests in interactive mode inside the browser you can type

```bash
npm run test:browser
```

Unit tests will report your progress with the implementation and show you what the expected result of running a single piece of code and how it is different from your implementation. Some of the tests might look as they are passing, but in fact they can show *false positive* when the test expectation is the code to throw if invalid data is passed, but in reality the code throw because there are problems with the implementations.

Use the tests wisely.

<br>

### 6. Problems to fix

**Important:** Before you rush into refactoring the code read carefully what every piece of the code does, what is the purpose of each model and how models connect to each other. Familiarize yourself with the code and then continue with fixing the problems.

As of this moment the application is working as intended. Testing code runs with no errors. However, there are several problems you will need to address following the principles for good OO design. When you do that ask yourself the following questions:

- Is there repeating logic?
- Can repeating logic be extracted to a:
  - function/method
  - another file
  - base class
- Is every model in the application put to a use?
- Is every model in the application used to its full potential?
- Can a problem with a complex solution be solved in a simpler manner?

Here are some of the problems with the application in its current form (the rest you will need to find yourself):

- repeating logic: look at the `DarkTemplar` and `Zealot` models - there is a lot of repeating logic. What can you do about it? (Similarly look at the `models/terran` and `models/zerg` folders and the models inside.)
- unused models: the `ProtossUnit` model is not used anywhere, should this model even exist and if yes - why? What would you use it for? The `ProtossPlayer` model is not used as well (has 0 references)
- one player can have multiple race models, i.e. it can have both `Zerg` and `Terran` units and currently there is no way to restrict a player instance to a specific race:
  ```js
  const player = new Player('Generic player');

  // both units will be added to the player's army with no error
  const marine1 = new Marine(player, 20, 5, 5, 2);
  const hydralisk2 = new Hydralisk(player, 60, 10, 8, 4);
  ```

All of the problems above are interconnected and have a simple solution provided you read carefully the workshop description. Inheritance for the win!

**Note:** Pay special attention to hardcoded values - is there a better way to handle them?

<br>

### 7. Unit tests

To help you code refactoring you are provided with a few unit tests, which are a big hint towards what need to do to fix the problems with the application and  increase code quality. There are two ways to run the tests:

- `npm run test` (shorthand `npm test` or `npm t`) - This will run the tests in the console
- `npm run test:browser` - This will run the test in interactive mode in the browser where you can control when and how tests are run

Unit test will provide you with feedback on what is working in your code and what is not (and why).

<br>

### 8. Testing the system

There is some testing code the `main.js` file, but the refactored version of the application will need a few tiny adjustment (also a big hint):

```js
/* eslint-disable no-unused-vars */
import { ProtossPlayer } from './models/protoss/protoss-player.model.js';
import { Zealot } from './models/protoss/zealot.model.js';
import { TerranPlayer } from './models/terran/terran-player.model.js';
import { ZergPlayer } from './models/zerg/zerg-player.model.js';
import { Marine } from './models/terran/marine.model.js';
import { Viking } from './models/terran/viking.model.js';
import { DarkTemplar } from './models/protoss/dark-templar.model.js';
import { Hydralisk } from './models/zerg/hydralisk.model.js';
import { Lurker } from './models/zerg/lurker.model.js';

const wait = (seconds) => new Promise(r => setTimeout(r, seconds * 1000));

const main = async () => {

  const raynor = new TerranPlayer('Jim Raynor');
  const kerrigan = new ZergPlayer('Sarah Kerrigan');
  const zeratul = new ProtossPlayer('Zeratul');

  const marine1 = new Marine(raynor, 20, 5, 5, 2);
  const marine2 = new Marine(raynor, 20, 5, 5, 2);
  const viking = new Viking(raynor, 200, 50, 12, 8);

  const zealot = new Zealot(zeratul, 40, 6, 6, 2);
  const darkTemplar = new DarkTemplar(zeratul, 150, 2, 12, 4);

  const hydralisk1 = new Hydralisk(kerrigan, 60, 10, 8, 4);
  const hydralisk2 = new Hydralisk(kerrigan, 60, 10, 8, 4);

    // If no error messages show, all the test pass successfully

    console.log('Start testing, it will take a while...');

    // Testing hydralisk morph to lurker ability
  
    console.assert(kerrigan.army.length === 2, `Expected 2, got ${kerrigan.army.length} instead!`);
  
    const lurker = hydralisk2.morphToLurker();
  
    console.assert(kerrigan.army.length === 2, `Expected 2, got ${kerrigan.army.length} instead!`);
    console.assert(kerrigan.army[1] instanceof Lurker, `Expected instance of Lurker, got instance of ${kerrigan.army[1].constructor.name} instead!`);
  
    // Testing marine stimpack ability
  
    console.assert(marine1.damage === 5, `Expected 5, got ${marine1.damage} instead!`);
    marine1.stimpack();
    console.assert(marine1.damage === 5 * 1.5, `Expected ${5 * 1.5}, got ${marine1.damage} instead!`);
    await wait(6);
    console.assert(marine1.damage === 5, `Expected 5, got ${marine1.damage} instead!`);
  
    // Testing zealot
    zealot.charge(marine2);
    console.assert(marine2.health === 20 - 6 * 1.33, `Expected ${20 - 6 * 1.33}, got ${marine2.health} instead!`);
  
    // Testing viking mode change
  
    console.assert(viking.damage === 12, `Expected 12, got ${viking.damage} instead!`);
    viking.switch();
    console.assert(viking.damage === 12 * 0.8, `Expected ${12 * 0.8} got ${viking.damage} instead`);
  
    console.log('End testing.');

};

main()
  .catch(console.log);
```

Sample output text:

```text
Start testing, it will take a while...
End testing.
```