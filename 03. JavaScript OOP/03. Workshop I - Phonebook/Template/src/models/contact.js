import moment from 'moment';

export class Contact {
  name;
  phone;
  history;

  static minNameLength = 3;
  static maxNameLength = 25;

  constructor(name, phone) {
    if (!name || name === '') {
      throw new Error('No contact name assigned!');
    }

    if (name.length <= Contact.minNameLength || name.length >= Contact.maxNameLength) {
      throw new Error('Invalid name length!');
    }

    this.name = name;
    this.setPhone(phone);
    this.history = [];
  }

  setPhone(phone) {
    const regExSpaceCheck = /^0\d{3}\s\d{3}\s\d{3}$/g;
    const regExDashCheck = /^0\d{3}\-\d{3}\-\d{3}$/g;

    if (typeof phone !== 'string') {
      throw new Error('Wrong number input type');
    }

    if (!regExDashCheck.test(phone) && !regExSpaceCheck.test(phone)) {
      throw new Error('Invalid number format');
    }

    this.phone = phone;
  }

  call() {
    this.history.push(`[${new Date().toLocaleDateString()}` +
      ` ${new Date().toLocaleTimeString()}]Call: Duration: ${Math.floor(Math.random() * 151)} sec.`);
  }

  message(msg) {
    if (!msg || msg === '') {
      throw new Error('Message cannot be empty!');
    }

    if (typeof msg !== 'string') {
      throw new Error('Wrong message input type');
    }

    this.history.push(`[${new Date().toLocaleDateString()}` +
      ` ${new Date().toLocaleTimeString()}]Message: (${msg})`);
  }

  viewHistoryLog() {
    if (this.history.length < 1) {
      console.log('No entries');
    } else {
      console.log(`${this.name}'s call log:\n${this.history.slice(0).reverse().join('\n')}`);
    }
  }
}
