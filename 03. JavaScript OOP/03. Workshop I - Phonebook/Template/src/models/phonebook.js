export class Phonebook {
  contacts;

  constructor() {
    this.contacts = [];
  }

  addContact(contact) {
    const contactIndex = this.contacts.findIndex((existingContact) => existingContact === contact);
    if (!contact || contact === '') {
      throw new Error('Invalid contact input!');
    }

    if (contactIndex >= 0) {
      throw new Error('The contact already exists!');
    }
    this.contacts.push(contact);
  }

  removeContact(name) {
    const contactIndex = this.contacts.findIndex((existingContact) => existingContact.name === name);
    if (!name || name === '') {
      throw new Error('Invalid input!');
    }
    if (contactIndex < 0) {
      throw new Error('Such contact does not exist!');
    }
    this.contacts = this.contacts.filter((item) => item.name !== name);
  }

  updateContact(name, phone) {
    if (!name || name === '') {
      throw new Error('Invalid name!');
    }
    if (!this.contacts.find((contact) => contact.name === name)) {
      throw new Error('This contact does not exist!');
    }

    this.contacts.forEach((contact) => {
      if (contact.name === name) {
        contact.setPhone(phone);
      }
    });
  }

  listAllContacts() {
    if (this.contacts.length < 1) {
      return 'No contacts.';
    }
    let allContacts = '';
    this.contacts.forEach((contact) => {
      allContacts = allContacts.concat(`All contacts:\n${contact.name}: [${contact.phone}]\n`);
    });

    return allContacts;
  }

  search(partialName) {
    if (!partialName) {
      throw new Error('Invalid search type');
    }

    this.contacts = this.contacts.filter((element) => element.name === partialName);
  }
}
