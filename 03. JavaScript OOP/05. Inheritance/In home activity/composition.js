// don't forget to import Athlete from inheritance.js (you will need to also export it there)

import { Athlete } from "./inheritance.js";

class Game {
    constructor(playerOne, playerTwo){
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    start(){
        console.log(this.playerOne.introduce());
        console.log(this.playerTwo.introduce());
    }
}
const mark = new Athlete('Mark', 'Williams', 'snooker');
const john = new Athlete('John', 'Higgins', 'snooker');
const steve = new Athlete('Steve', 'Davis', 'snooker');

const game1 = new Game(mark, john);
game1.start();
const game2 = new Game(mark, steve);
game2.start();
const game3 = new Game(steve, john);
game3.start();