class Person {
    firstName;
    lastName;

    constructor (firstName, lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    introduce(){
        return `Hello, my name is ${this.firstName} ${this.lastName}. `;
    }
}


export class Athlete extends Person{
    sport;

    constructor(firstName, lastName, sport){
        super(firstName, lastName);
        this.sport = sport;
    }

    play(){
        return `Playing some ${this.sport}. `;
    }

    introduce(){
        const baseMessage = super.introduce();
        const childMessage = `I am a ${this.sport} player. `;

        return baseMessage + ' ' + childMessage;
    }
}
