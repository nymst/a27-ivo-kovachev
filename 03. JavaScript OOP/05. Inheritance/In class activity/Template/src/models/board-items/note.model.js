import { noteImportance } from '../../common/note-importance.enum.js';
import { noteStatus } from '../../common/note-status.enum.js';
import { BoardItem } from './board-item.model.js';

export class Note extends BoardItem {
  #description;
  #importance;

  static MIN_DESCRIPTION_LENGTH = 6;
  static MAX_DESCRIPTION_LENGTH = 60;

  constructor(name, description) {
    super(name, noteStatus.CREATED);
    this.description = description;
    this.#importance = noteImportance.AVERAGE;
    this.resetStatus = noteStatus.CREATED;
    this.advanceStatus = noteStatus.PENDING;
    this.completeStatus = noteStatus.APPROVED;
  }

  get description() {
    return this.#description;
  }

  set description(value) {
    if (!value || value === '') {
      throw new Error('The description cannot be empty');
    }

    if (typeof value !== 'string') {
      throw new Error('Wrong input type');
    }

    if (value.length < Note.MIN_DESCRIPTION_LENGTH || value.length > Note.MAX_DESCRIPTION_LENGTH) {
      throw new Error('Description length not within the constraints');
    }

    this.#description = value;
  }

  get importance() {
    return this.#importance;
  }

  toString() {
    return '* Note *\n' +
            `Name: ${this.name}\n` +
            `Status: ${this.status}\n` +
            `Description: ${this.description}`;
  }
}