import { issueStatus } from '../../common/issue-status.enum.js';
import { BoardItem } from './board-item.model.js';

export class Issue extends BoardItem{
  #createdOn;
  #resolvedOn;
  #description;

  static MIN_DESCRIPTION_LENGTH = 10;
  static MAX_DESCRIPTION_LENGTH = 40;

  constructor(name, description) {
    super(name, issueStatus.RAISED);
    this.#createdOn = new Date().toDateString() + ', ' + new Date().toLocaleTimeString();
    this.#resolvedOn = null;
    this.description = description;
    this.resetStatus = issueStatus.RAISED;
    this.advanceStatus = issueStatus.IN_REVIEW;
    this.completeStatus = issueStatus.RESOLVED;
  }

  get description() {
    return this.#description;
  }

  set description(value) {
    if (!value || value === '') {
      throw new Error('The description cannot be empty');
    }

    if (typeof value !== 'string') {
      throw new Error('Wrong input type');
    }

    if (value.length < Issue.MIN_DESCRIPTION_LENGTH || value.length > Issue.MAX_DESCRIPTION_LENGTH) {
      throw new Error('Description length not within the constraints');
    }

    this.#description = value;
  }

  get createdOn() {
    return this.#createdOn;
  }

  get resolvedOn() {
    return this.#resolvedOn;
  }

  complete() {
    super.complete();
    this.#resolvedOn = new Date();
  }

  toString() {
    if (this.#resolvedOn === null) {
      return ` *Issue *\n
      Name: ${this.name}\n
      Status: ${this.status}\n
      Description: ${this.#description}\n
      Created on: ${this.#createdOn}\n
      Resolved on: Not yet resolved`;
    } else {
      return '* Issue *\n' +
             `Name: ${this.name}\n` +
             `Status: ${this.status}\n` +
             `Description: ${this.#description}\n` +
             `Created on: ${this.#createdOn}\n` +
             `Resolved on: ${this.#resolvedOn.toDateString() + ', ' + this.#resolvedOn.toLocaleTimeString()}`;
    }
  }
}

