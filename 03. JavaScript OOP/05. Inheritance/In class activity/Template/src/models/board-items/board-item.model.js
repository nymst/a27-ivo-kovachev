export class BoardItem {
  #name;
  #status;
  #resetStatus;
  #advanceStatus;
  #completeStatus;

  static MIN_NAME_LENGTH = 6;
  static MAX_NAME_LENGTH = 20;

  constructor(name, defaultStatus) {
    this.name = name;
    this.#status = defaultStatus;
  }

  get name() {
    return this.#name;
  }

  set name(value) {
    if (!value || value === '') {
      throw new Error('The name cannot be empty');
    }

    if (typeof value !== 'string') {
      throw new Error('Wrong input type');
    }

    if (value.length < BoardItem.MIN_NAME_LENGTH || value.length > BoardItem.MAX_NAME_LENGTH) {
      throw new Error('Name length not within the constraints');
    }

    this.#name = value;
  }

  get status() {
    return this.#status;
  }

  get resetStatus() {
    return this.#resetStatus;
  }

  set resetStatus(value) {
    this.#resetStatus = value;
  }

  get advanceStatus() {
    return this.#advanceStatus;
  }

  set advanceStatus(value) {
    this.#advanceStatus = value;
  }

  get completeStatus() {
    return this.#completeStatus;
  }

  set completeStatus(value) {
    this.#completeStatus = value;
  }

  reset() {
    this.#status = this.resetStatus;
  }

  advance() {
    this.#status = this.advanceStatus;
  }

  complete() {
    this.#status = this.completeStatus;
  }
}
