import { taskStatus } from '../../common/task-status.enum.js';
import { BoardItem } from './board-item.model.js';

export class Task extends BoardItem {
  #dueDate;

  constructor(name, dueDate) {

    super(name, taskStatus.TODO);
    this.dueDate = dueDate; 
    this.resetStatus = taskStatus.TODO;
    this.advanceStatus = taskStatus.IN_PROGRESS;
    this.completeStatus = taskStatus.DONE;
  }


  get dueDate() {
    return this.#dueDate;
  }

  set dueDate(value) {
    if (!value) {
      throw new Error('Due date not provided!');
    }

    if (value.valueOf() < Date.now().valueOf()) {
      throw new Error('Can\'t set due date to a date in the past!');
    }

    this.#dueDate = value;
  }

  toString() {
    return '* Task *\n' + 
           `Name: ${this.name}\n` + 
           `Status: ${this.status}\n` +
           `Due: ${this.#dueDate.toLocaleDateString()} ${this.#dueDate.toLocaleTimeString()}`;
  }
}
