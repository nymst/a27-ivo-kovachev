// FOR LOOP
// const arr = [2, 1, 3, 4, 6, 0, 0, 5, 7, 8, 10];

// for(let i=0; i<arr.length; i++){
//     console.log(arr[i]);
// }



// WHILE LOOP
// const arr = [2, 1, 3, 4, 6, 0, 0, 5, 7, 8, 10];
// let i = 0;

// while(i<arr.length){
//     console.log(arr[i]);
//     i++;
// }



// FOR-IN LOOP
// const arr = [2, 1, 3, 4, 6, 0, 0, 5, 7, 8, 10];

// for (const key in arr){
//     console.log(arr[key]);
// }



// FOR-OF LOOP
// const arr = [2, 1, 3, 4, 6, 0, 0, 5, 7, 8, 10];

// for(const elements of arr){
//     console.log(elements);
// }



// FIRST HALF ONLY
// const arr = [2, 1, 3, 4, 6, 0, 0, 5, 7, 8, 10];

// for(let i=0; i<(arr.length)/2; i++){
//     console.log(arr[i]);
// }



// EVEN ELEMENTS ONLY
// const arr = [2, 1, 3, 4, 6, 0, 0, 5, 7, 8, 10];

// for (let i = 0; i < arr.length; i++) {

//     if(arr[i] == 0){
//         continue;
//     }

//     if (arr[i] % 2 == 0){
//         console.log(arr[i]);
//     }
// }