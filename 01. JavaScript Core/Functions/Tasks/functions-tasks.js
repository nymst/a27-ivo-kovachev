/**
 * @functionsInHome
 * @task1
 */

// const printError = (msg) => {
//   console.error(msg);
// };

// const add = (a, b) => {
//   if (typeof a !== 'number') {
//     printError(`${a} is not a number!`);
//   }

//   if (typeof b !== 'number') {
//     printError(`${b} is not a number!`);
//   }

//   return a + b;
// }

/**
* @functionsInHome
* @task2
*/

// const checkIfNumber = (number) => {
//   if(typeof number !== 'number'){
//     throw new Error(`${number} is not a number!`);
//   }
// };

// const add = (a, b) => {
//   checkIfNumber(a);
//   checkIfNumber(b);

//   return a + b;
// };

// const subtract = (a, b) => {
//   checkIfNumber(a);
//   checkIfNumber(b);

//   return a - b;
// };

// const multiply = (a, b) => {
//   checkIfNumber(a);
//   checkIfNumber(b);

//   return a * b;
// };

// const divide = (a, b) => {
//   checkIfNumber(a);
//   checkIfNumber(b);

//   return a / b;
// };




//5.
// const calculator = (a, b, operation) => {
//   let results = [];

//     let res = operation(a, b);
//     results.push(res);

//   return results;
// };

// try {
//   console.log(calculator(1, 2, add, add, subtract, divide, multiply));
// } catch (error) {
//   console.log(error.message);
// }



//6.
// const calculator = (a, b, ...operations) => {
//   let results = [];

//   for(const operation of operations){
//     let res = operation(a, b);
//     results.push(res);
//   }
//   return results;
// };


/**
 * @functionsInClassActivity
 */

// const array = [1, 2, 3];

// const forEach = (array, fn) => {
//   for(const item of array) {
//     fn(item);
//   }
// };

// const filter = (array, checkFn) => {
//   const newArray = [];

//   for (const item of array){
//     if (checkFn(item)){
//       newArray.push(item);
//     }
//   }

//   return newArray;
// };

// const map = (array, modifyFn) => {
//   const newArray = [];

//   for (const item of array){
//     const modified = modifyFn(item)
//     newArray.push(modified);
//   }

//   return newArray;
// };

// const modifiedArray = map(array, element => element + 10);
// const filterredArray = filter(modifiedArray, element => element > 10);
// forEach(filterredArray, console.log);