/**
 * @objectsInHome
 */

const user = {
  username: 'Nymst',
  firstName: 'Ivo',
  lastName: 'Kovachev',
  age: 40,
  getFullName() {
    console.log(this.firstName + ' ' + this.lastName);
  }
};


const user1 = Object.create(user, {
  login: {
    value: function () {
      console.log(`${this.username} just logged in!`)
    }
  },
  friends: {
    value: ['kurti', 'tinko', 'parky', 'sisi', 'ya'],
    writable: true
  }
});

user1.logout = function () {
  console.log(`${this.username} just logged out.`)
}

user1.friend = {
  friend: 'Krusteff'
};

Object.defineProperties(user1, {
  location: {
    value: 'Varna',
  }
});

const user2 = {
  username: 'Soulless',
  firstName: 'Joey',
  lastName: 'Stevens',
  age: 30,
  friends: ['murti', 'finko', 'warky', 'disi', 'za']
};


const addFriend = (user1, user2) => {
  user1.friends = [...user1.friends, user2.username];
  user2.friends = [...user2.friends, user1.username];
 
  return {
    user1friends: user1.friends,
    user2friends: user2.friends
  };
};

const friendsArrays = addFriend(user1, user2);
console.log(friendsArrays);