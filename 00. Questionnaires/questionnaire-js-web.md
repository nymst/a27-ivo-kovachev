#### What is a browser? How is it different from node.js?

- A web browser takes you anywhere on the internet, letting you see text, images and video from anywhere in the world. It retrieves information from other parts of the web and displays it on your desktop or mobile device. The information is transferred using the HyperText Transfer Protocol (HTTP), which defines how text, images and video are transmitted on the web. This information needs to be shared and displayed in a consistent format so that people using any browser, anywhere in the world can see the information.

- Both the browser and Node use JavaScript as their programming language but building apps that run in the browser is a **completely different thing** from building a Node.js application. Unlike a browser, Node.js apps bring with them a huge advantage: the comfort of programming everything - the frontend and the backend - **in a single language**. In the browser, most of the time what you are doing is interacting with the DOM, or other Web Platform APIs like Cookies. Those **do not exist** in Node.js, of course. And in the browser, we don't have all the nice APIs that Node.js provides through its modules, like the **filesystem access functionality**. Another big difference is that in Node.js you **control the environment** - you know which version of Node.js you will run the application on and compared to the browser environment, where you don't get the luxury to choose what browser your visitors will use, this is very convenient. This means that you can write all the modern ES6-7-8-9 JavaScript that your Node.js version supports.
- ?Node.js uses ES6-7-8-9 while browsers use Commonjs only?



#### What is the difference between block elements and inline elements?

- A block-level element always **starts on a new line**, always **takes up the full width available** (stretches out to the left and right as far as it can) and has a **top and a bottom margin**, whereas an inline element does not. An inline element does not start on a new line and only **takes up as much width as necessary**. Generally, inline elements may contain **only data and other inline elements**. You **can't** put block elements inside inline elements. By default, inline elements **do not force a new line to begin in the document flow**. Block elements, on the other hand, typically cause a line break to occur (although, as usual, this can be changed using CSS).



### What is HTTP? How does it work?

- HTTP is an abbreviation of **HyperText Transfer Protocol** and it is an Application Protocol for transferring resources across the internet. HTTP uses **Port 80** and it also uses a server-client model. A client may be a home computer or a mobile device. The HTTP server is typically a web host running web server software, such as Apache or IIS.
- As a **request-response protocol**, HTTP gives users a way to interact with web resources such as HTML files by transmitting hypertext messages between clients and servers. HTTP utilizes specific **request methods** in order to perform various tasks.

![](images/HTTP-Basics.png)