<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [SQL Tutorial](https://www.w3schools.com/sql/default.asp)
- [What is SQL? All you need to know about Structured Query Language](https://www.computerworld.com/article/3427818/what-is-sql--all-you-need-to-know-about-structured-query-language.html)
- [GROUP BY and HAVING Clause in SQL](https://www.datacamp.com/community/tutorials/group-by-having-clause-sql)
- [Solve SQL | HackerRank](https://www.hackerrank.com/domains/sql)
