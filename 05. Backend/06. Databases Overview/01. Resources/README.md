<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [7 reasons why you need a database management system](https://www.techopedia.com/2/31970/it-business/7-reasons-why-you-need-a-database-management-system)
- [Database Normalization (Explained in Simple English)](https://www.essentialsql.com/get-ready-to-learn-sql-database-normalization-explained-in-simple-english/)
- [Primary keys: ids versus GUIDs](https://blog.codinghorror.com/primary-keys-ids-versus-guids/)
- [What is database and why do we need them?](https://www.softwaretestingclass.com/what-is-database-and-why-do-we-need-them/)