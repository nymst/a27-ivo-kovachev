import peopleData from './people-data.js';

const getAllPeople = (filter) => {
  const filteredPeople = filter
    ? peopleData.filter((p) =>
        p.name.toLowerCase().includes(filter.toLowerCase())
      )
    : peopleData;

  return filteredPeople;
};

const getPersonById = (id) => {
  return peopleData.find((p) => p.id === id);
};

const createPerson = (personData) => {
  const newPerson = { ...personData, id: peopleData.length };
  peopleData.push(newPerson);

  return newPerson;
};

const updatePerson = (id, personData) => {
  const personIndex = peopleData.findIndex((p) => p.id === id);
  peopleData[personIndex] = { ...peopleData[personIndex], ...personData };

  return peopleData[personIndex];
};

const deletePerson = (id) => {
  const personIndex = peopleData.findIndex((p) => p.id === id);
  const person = peopleData[personIndex];

  peopleData.splice(personIndex, 1);

  return person;
};

export default {
  getAllPeople,
  getPersonById,
  createPerson,
  updatePerson,
  deletePerson,
};
