import express from 'express';
import peopleService from './people-service.js';

const peopleController = express.Router();

peopleController
  // get all people with searching
  .get('/', (req, res) => {
    const { search } = req.query;
    const people = peopleService.getAllPeople(search);

    res.status(200).send(people);
  })
  // get person by id
  .get('/:id', (req, res) => {
    const { id } = req.params;

    const person = peopleService.getPersonById(+id);
    if (!person) {
      res.status(404).send({ message: 'The person is not found!' });
      return;
    }

    res.status(200).send(person);
  })
  // create person
  .post('/', (req, res) => {
    const person = req.body;

    if (!person.name || !person.age) {
      res
        .status(400)
        .send({ message: 'The person should have valid name and age!' });

      return;
    }

    const newPerson = peopleService.createPerson(person);
    res.status(201).send(newPerson);
  })
  // update person by id
  .put('/:id', (req, res) => {
    const { id } = req.params;
    const updateData = req.body;

    if (!updateData.name && !updateData.age) {
      res.status(400).send({
        message: 'The person update data should have valid name or age!',
      });

      return;
    }

    const person = peopleService.getPersonById(+id);
    if (!person) {
      res.status(404).send({ message: 'The person is not found!' });
      return;
    }

    const updatedPerson = peopleService.updatePerson(+id, updateData);
    res.status(200).send(updatedPerson);
  })
  // delete person by id
  .delete('/:id', (req, res) => {
    const { id } = req.params;

    const person = peopleService.getPersonById(+id);
    if (!person) {
      res.status(404).send({ message: 'The person is not found!' });
      return;
    }

    const deletedPerson = peopleService.deletePerson(+id);
    res.status(200).send(deletedPerson);
  });

export default peopleController;
