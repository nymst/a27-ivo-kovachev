import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();
const PORT = 3000;

// CORS Policy and Body Parser Third-party Middleware
app.use(cors(), bodyParser.json());

const data = [
  {
    id: 0,
    name: 'John',
    age: 20,
  },
];

const loggerMiddleware = (req, res, next) => {
  console.log('here');
  next();
};

const peopleRouter = express.Router();
peopleRouter.use(loggerMiddleware);

peopleRouter.get('/', (req, res) => {
  res.status(200).send(data);
});

app.use('/people', peopleRouter);

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
