export default {
  isPublic: (value) => typeof value === 'boolean',
};
