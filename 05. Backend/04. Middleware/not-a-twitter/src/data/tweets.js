/* eslint-disable no-unused-vars */
const tweets = [];

let tweetId = 1;

export const addTweet = (tweet) => tweets.push(tweet);

export const getAllTweets = () => tweets.filter(t => !t.isDeleted);

export const getTweetById = (id) => tweets.find(t => t.id === id && t.isDeleted === false);

export const updateTweet = (id, partialTweet) => {
  const tweet = getTweetById(id);
  if (tweet) {
    Object.keys(partialTweet).forEach(key => tweet[key] = partialTweet[key]);
  }
};

export const createTweet = (tweet, user) => {
  tweets.push({
    ...tweet,
    id: tweetId++,
    date: new Date(),
    isDeleted: false,
    authorId: user.id,
  });

  return tweets[tweets.length - 1];
};

export const deleteTweet = (id) => {
  const tweet = getTweetById(id);

  tweet.isDeleted = true;
};
