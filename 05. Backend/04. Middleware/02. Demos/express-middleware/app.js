import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();
const PORT = 3000;

const data = [
  {
    id: 0,
    name: 'John',
    age: 20,
  },
];

const loggerMiddleware = (req, res, next) => {
  console.log('Here');
  next();
};

const validatorMiddleware = (req, res, next) => {
  if (Object.keys(req.body).length === 0) {
    next('You must send data!');
  } else {
    next();
  }
};

const errorHandlerMiddleware = (err, req, res, next) => {
  res.status(400).send({ message: err });
};

const getPeople = (req, res, next) => {
  res.status(200).send(data);
};

// CORS Policy and Body Parser Third-party Middleware
app.use(cors(), bodyParser.json());

// custom middleware - register after third-party
app.use(loggerMiddleware, validatorMiddleware, errorHandlerMiddleware);

// route handler
app.get('/people', getPeople);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
