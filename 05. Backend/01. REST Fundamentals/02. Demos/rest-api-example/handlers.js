const searchClickHandler = (input, url) => {

  return async () => {
    const res = await fetch(`${url}/orgs/${input.value}/repos`);
    const jsonResult = await res.json();

    if (jsonResult.message) {
      alert(jsonResult.message);
    } else {
      console.log(jsonResult);
    }
  }
}

export {
  searchClickHandler,
}
