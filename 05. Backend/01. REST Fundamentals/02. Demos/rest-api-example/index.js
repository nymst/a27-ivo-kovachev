import { searchRepoEvent } from './events.js';
import { searchClickHandler } from './handlers.js';
import { apiUrl } from './common.js';

(async () => {

  const btn = document.getElementById('get-repos');
  const input = document.getElementById('repo-name');
  searchRepoEvent(btn, searchClickHandler(input, apiUrl));

})();
