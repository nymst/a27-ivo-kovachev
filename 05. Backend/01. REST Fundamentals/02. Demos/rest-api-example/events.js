const searchRepoEvent = (el, handler) => el.addEventListener('click', handler);

export {
  searchRepoEvent,
}
