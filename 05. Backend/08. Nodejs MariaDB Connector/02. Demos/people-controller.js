import express from 'express';
import peopleService from './people-service.js';

const peopleController = express.Router();

peopleController
    // get all people with searching
    .get('/', async (req, res) => {
        const { search } = req.query;
        const people = await peopleService.getAllPeople(search);

        res.status(200).send(people);
    })
    // get person by id
    .get('/:id', async (req, res) => {
        const { id } = req.params;

        const person = await peopleService.getPersonById(+id);
        if (!person) {
            return res.status(404).send({
                message: 'The person is not found!'
            });
        }

        res.status(200).send(person);
    })
    // create person
    .post('/', async (req, res) => {
        const person = req.body;

        if (!person.name || !person.age) {
            return res.status(400).send({
                message: 'The person should have valid name and age!'
            });
        }

        const newPerson = await peopleService.createPerson(person);

        if (newPerson) {
            res.status(201).send(newPerson);
        } else {
            res.status(409).send({ message: 'Name not available' });
        }
    })
    // update person by id
    .put('/:id', async (req, res) => {
        const { id } = req.params;
        const updateData = req.body;

        if (!updateData.name && !updateData.age) {
            return res.status(400).send({
                message: 'The person update data should have valid name or age!',
            });
        }

        const updatedPerson = await peopleService.updatePerson(+id, updateData);

        if (!updatedPerson) {
            res.status(404).send({ message: 'Person not found!' });
        } else {
            res.status(200).send(updatedPerson);
        }
    })
    // delete person by id
    .delete('/:id', async (req, res) => {
        const { id } = req.params;
        const deletedPerson = await peopleService.deletePerson(+id);

        if (!deletedPerson) {
            res.status(404).send({ message: 'Person not found!' });
        } else {
            res.status(200).send(deletedPerson);
        }
    });

export default peopleController;
