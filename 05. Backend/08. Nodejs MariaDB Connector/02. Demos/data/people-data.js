import pool from './pool.js';

const getAll = async () => {
    const sql = `
        SELECT id, name, age 
        FROM people
    `;

    return await pool.query(sql);
}

const getBy = async (column, value) => {
    const sql = `
        SELECT id, name, age 
        FROM people
        WHERE ${column} = ?
    `;  

    const result = await pool.query(sql, [value]);

    return result[0];
}

const searchBy = async (column, value) => {
    const sql = `
        SELECT id, name, age 
        FROM people
        WHERE ${column} LIKE '%${value}%' 
    `; 

    return await pool.query(sql);
}

const create = async (name, age) => {
    const sql = `
        INSERT INTO people(name, age)
        VALUES (?, ?)
    `;

    const result = await pool.query(sql, [name, age]);

    return {
        id: result.insertId,
        name: name,
        age: age
    };
}

const update = async (person) => {
    const { id, name, age } = person;
    const sql = `
        UPDATE people SET
          name = ?,
          age = ?
        WHERE id = ?
    `;

    return await pool.query(sql, [name, age, id]);
}

const remove = async (person) => {
    const sql = `
        DELETE FROM people 
        WHERE id = ?
    `;

    return await pool.query(sql, [person.id]);
}

export default {
    getAll,
    searchBy,
    getBy,
    create,
    update,
    remove
}
