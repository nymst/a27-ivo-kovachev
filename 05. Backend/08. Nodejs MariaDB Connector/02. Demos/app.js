import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import peopleController from './people-controller.js';

const app = express();
const PORT = 3000;

// CORS Policy and Body Parser Third-party Middleware
app.use(cors(), bodyParser.json());

app.use('/people', peopleController);

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
