import peopleData from './data/people-data.js';

const getAllPeople = async (filter) => {
  return filter
    ? await peopleData.searchBy('name', filter)
    : await peopleData.getAll()
};

const getPersonById = async (id) => await peopleData.getBy('id', id);

const createPerson = async (personData) => {
  const { name, age } = personData;

  const existingPerson = await peopleData.getBy('name', name);

  if (existingPerson) {
    // already exists with the same name - do not create
    return null;
  }

  return await peopleData.create(name, age);
};

const updatePerson = async (id, personData) => {
  const person = await peopleData.getBy('id', id);
  if (!person) {
    // no such person to update
    return null;
  }

  const updated = { ...person, ...personData };
  const _ = await peopleData.update(updated);

  return updated;
};

const deletePerson = async (id) => {
  const person = await peopleData.getBy('id', id);
  if (!person) {
    // no such person to delete
    return null;
  }

  const _ = await peopleData.remove(person);

  return person;
};

export default {
  getAllPeople,
  getPersonById,
  createPerson,
  updatePerson,
  deletePerson,
};
