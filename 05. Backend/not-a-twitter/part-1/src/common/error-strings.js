export default {
  tweet: {
    text: `Expected string with length [1-240]`,
    isPublic: `Expected boolean`,
  },
  user: {
    username: ``,
    password: ``,
    email: ``,
  },
};
