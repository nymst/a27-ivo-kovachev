export default {
  text: (value) => typeof value === 'string' && value.length > 0 && value.length < 241,
  isPublic: (value) => typeof value === 'boolean',
};
