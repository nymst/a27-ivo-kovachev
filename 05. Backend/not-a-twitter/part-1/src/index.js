import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import {
  getAllTweets, getTweetById, createTweet, updateTweet, deleteTweet,
} from './data/tweets.js';
import validateBody from './middlewares/validate-body.js';
import createTweetValidator from './validators/create-tweet-validator.js';
import updateTweetValidator from './validators/update-tweet-validator.js';
import transformBody from './middlewares/transform-body.js';
import injectUser from './middlewares/inject-user.js';
import logger from './middlewares/logger.js';

const PORT = 5555;

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(logger);

app.get('/tweets', (req, res) => {
  res.json(getAllTweets());
});

app.get('/tweets/:id', (req, res) => {
  res.json(getTweetById(+req.params.id));
});

app.post('/tweets', injectUser, transformBody(createTweetValidator), validateBody('tweet', createTweetValidator), (req, res) => {
  const tweet = createTweet(req.body, req.user);

  res.json(tweet);
});

app.put('/tweets/:id', transformBody(updateTweetValidator), validateBody('tweet', updateTweetValidator), (req, res) => {
  updateTweet(+req.params.id, req.body);

  res.json({
    message: `Tweet updated`,
  });
});

app.delete('/tweets/:id', (req, res) => {
  deleteTweet(+req.params.id);

  res.json({
    message: `Tweet deleted`,
  });
});

app.listen(PORT, () => console.log(`Listening on ${PORT}...`));
