export default (req, res, next) => {
  req.user = {
    id: 1,
    username: 'Pesho',
    email: 'pesho@ne6tosi.com',
  };

  next();
};
