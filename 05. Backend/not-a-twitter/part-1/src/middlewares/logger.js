import fs from 'fs';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const LOG_FILE_PATH = join(__dirname, '../logs/requests.json');

export default (req, res, next) => {
  try {
    const file = fs.readFileSync(LOG_FILE_PATH, 'utf-8');
    const logs = JSON.parse(file);

    const requestLog = {
      url: req.originalUrl,
      method: req.method,
    };

    if (Object.keys(req.body).length > 0) {
      requestLog.body = req.body;
    }
    if (Object.keys(req.query).length > 0) {
      requestLog.query = req.query;
    }
    if (Object.keys(req.params).length > 0) {
      requestLog.params = req.params;
    }

    logs.push(requestLog);

    fs.writeFileSync(LOG_FILE_PATH, JSON.stringify(logs), 'utf-8');
  } catch (e) {
    console.log(e);
  }

  next();
};
