<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Not-a-twitter API

### 1. Description

A big Chinese company has hired you to develop a tweets managing API that is totally not a clone of Twitter. You're provided with several requirements and the application is built in sprints, information about which you can find below.

There are rumours about a second part of the project focus on a front-end client, but nothing is confirmed at this point, so focus on the API.

<br>

### 2. Project information and requirements

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**

<br>

### 3. Goals

The goal is to develop an application that looks like Twitter, works like Twitter, but is totally not a Twitter app. Nobody wants to be sued

<br>

### 3. Project resources and data format

The API exposes the following resources and sub-resources

<br>

#### 3.1 User

The main resource are the users of the application. Each user contains some basic information, but you can add more properties as you see fit.

- `id` (number, to keep it simple)
- `username` - string with length in the range `[6-30]`
- `displayName` - the public name of the user, string in the range `[6-40]`
- `password` - self-explanatory, must contain letters and numbers, string in the range `[6-100]` (can't be too safe)

<br>

#### 3.2 Tweet

Also the main resource of the application, and a sub-resource of users. All users have tweets. Every tweet has one author (owner, user). A tweet has the following properties:

- `id` (number, to keep it simple)
- `text` - string with length in the range `[1-240]`
- `date` - date / date string - when it was created
- `isPublic` - boolean, self-explanatory
- `isDeleted` - boolean, marks if the tweet has been deleted
- `userId` - the id of the user who created the tweet

<br>

#### 3.3 Votes

Votes are not a resource by itself. Although they will exist as separate objects (data entities), they only describe relations between users and tweets in a more meaningful way, i.e. how users react to tweets. A vote has the following properties:

- `id` (number, to keep it simple)
- `reaction` - an enum value of possible reactions (like, dislike, love, cry, etc.)
- `tweetId` - the id of the tweet the vote/reaction belongs to
- `userId` - the user who create the vote

Please keep in mind the combination of `tweetId` and `userId` must be unique. Only one object (record, data entity) should exist on the same tweet made by the same user, or one user can have only one reaction to a tweet. If they try to vote with a different reaction, the existing vote should be updated.

<br>

### 4. Setup

You will be working in the `api` folder. This will be designated as the **root** folder, where `package.json` should be placed.

There are several things needed to setup the project, but to reduce the amount of work on the setup, you are provided with the following bash script, which once run (in git bash or in the terminal, if you are on unix based OS) will generate the project skeleton for you:

```sh
echo "{
  \"name\": \"not-a-twitter\",
  \"version\": \"1.0.0\",
  \"description\": \"\",
  \"main\": \"src/index.js\",
  \"type\": \"module\",
  \"scripts\": {
    \"start\": \"node src/index.js\",
    \"start:dev\": \"nodemon src/index.js\"
  },
  \"keywords\": [],
  \"author\": \"\",
  \"license\": \"ISC\"
}" >> package.json

npm i babel-eslint babel-jest @babel/preset-env regenerator-runtime @babel/plugin-syntax-dynamic-import eslint nodemon helmet express body-parser cors

echo "{
  \"presets\": [\"@babel/preset-env\"],
  \"plugins\": [\"@babel/plugin-syntax-dynamic-import\"]
}" >> .babelrc

echo "// eslint-disable-next-line no-undef
module.exports = {
	'env': {
		'es2020': true,
		'node': true,
	},
	'parser': 'babel-eslint',
	'parserOptions': {
		'sourceType': 'module',
		'ecmaVersion': 11,
		'ecmaFeatures': {
			'impliedStrict': true,
		},
	},
	'extends': 'eslint:recommended',
	'rules': {
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'comma-dangle': ['warn', 'always-multiline'],
		'max-classes-per-file': ['error', 1],
	},
};" >> .eslintrc.js

mkdir src

touch src/index.js
```

Copy the above in `setup.sh` directly inside the root folder and run it

```sh
bash setup.sh
```

The project can be run in two ways:

- `npm start` - will run the current version of the code and will **not** reflect any changes done to the code until this command is executed again
- `npm run start:dev` - will run the code in *development* mode, meaning every change done to the code will trigger the program to restart and reflect the changes made

<br>

<br>

## Sprint I

<br>

### 1. Project resources and data format

For now we will be implementing only the tweets resource and CRUD operations over it. We will not delve into users and relations between resources. For this purpose the tweets properties have one minor change:

- `id` (number, to keep it simple)
- `text` - string with length in the range `[1-240]`
- `date` - date / date string - when it was created
- `isPublic` - boolean, self-explanatory
- `isDeleted` - boolean, marks if the tweet has been deleted
- `author` - the author of the tweet, string with length in the range `[6-40]`

<br>

### 2. CRUD and endpoints

The following CRUD operations and endpoints should be implemented:

- `GET: /tweets` - returns all non-deleted tweets, should support search by query parameters (`text` and/or `author`), search should be case insensitive
- `GET: /tweets/:id` - returns a single, non-deleted tweet by its id
- `POST: /tweets` - should create a new tweet if the body is valid, or return bad request if it's not
  - body: `{ text: String, isPublic: Boolean}`, validation criteria provided above
- `PUT: /tweets/:id` - should update existing, non-deleted tweet with the provided body (all body properties are optional)
  - body: `{ isPublic: Boolean}`, validation criteria provided above; that's right, tweet's text is immutable, be careful what you post
- `DELETE: /tweets/:id` - deletes a single, existing tweet by its id

<br>

### 3. App features

There are several features you need to implement as well. A non-exhaustive list is provided below.

<br>

### 3.1 Cross-origin requests

CORS needs to be enabled in the app.

<br>

### 3.2 Protection

Provide additional protection to the application by hiding headers exposing the framework name. You may provide additional protection as you see fit.

<br>

#### 3.3 Validation

Every method supporting body should have their body validated. In-handler validation is ok, custom middleware validation is better.

<br>

#### 3.4 Logging

Every request should be logged in a log file. The complete form of the log object is provided below. Note that `body`, `query` and `params` properties are optional and depend on the type and context of the request.

```js
{
  url: String,
  method: String,
  body: Object,
  query: Object,
  params: Object,
}
```

The **Express** [documentation](https://expressjs.com/en/5x/api.html#req) can help you with finding what you need.