import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();
const PORT = 3000;

// CORS Policy and Body Parser
app.use(cors(), bodyParser.json());

const data = [
  {
    id: 0,
    name: 'John',
    age: 20,
  },
];

app
  // get all people with searching
  .get('/people', (req, res) => {
    const { search } = req.query;

    const searchedPeople = search
      ? data.filter((p) => p.name.toLowerCase().includes(search.toLowerCase()))
      : data;

    res.status(200).send(searchedPeople);
  })
  // get person by id
  .get('/people/:id', (req, res) => {
    const { id } = req.params;

    const person = data.find((p) => p.id === +id);
    if (!person) {
      res.status(404).send({ message: 'The person is not found!' });
      return;
    }

    res.status(200).send(person);
  })
  // create person
  .post('/people', (req, res) => {
    const person = req.body;

    if (!person.name || !person.age) {
      res
        .status(400)
        .send({ message: 'The person should have valid name and age!' });

      return;
    }

    const newPerson = { ...person, id: data.length };
    data.push(newPerson);

    res.status(201).send(newPerson);
  })
  // update person by id
  .put('/people/:id', (req, res) => {
    const { id } = req.params;
    const personToUpdate = req.body;

    if (!personToUpdate.name && !personToUpdate.age) {
      res.status(400).send({
        message: 'The person update data should have valid name or age!',
      });

      return;
    }

    const personIndex = data.findIndex((p) => p.id === +id);
    if (personIndex === -1) {
      res.status(404).send({ message: 'The person is not found!' });
      return;
    }

    data[personIndex] = { ...data[personIndex], ...personToUpdate };
    res.status(200).send(data[personIndex]);
  })
  // delete person by id
  .delete('/people/:id', (req, res) => {
    const { id } = req.params;

    const personIndex = data.findIndex((p) => p.id === +id);
    if (personIndex === -1) {
      res.status(404).send({ message: 'The person is not found!' });
      return;
    }

    const person = data[personIndex];
    data.splice(personIndex, 1);

    res.status(200).send(person);
  });

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
