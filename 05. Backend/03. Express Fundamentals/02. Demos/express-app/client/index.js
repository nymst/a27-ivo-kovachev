(() => {
  const btn = document.getElementById('get-people');

  btn.addEventListener('click', async () => {
    const res = await fetch('http://localhost:3000/people');
    const jsonResult = await res.json();

    console.log(jsonResult);
  });
})()
