import express from 'express';
import cors from 'cors';

const app = express();
const PORT = 3000;

// CORS Policy
app.use(cors());

// data
const people = [
  {
    name: 'John',
    age: 20,
  },
  {
    name: 'Maria',
    age: 23,
  },
  {
    name: 'Ivan',
    age: 21,
  },
];

app.get('/people', (req, res) => {
  res.status(200).send(people);
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
