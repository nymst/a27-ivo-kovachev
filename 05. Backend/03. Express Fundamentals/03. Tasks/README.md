<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Todo API

### 1. Description

A big product company has hired you to develop a full-stack application for managing todo items in order to increase company productivity and to introduce you to the company processes. The work on the app is split into two parts - backend API application and front-end client application. Your job is to build the API first, following the best practices of the REST architecture.

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**

<br>

### 3. Goals

The main goal is to initialize and setup the project, and create the base API endpoints.

You are provided with a detailed guide on how to complete the necessary tasks, follow it carefully.

You will exercise:

- setting up a JavaScript project
- setting up the project's environment - **ESLint** and **Babel** configurations, continuous project reloading with **nodemon**
- creating and managing **resources**
- exposing resources through RESTfull API
- processing request objects and returning responses
- applying basic request body validation

<br>

### 4. Setup

The main framework you will be using is **Express**. It allows for very modular design, both OOP and functional style oriented (though it clings more to the functional side and this is the way you will consume it). Since most libraries you will need are not included in **Express** you will need to install them and set them up on your own. No worries, this document will guide you through the process.

<br>

#### 4.1 Initialize the project

You will be working in the `api` folder. This will be designated as the **root** folder, where `package.json` should be placed.

In the `api` folder run the `npm init -y` to initialize the project. The `package.json` file will be created. You may modify the name, version and description as you like. Since you will be using ES6 modules, make sure you have **Nodejs** version 14+ installed. To enable ES6 modules in the project, add this to the `package.json` file:

```
"type": "module"
```

<br>

#### 4.2 Setup **Babel**

Since some language feature are still new or experimental you will need to install and setup **Babel**. **Babel** is a library that will translate new JavaScript code to older, more compatible with older **Nodejs** and browser versions. It has hundreds of plugins which cater to different language features.

First, you will need to install **Babel** and required plugins:

```sh
npm i babel-eslint babel-jest @babel/preset-env regenerator-runtime @babel/plugin-syntax-dynamic-import
```

After all is installed, you will need to create (inside the root folder) the configuration file for **Babel** (exactly as named) `.babelrc` with the following content:

```json
{
  "presets": ["@babel/preset-env"],
  "plugins": ["@babel/plugin-syntax-dynamic-import"]
}
```

<br>

#### 4.3 Setup **ESLint**

The next step is to make sure you have linter included in the project. For this install **ESLint**

```sh
npm i eslint
```

Create the `.eslintrc.js` file inside the root folder with the following content:

```js
// eslint-disable-next-line no-undef
module.exports = {
	'env': {
		'es2020': true,
		'node': true,
	},
	'parser': 'babel-eslint',
	'parserOptions': {
		'sourceType': 'module',
		'ecmaVersion': 11,
		'ecmaFeatures': {
			'impliedStrict': true,
		},
	},
	'extends': 'eslint:recommended',
	'rules': {
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'comma-dangle': ['warn', 'always-multiline'],
		'max-classes-per-file': ['error', 1],
	},
};
```

<br>

#### 4.4 Setup **Nodemon**

Since you will often be making changes to the code, you will want the changes to be reflected in the already running app automatically. You can achieve that through **Nodemon**. Install the package:

```sh
npm i nodemon
```

You will need to add the following scripts to the `package.json`

```json
"scripts": {
  "start": "node src/index.js",
  "start:dev": "nodemon src/index.js"
},
```

As you can see, the starting point of the app is `src/index.js`. The file doesn't exist yet, but don't worry. You will get to that in a minute. When everything is set up, there are two ways to run the app:

- `npm run start` - will run the current version of the code, making changes will not reflect in the running code
- `npm run start:dev` - will run the application in *development* mode, which means every single change to the code will restart the application (any dynamic piece of data will be lost such as elements added or removed to an array, initialized variables, etc.) and the latest version of the code will run.

<br>

#### 4.5 Setup **Express** and related libraries

As we said, **Express** allows for a very modular design. To achieve that, the creators haven't included anything but the core functionalities, leaving the users of the framework to decide which libraries and plugins to use depending on their needs. That gives us the freedom of choice, but also delegates to us to set up every little bit of features we want to include in the app. Again, don't worry. You will be guided through the process.

First, let's install **Express**:

```sh
npm i express
```

You will need to include `body-parser` in order to be able to *read* the requests body:

```sh
npm i body-parser
```

And finally, to make sure your app allows all kinds of clients to consume the API, you will need to allow cross-origin requests. To do that, install `cors`:

```sh
npm i cors
```

<br>

### 5. Project structure

For now you will not be concerning yourself with project architecture, layered design and moving code to single responsible modules. For this exercise you will be working exclusively in the `src/index.js` file, which you will need to create first.

This file will hold the server initialization code, the state of the application (where all todo resources will be contained), and the endpoints where the main resource is created, modified, retrieved, and deleted. More on this below.

- `src/index.js` - the entry point of the project and the only file, for now

<br>

### 6. Working with Postman and postman collections

**Postman** is a great tool for testing the API you will be building. In order to test your api while developing it you can use the **[Postman](https://www.getpostman.com/downloads/)** tool. You are provided with a **postman collection** with a couple of sample requests that will help you do that. Don't feel limited by them and try to create your own cases and requests.

1. Open the **Postman** tool
1. Click the **Import** button and choose the **postman_collection.json** file in the **postman** folder
1. Use the imported requests to **test the API** for the different tasks

![postman_import](./postman/snapshots/postman_import.png)
![postman_collection](./postman/snapshots/postman_collection.png)

<br>

### 7. The **Todo** resource

As you already know *R* in REST stands for **resource**. When building a RESTful API our main concern are the resources we will be exposing through the API. It's our place to decide which resources will be available to the end client application and which of the CRUD operations we will be allowing to be used on the resource

**CRUD** stands for:

- Create (mapped to POST requests)
- Read (or Retrieve) (mapped to GET requests)
- Update (mapped to PUT and PATCH requests)
- Delete (mapped to DELETE requests)

You will be implementing the following endpoints:

- `GET: /todos` - read all non-deleted todos
- `POST: /todos` - create a new todo
- `PUT: /todos/:id` - update a single, non-deleted todo
- `DELETE: /todos/:id` - delete a single todo

<br>

### 8. Creating the app

To create a server app you need to import `express`, `body-parser`, and `cors`. Modules provide default exports, so think of the best way to import them.

Don't forget you will be using ES6 imports and exports from now on.

Then you can use the following template for the app:

```js
const PORT = 3000;

// create an instance of the server
const app = express();

// "tell" express to parse the body to JSON
app.use(bodyParser.json());
// enable cross-origin request
app.use(cors());

const todos = [];

// endpoints implementation here

// mount the server app to the port
app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
```

From now on you will be implementing the endpoints with the information provided below.

Individual endpoints have the following signature:

```js
// app.{the request's method}('{endpoint}', {controller function})
app.get('/todos', (req, res) => {
  // endpoint logic
});
```

The **request** (`req`) object contains valuable information for the request. `req.body` is an object with the request body, `req.params` is an object containing all the path parameters of the endpoint (if any), and `req.query` is an object containing all the query parameters of the endpoint (if any).

The **response** (`res`) object contains all the methods required to send a response. Methods are chainable. `res.status(400)` will force the response status to **400**, `res.json(object)` will return a response with the `object` passes as the body of the response. You can chain them:

```js
res.status(400).json({ msg: 'Not found!' });
```

<br>

### 9. Todo data

In order to start the app with some todos, you will need to hardcode them in the application setup (and update them lated if and when necessary). You will not be actually deleting the todos, but rather mark them as deleted setting the `isDeleted` property to `true`.

```js
const todos = [
  {
    id: 1,
    name: 'Buy milk',
    due: '2021-05-02',
    isDone: false,
    isDeleted: false,
  },
  {
    id: 2,
    name: 'Learn JavaScript',
    due: '2021-09-02',
    isDone: false,
    isDeleted: false,
  },
];
```

<br>

### 10. Retrieving all todos - GET: /todos

- Description: Return all todos that are not deleted from the system.
- Responses:
  - Status: **200**
    - Description: If everything is OK, return status **200 - OK** with a filtered **todos array** that contains only the todos that are not deleted
    - Content:
      - **application/json**
      - Response body: array of todos

<br>

### 11. Creating a todo - POST: /todos

- Description: Add a new todo in the system.
- Body Content:
  - **application/json**
  - Type: **{ name: String, due: Date String }**
- Responses:
  - Status: **201**
    - Description: If everything is OK, create a new **todo** using the **name** and **due** date sent in the **request body**, store it and return status **201 - Created** with the response of the created todo object. The date should might be in format - **YYYY-MM-DD**
    - Content:
      - **application/json**
      - Response body: the created todo
  - Status: **400**
    - Description: If the **name** or the **due** date in the body is invalid, return status **400 - Bad Request** with appropriate message
    - Content:
      - **application/json**
      - Response body: **{ msg: String }**

Note: Date String is any string value, that can be converted to a date with `new Date(dateString)`.

<br>

### 12. Updating a todo - PUT: /todos/:id

- Description: Find a todo by id and update it with the passed object in the body.
- Path Parameter:
  - Description: The desired todo's id
  - Type: **String**
- Body Content:
  - **application/json**
  - Type: **{ name: String, due: Date String, isDone: Boolean }**
- Responses:
  - Status: **200**
    - Description: If everything is OK, update the desired todo using the properties sended in the **request body** and return status **200 - OK** with the response of the updated todo object
    - Content:
      - **application/json**
      - Response body: the updated todo
  - Status: **404**
    - Description: If a todo with such **id** does not exist or it is **deleted**, return status **404 - Not Found** with appropriate message
    - Content:
      - **application/json**
      - Response body: **{ msg: String }**

Note: Date String is any string value, that can be converted to a date with `new Date(dateString)`.

<br>

### 13. Deleting a todo - DELETE: /todos/:id

- Description: Find a todo by id and delete it.
- Path Parameter:
  - Description: The desired todo's id
  - Type: **String**
- Responses:
  - Status: **200**
    - Description: If everything is OK, find the desired todo, update its **isDeleted** property and return status **200 - OK** with the response **{ msg: 'Todo Deleted!' }**
    - Content:
      - **application/json**
      - Response body: **{ msg: String }**
  - Status: **404**
    - Description: If a todo with such **id** does not exist or it is **deleted**, return status **404 - Not Found** with appropriate message
    - Content:
      - **application/json**
      - Response body: ***{ msg: String }**

<br>

### 14. Advanced tasks

<br>

#### 14.1 GET: /

- Description: The landing route for the application.
- Responses:
  - Status: **301**
    - Description: Redirect to the main application resource - **/todos**. You might need to make some research.

<br>

#### 14.2 GET: /todos?name=learn

- Description: Add an optional **query parameter** to your route that will filter the todos by their name.
- Query Parameter:
  - Description: The filtering name that the returned todos should include.
  - Type: **String**