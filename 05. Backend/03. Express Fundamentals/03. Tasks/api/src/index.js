import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

const PORT = 3000;

const server = express();

server.use(bodyParser.json());
server.use(cors());

const todos = [
  {
    id: 1,
    name: 'Buy milk',
    due: '2021-05-02',
    isDone: false,
    isDeleted: false,
  },
  {
    id: 2,
    name: 'Learn JavaScript',
    due: '2021-09-02',
    isDone: false,
    isDeleted: false,
  },
];


server.get()


server.listen(PORT, () => console.log(`Server is listening on port ${PORT}`));