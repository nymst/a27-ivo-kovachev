import { API_URL } from '../../../solution/src/common/constants.js';
import { loadSearchMovies } from '../../../solution/src/requests/request-service.js';
import { CONTAINER_SELECTOR } from '../common/constants.js';
import { movies } from '../data/movies-data.js';
import { searchMovies } from '../data/movies.js';
import { toSearchView } from '../views/search-view.js';
import { q } from './helpers.js';

export const renderSearchItems = (searchTerm) => {
  loadSearchMovies(searchTerm)
  .then(movies => q(CONTAINER_SELECTOR).innerHTML = toSearchView(movies, searchTerm));

};
