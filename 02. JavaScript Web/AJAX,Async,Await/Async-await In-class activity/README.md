<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Mov(e)ster

### 1. Description

Mov(e)ster is a simple web app for viewing and adding/removing movies from favorites. It's completely implemented with vanilla JavaScript (Native DOM API) and vanilla CSS (no libraries).

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**

<br>

### 3. Goals

You are provided with the complete implementation of the app, data layer already moved to the server. However, at the moment code is depending on promise chain in order to work, and promise chains sometimes make the code harder to reason with and figure out when each line will be executed.

The **goal** is to replace promises (wherever possible) and promise chains with the new `async/await` syntax.

- Working with promises and async/await
- Making code more readable and easier to reason with

<br>

### 4. Setup

You can work in the `template` folder or create your own **solution** folder and keep the `template` intact. To run the app make sure you have `live-server` installed **globally**. You are also provided with a separate `server` application which provides the data for the app.

<br>

To run the web app:

1. Go inside the `template` folder (or your own work directory)
1. At root level (where the `index.html` is) run `live-server`
1. You are good to go

<br>

To run the server app:

1. Go inside the `server` folder
1. Install all the npm packages running `npm install`
1. Run the server with `npm start` - it will run on port `3000`

**Important:** Make sure you have [Postman](https://www.postman.com/downloads/) installed in order to test server endpoints.

<br>

### 5. Problems to solve

You're now provided with the complete implementation of the app. No new features need to be added, no changes in logic, no addition or removal of files. The only thing that needs to be done is to replace promises with `async/await` with a few exceptions when working with parallel async executions (you will have to figure out where that is).

There are two main approaches:

- starting from the top - updating `index.js` down to the service layer (the request service)
- starting from the bottom up - updating the service layer first up to the `index.js` at the end

Each approach is valid and each approach will teach you different things. We encourage you to try them both.

<br>

### 6. Things to look for

Be reminded that the `await` keyword will work only in `async` functions. The `await` keyword has no effect on expressions that don't produce a promise value.
