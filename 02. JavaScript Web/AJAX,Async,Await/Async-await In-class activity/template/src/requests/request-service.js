import { API_URL } from '../common/constants.js';

export const loadCategories = async () =>  (await fetch(`${API_URL}/categories`)).json();

export const loadCategory = async (id = null) => (await fetch(`${API_URL}/categories/${id}`)).json();

export const loadMovies = async (categoryId = null) => {
  if (categoryId) {
    return (await fetch(`${API_URL}/categories/${categoryId}/movies`)).json();
  }

  return (await fetch(`${API_URL}/movies`)).json();
};

export const loadSingleMovie = async (id) => (await fetch(`${API_URL}/movies/${id}`)).json();

export const searchMovies = async (searchTerm = '') => (await fetch(`${API_URL}/movies?search=${searchTerm}`)).json();
