import { CONTAINER_SELECTOR } from '../common/constants.js';
import { searchMovies } from '../requests/request-service.js';
import { toSearchView } from '../views/search-view.js';
import { q } from './helpers.js';

export const renderSearchItems = (searchTerm) => {
  searchMovies(searchTerm)
    .then(movies => q(CONTAINER_SELECTOR).innerHTML = toSearchView(movies, searchTerm));
};
